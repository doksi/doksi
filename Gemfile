# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '~> 2.6.3'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 6.0.3'
# Use Puma as the app server
gem 'puma', '~> 4.3'
# Use SCSS for stylesheets
gem 'sass-rails', '>= 6'
# Transpile app-like JavaScript. Read more: https://github.com/rails/webpacker
gem 'webpacker', '~> 5.1'
# Turbolinks makes navigating your web application faster.
# Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.10'
# Use Redis adapter to run Action Cable in production
gem 'redis', '~> 4.2'
# Use Active Model has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Active Storage variant
# gem 'image_processing', '~> 1.2'

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.4.2', require: false

group :development, :production do
  # Use PosgreSQL as the database for Active Record
  gem 'pg', '~> 1.2'
end

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger
  # console
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  # Use sqlite3 as the database in test and dev
  gem 'sqlite3', '~> 1.4'

  # Use FactoryBot to generate fixtures
  gem 'factory_bot_rails', '~> 6.0'
end

group :development do
  # Access an interactive console on exception pages or by calling 'console'
  # anywhere in the code.
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'web-console', '>= 3.3.0'
  # Spring speeds up development by keeping your application running in the
  # background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'

  # custom
  gem 'faker', '~> 2.12.0'
  gem 'foreman', '~> 0.87.1'
end

group :test do
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '>= 2.15'
  gem 'haml_lint', '~> 0.35.0'
  gem 'mocha', '~> 1.11'
  gem 'rubocop', '~> 0.85.1'
  gem 'selenium-webdriver'
  # Easy installation and use of web drivers to run system tests with browsers
  gem 'webdrivers'
  # Test coverage
  gem 'simplecov', '~> 0.18.5'
  gem 'simplecov-cobertura', '~> 1.3.1'
end

###
# Customizations from vanilla Rails
#

# Select box in views with every country
gem 'country_select', '~> 4.0.0'

# Authentication and user management
gem 'devise', '~> 4.7.2'

# HAML is much cleaner than HTML
gem 'haml', '~> 5.1.2'

# Authentication using Google Accounts
gem 'omniauth-google-oauth2', '~> 0.8.0'

# Authentication using Keycloak OIDC
gem 'omniauth-keycloak', '~> 1.2.0'

# Authentication using Microsoft Accounts
gem 'omniauth-microsoft-office365', '~> 0.0.8'

# Authorization
gem 'pundit', '~> 2.1'

# Syntax highlightning in code blocks
gem 'coderay', '~> 1.1'

# To monitor ActiveJob
gem 'activejob-status', '~> 0.1'

# To create zip archives when exporting documents.
gem 'rubyzip', '~> 2.3'

# To create tar archives when exporting documents.
gem 'minitar', '~> 0.9'

# To compress with xz when exporting documents.
gem 'ruby-xz', '~> 1.0'

# To enable settings.yml files.
gem 'config', '~> 2.2'

# To save documents in git repositories
gem 'git', '~> 1.7'

# To validate URL properties of models
gem 'validate_url', '~> 1.0'
