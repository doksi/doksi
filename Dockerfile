FROM docker.io/library/ruby:2.6-alpine

ENV DATABASE_HOST="postgres" \
    DATABASE_PORT=5432 \
    DATABASE_USER="postgres" \
    DATABASE_PASS="changeme" \
    DATABASE_NAME="doksi"

LABEL maintainer="Christoffer Reijer" \
      io.openshift.tags="rails,doksi,web" \
      io.k8s.description="The Doksi web application" \
      io.openshift.wants="postgresql"

# install packages
RUN apk add --no-cache \
    build-base \
    git \
    libxml2-dev \
    libxslt-dev \
    nodejs \
    npm \
    postgresql-client \
    postgresql-dev \
    sqlite-dev \
    tzdata \
  && npm install -g yarn \
  && gem install bundler \
  && gem cleanup

# install podman using edge/testing repository
# todo: remove when podman makes it into stable
COPY container/repositories /etc/apk/repositories
RUN apk add --no-cache podman

# create user
RUN adduser -h /doksi -s /bin/ash -u 65530 -G root -D doksi
WORKDIR /doksi
USER 65530

# install dependencies
COPY Gemfile Gemfile.lock package.json yarn.lock /doksi/
RUN bundle config set path 'vendor/bundle' \
  && bundle config set without 'test' \
  && bundle install \
  && yarn install \
  && find vendor/bundle/ -name "*.md" -exec rm {} \; \
  && find vendor/bundle/ -name "LICENSE" -exec rm {} \; \
  && find node_modules/ -name "*.md" -exec rm {} \; \
  && find node_modules/ -name "LICENSE" -exec rm {} \; \
  && rm -rf .cache .bundle/cache tmp/cache

# copy app files
COPY . /doksi
USER 0
RUN chgrp -R 0 /doksi \
  && chmod -R g=u /doksi \
  # convert dev secrets to production, since the container is run in prod mode
  # for a real prod environment these files should be replaced by mounted files
  && mv config/credentials/development.key \
        config/credentials/production.key \
  && mv config/credentials/development.yml.enc \
        config/credentials/production.yml.enc
USER 65530

# precompile assets
RUN RAILS_ENV=production bin/rails assets:precompile

# add entrypoint and fix permissions
USER 0
COPY container/entrypoint.sh /usr/bin/
RUN chgrp -R 0 /doksi \
  && chmod -R g=u /doksi \
  && chmod +x /usr/bin/entrypoint.sh
USER 65530

# set properties
ENTRYPOINT ["entrypoint.sh"]
ENV HOME /doksi
EXPOSE 3000
CMD ["bundle", "exec", "bin/rails", "server", "-b", "0.0.0.0"]
