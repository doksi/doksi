# frozen_string_literal: true

require 'test_helper'

class DocumentTest < ActiveSupport::TestCase
  setup do
    @document_path = 'storage/test/documents'
  end

  test 'should save document' do
    doc = build(:document)
    assert doc.valid?
    assert doc.save
  end

  test 'should save document with classification' do
    doc = build(:document)
    doc.classification = create(:classification, name: 'Open')
    assert doc.valid?
    assert doc.save
    assert_equal 'Open', doc.classification.name
  end

  test 'should not save document without owner' do
    doc = build(:document, owner: nil)
    assert_not doc.valid?
    assert_equal 'Owner must exist',
                 doc.errors.full_messages_for(:owner).first
  end

  test 'should not save document without title' do
    doc = build(:document, title: '')
    assert_not doc.valid?
    assert_equal 'Title is too short (minimum is 2 characters)',
                 doc.errors.full_messages_for(:title).first
  end

  test 'should not save document with short title' do
    doc = build(:document, title: 'x')
    assert_not doc.valid?
    assert_equal 'Title is too short (minimum is 2 characters)',
                 doc.errors.full_messages_for(:title).first
  end

  test 'should not save document with long title' do
    doc = build(:document, title: 'X' * 51)
    assert_not doc.valid?
    assert_equal 'Title is too long (maximum is 50 characters)',
                 doc.errors.full_messages_for(:title).first
  end

  test 'should not save document with long copyright' do
    doc = build(:document, copyright: 'X' * 51)
    assert_not doc.valid?
    assert_equal 'Copyright is too long (maximum is 50 characters)',
                 doc.errors.full_messages_for(:copyright).first
  end

  test 'should search' do
    doc = create(:document)
    results = Document.search(nil)
    assert_equal Document.all.length, results.length

    doc.update(title: 'Very unique title')

    results = Document.search(doc.title)
    assert_equal 1, results.length
    assert_equal doc.id, results.first.id
  end

  test 'should get classifications' do
    classification = create(:classification)
    doc = build(:document, owner: nil)
    assert_includes doc.classifications, classification
  end

  test 'should get classifications from user' do
    create(:classification)
    user = create(:user, classifications: [create(:classification)])
    doc = create(:document, owner: user)
    global_classifications = Classification.where(owner: nil)
    assert_includes doc.classifications, global_classifications.first
    assert_includes doc.classifications, user.classifications.first
  end

  test 'should get classifications from team' do
    create(:classification)
    team = create(:team, classifications: [create(:classification)])
    doc = create(:document, owner: team)
    global_classifications = Classification.where(owner: nil)
    assert_includes doc.classifications, global_classifications.first
    assert_includes doc.classifications, team.classifications.first
  end

  test 'should get statuses' do
    status = create(:status)
    doc = build(:document, owner: nil)
    assert_includes doc.statuses, status
  end

  test 'should get statuses from user' do
    create(:status)
    user = create(:user, statuses: [create(:status)])
    doc = create(:document, owner: user)
    global_statuses = Status.where(owner: nil)
    assert_includes doc.statuses, global_statuses.first
    assert_includes doc.statuses, user.statuses.first
  end

  test 'should get statuses from team' do
    create(:status)
    team = create(:team, statuses: [create(:status)])
    doc = create(:document, owner: team)
    global_statuses = Status.where(owner: nil)
    assert_includes doc.statuses, global_statuses.first
    assert_includes doc.statuses, team.statuses.first
  end

  test 'should get properties' do
    prop = create(:property)
    doc = build(:document, owner: nil)
    assert_includes doc.available_properties, prop
  end

  test 'should get properties from user' do
    create(:property)
    user = create(:user, properties: [create(:property)])
    doc = create(:document, owner: user)
    global = Property.where(owner: nil)
    assert_includes doc.available_properties, global.first
    assert_includes doc.available_properties, user.properties.first
  end

  test 'should get properties from team' do
    create(:property)
    team = create(:team, properties: [create(:property)])
    doc = create(:document, owner: team)
    global = Property.where(owner: nil)
    assert_includes doc.available_properties, global.first
    assert_includes doc.available_properties, team.properties.first
  end

  test 'should set property' do
    prop = create(:property)
    doc = create(:document)
    DocumentsProperty.create(document: doc, property: prop, value: 'Custom value')
    assert_equal 1, doc.properties.count
    assert_equal 1, doc.assigned_properties.count
    assert_equal 'Custom value', doc.assigned_properties.first.value
  end

  test 'should update property' do
    prop = create(:property)
    doc = create(:document)
    DocumentsProperty.create(document: doc, property: prop, value: 'Custom value')
    doc.assigned_properties.where(property: prop).update(value: 'New value')
    assert_equal 1, doc.properties.count
    assert_equal 1, doc.assigned_properties.count
    assert_equal 'New value', doc.assigned_properties.first.value
  end
end
