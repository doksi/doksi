# frozen_string_literal: true

require 'test_helper'

class UserTest < ActiveSupport::TestCase
  def omniauth_for(provider)
    json = File.read("test/fixtures/files/omniauth/#{provider}.json")
    JSON.parse json, object_class: OpenStruct
  end

  test 'should create user' do
    assert_difference('User.count', 1, "Didn't create user") do
      User.create(email: 'foo@bar.com', time_zone: 'UTC', username: 'myuser')
    end

    assert_no_difference('User.count', 'Created user with duplicate mail') do
      assert_raises(ActiveRecord::RecordNotUnique) do
        User.create(email: 'foo@bar.com', time_zone: 'UTC', username: 'auser')
      end
    end
  end

  test 'should destroy user' do
    user = create(:user)
    assert_difference('User.count', -1, "Didn't remove user") do
      user.destroy
    end
  end

  test 'should create from google auth' do
    assert_difference('User.count', 1, "Didn't create user") do
      auth = omniauth_for(:google)
      user = User.from_omniauth auth
      assert_equal 'john@example.com', user.email
      assert_equal 'john@example.com', auth.info.email
      assert_equal 'John Smith', user.fullname
      assert_equal 'John Smith', auth.info.name
      assert_equal 'john', user.username
      assert user.persisted?
    end
  end

  test 'should create from office365 auth' do
    auth = omniauth_for(:office365)

    assert_difference('User.count', 1, "Didn't create user") do
      user = User.from_omniauth auth
      assert_equal 'john@example.com', user.email
      assert_equal 'john@example.com', auth.info.email
      assert_equal 'john', user.username
      assert user.persisted?
    end
  end

  test 'should create from keycloak auth' do
    auth = omniauth_for(:keycloak)

    assert_difference('User.count', 1, "Didn't create user") do
      user = User.from_omniauth auth
      assert_equal 'john@example.com', user.email
      assert_equal 'john@example.com', auth.info.email
      assert_equal 'john', user.username
      assert user.persisted?
    end
  end

  test 'should create with existing email' do
    create(:user, email: 'adam@example.com', username: 'adam')
    auth = omniauth_for(:google)
    auth.info.email = 'adam@example.com'

    assert_difference('User.count', 0, 'Created new user') do
      user = User.from_omniauth auth
      assert_equal 'adam@example.com', user.email
      assert_equal 'adam@example.com', auth.info.email
      assert_equal 'adam', user.username
      assert user.persisted?
    end
  end

  test 'should support mulitple providers' do
    auth1 = omniauth_for(:google)
    auth2 = omniauth_for(:office365)
    auth1.info.email = auth2.info.email

    user1 = User.from_omniauth auth1
    user2 = User.from_omniauth auth2

    assert_equal user1, user2
  end

  test 'should generate new username' do
    create(:user, email: 'aadam@test.com', username: 'adam')
    auth = omniauth_for(:office365)
    auth.info.email = 'adam@example.com'

    user = User.from_omniauth auth
    assert_equal 'adam@example.com', user.email
    assert_match(/adam_[1-9]+/, user.username)
    assert user.persisted?
  end

  test 'should validate time zone' do
    user = User.new(
      fullname: 'My User',
      username: 'myuser'
    )

    user.time_zone = 'invalid'
    assert_not user.valid?

    user.time_zone = 'Stockholm'
    assert user.valid?
  end

  test 'should validate description' do
    user = User.new(
      fullname: 'My User',
      username: 'myuser',
      time_zone: 'Stockholm'
    )

    user.description = 'X' * 251
    assert_not user.valid?

    user.description = 'X' * 250
    assert user.valid?
  end

  test 'should get team documents' do
    Git.stubs(:init).returns(stub(config: true, add: true, status: stub(added: {}), commit: true))
    docs = create_list(:document, 3)
    user = create(:user, teams: create_list(:team, 2))
    user.teams[0].documents << docs[0]
    user.teams[0].documents << docs[1]
    user.teams[1].documents << docs[2]
    assert_equal docs, user.team_documents
  end
end
