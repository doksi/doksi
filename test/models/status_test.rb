# frozen_string_literal: true

require 'test_helper'

class StatusTest < ActiveSupport::TestCase
  test 'should save status' do
    status = create(:status)
    assert status.valid?
    assert status.save
  end

  test 'should be associated with team' do
    team = create(:team)
    status = build(:status, owner: team)
    assert status.save
    assert_includes team.statuses, status
  end

  test 'should be associated with user' do
    user = create(:user)
    status = build(:status, owner: user)
    assert status.save
    assert_includes user.statuses, status
  end

  test 'should require a name' do
    status = build(:status, name: '')
    assert_not status.valid?
    assert_equal "Name can't be blank",
                 status.errors.full_messages_for(:name).first
  end

  test 'should limit minimum name length' do
    status = build(:status, name: 'x')
    assert_not status.valid?
    assert_equal 'Name is too short (minimum is 2 characters)',
                 status.errors.full_messages_for(:name).first
  end

  test 'should limit maximum name length' do
    status = build(:status, name: 'x' * 31)
    assert_not status.valid?
    assert_equal 'Name is too long (maximum is 30 characters)',
                 status.errors.full_messages_for(:name).first
  end
end
