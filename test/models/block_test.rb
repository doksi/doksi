# frozen_string_literal: true

require 'test_helper'

class BlockTest < ActiveSupport::TestCase
  setup do
    Git.stubs(:init).returns(stub(config: true, add: true, status: stub(added: {}), commit: true))
    @document = create(:document)
    @document_path = 'storage/test/documents'
  end

  test 'sanitize html' do
    b = build(:block, document: @document)
    b.content = <<~HTML
      This
      <div class="CodeRay">
        is
      </div>
      my <I>paragraph</i> cont<b>e<s>nt</b>!</s> With<b> bad tags
      <script>alert('hello')</script>
      stripped out.
    HTML

    expected_content = <<~HTML
      This
      <div>
        is
      </div>
      my <i>paragraph</i> cont<b>e<s>nt</s></b>! With<b> bad tags
      alert('hello')
      stripped out.
      </b>
    HTML

    assert b.save
    assert_equal expected_content.strip, b.content
  end

  test 'load rst content' do
    b = create(:block, document: @document)

    rst_path = "#{@document_path}/#{@document.id}/blocks/#{b.id}.rst"
    rst_content = 'this is the rst content'
    mock_parse_rst(rst_path, "<paragraph>#{rst_content}</paragraph>")

    result = b.class.from_rst(b.rst_file)
    assert_equal rst_content, result[:content]
  end

  test 'refresh' do
    blocks = [
      { id: 900, type: 'Paragraph', content: 'My Block' },
      { id: 901, type: 'List', content: 'My List' }
    ]
    @document.blocks.refresh blocks

    assert_equal 2, @document.blocks.count

    assert_instance_of Paragraph, @document.blocks[0]
    assert_equal 'My Block', @document.blocks[0].content
    assert_equal 0, @document.blocks[0].order
    assert @document.blocks[0].persisted?

    assert_instance_of List, @document.blocks[1]
    assert_equal 'My List', @document.blocks[1].content
    assert_equal 1, @document.blocks[1].order
    assert @document.blocks[1].persisted?
  end

  test 'find_or_build_list' do
    @document.blocks.find_or_build_list [
      { id: 900, type: 'Paragraph', content: 'My Block' },
      { id: 901, type: 'List', content: 'My List' }
    ]
    assert_equal 2, @document.blocks.count

    assert_instance_of Paragraph, @document.blocks[0]
    assert_equal 'My Block', @document.blocks[0].content
    assert_equal 0, @document.blocks[0].order
    assert @document.blocks[0].persisted?

    assert_instance_of List, @document.blocks[1]
    assert_equal 'My List', @document.blocks[1].content
    assert_equal 1, @document.blocks[1].order
    assert @document.blocks[1].persisted?
  end

  test 'find_or_build' do
    block = @document.blocks.find_or_build({ id: 123, type: 'Paragraph', content: 'My Block' }, 1)
    assert_instance_of Paragraph, block
    assert_equal 'My Block', block.content
    assert_equal 1, block.order
  end

  test 'render jinja template' do
    content = 'Answer: {{ foo + bar }}'
    data = '{ "foo": 40, "bar": 2 }'
    block = create(:block, content: content)

    status = mock
    status.expects(:success?).returns(true)
    Open3.expects(:capture3).with(
      'bin/parse-jinja',
      '--base64',
      Base64.encode64(content).strip,
      Base64.encode64(data).strip
    ).returns(['Answer: 42', '', status])

    assert_equal 'Answer: 42', block.rendered(data)
  end

  test 'returns content when render jinja fails' do
    block = create(:block)
    status = mock
    status.expects(:success?).returns(false)
    Open3.expects(:capture3).returns(['', 'my error', status])
    assert_equal block.content, block.rendered('{}')
  end

  test 'render html content' do
    content = 'foo'
    block = create(:block, content: content)

    status = mock
    status.expects(:success?).returns(true)
    Open3.expects(:capture3).with(
      'bin/parse-rst',
      'html',
      Base64.encode64(content).strip
    ).returns(['<p>foo</p>', '', status])

    assert_equal '<p>foo</p>', block.to_html
  end

  test 'raises to_rst' do
    exception = assert_raises(StandardError) do
      Block.new.to_rst
    end
    assert_equal 'You need to implement a custom to_rst for this block type', exception.message
  end

  test 'decrypts block' do
    cipher = '3b11a7337e88d8e2a74c7ed2aef732afa68a473eb99cf701eb07d7' \
             'eedd0cada4U2FsdGVkX1/zryBNDhms3UFXZGnHucUKQzE+PaAT/oU='
    block = create(:block, content: cipher)
    plain = block.decrypt('s3cr3t')
    assert_equal 'foobar', plain
  end
end
