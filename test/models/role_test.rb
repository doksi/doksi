# frozen_string_literal: true

require 'test_helper'

class RoleTest < ActiveSupport::TestCase
  test 'should save role' do
    role = build(:role)
    assert role.valid?
    assert role.save
  end

  test 'should require a name' do
    role = build(:role, name: '')
    assert_not role.valid?
    assert_equal "Name can't be blank",
                 role.errors.full_messages_for(:name).first
  end

  test 'should limit minimum name length' do
    role = build(:role, name: 'x')
    assert_not role.valid?
    assert_equal 'Name is too short (minimum is 2 characters)',
                 role.errors.full_messages_for(:name).first
  end

  test 'should limit maximum name length' do
    role = build(:role, name: 'x' * 31)
    assert_not role.valid?
    assert_equal 'Name is too long (maximum is 30 characters)',
                 role.errors.full_messages_for(:name).first
  end
end
