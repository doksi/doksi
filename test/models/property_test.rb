# frozen_string_literal: true

require 'test_helper'

class PropertyTest < ActiveSupport::TestCase
  test 'should save property' do
    property = build(:property)
    assert property.valid?
    assert property.save
  end

  test 'should be associated with team' do
    team = create(:team)
    property = build(:property, owner: team)
    assert property.save
    assert_includes team.properties, property
  end

  test 'should be associated with user' do
    user = create(:user)
    property = build(:property, owner: user)
    assert property.save
    assert_includes user.properties, property
  end
end
