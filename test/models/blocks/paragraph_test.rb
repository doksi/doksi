# frozen_string_literal: true

require 'test_helper'

class ParagraphTest < ActiveSupport::TestCase
  setup do
    Git.stubs(:init).returns(stub(config: true, add: true, status: stub(added: {}), commit: true))
    @document = create(:document)
    @document_path = 'storage/test/documents'
  end

  test 'should create paragraph' do
    b = Paragraph.new(content: '', document: @document)
    assert b.save
    assert_equal 'justify', b.alignment
  end

  test 'should write rst on create' do
    b = Paragraph.new(content: 'my paragraph', document: @document)
    assert b.save

    block_path = "#{@document_path}/#{@document.id}/blocks/#{b.id}.rst"
    actual_rst = File.read(block_path)

    assert FileTest.exist?(block_path)
    assert_equal 'my paragraph', actual_rst
  end

  test 'should remove rst on destroy' do
    b = Paragraph.new(content: 'my paragraph', document: @document)
    assert b.save

    block_path = "#{@document_path}/#{@document.id}/blocks/#{b.id}.rst"

    assert FileTest.exist?(block_path)
    assert b.destroy
    assert_not FileTest.exist?(block_path)
  end

  test 'load rst content' do
    b = create(:block, document: @document)

    rst_path = "#{@document_path}/#{@document.id}/blocks/#{b.id}.rst"
    rst_content = 'this is the rst content'
    mock_parse_rst(rst_path, "<paragraph>#{rst_content}</paragraph>")

    result = b.class.from_rst(b.rst_file)
    assert_equal rst_content, result[:content]
  end

  %w[warning note tip].each do |kind|
    test "should generate #{kind} rst" do
      b = Paragraph.new(content: 'my paragraph', document: @document, kind: kind)
      expected_rst = <<~EXPECTED
        .. #{kind}::
            my paragraph
      EXPECTED

      assert_equal expected_rst, b.to_rst
    end

    test "should load #{kind} rst" do
      b = create(:block, document: @document)

      rst_path = "#{@document_path}/#{@document.id}/blocks/#{b.id}.rst"
      rst_content = 'this is the rst content'
      mock_parse_rst(rst_path, "<#{kind}><paragraph>#{rst_content}</paragraph></#{kind}")

      result = b.class.from_rst(b.rst_file)
      assert_equal rst_content, result[:content]
      assert_equal kind, result[:kind]
    end
  end

  test 'should throw for invalid type' do
    b = create(:block, document: @document)

    rst_path = "#{@document_path}/#{@document.id}/blocks/#{b.id}.rst"
    mock_parse_rst(rst_path, '<tip><list>foo</list><tip>')

    exception = assert_raise StandardError do
      b.class.from_rst(b.rst_file)
    end
    assert_equal 'Unknown paragraph element: list', exception.message
  end

  test 'should throw for invalid kind' do
    b = create(:block, document: @document)

    rst_path = "#{@document_path}/#{@document.id}/blocks/#{b.id}.rst"
    mock_parse_rst(rst_path, '<list>foo</list>')

    exception = assert_raise StandardError do
      b.class.from_rst(b.rst_file)
    end
    assert_equal 'Unknown paragraph element: list', exception.message
  end
end
