# frozen_string_literal: true

require 'test_helper'

class QuoteTest < ActiveSupport::TestCase
  setup do
    Git.stubs(:init).returns(stub(config: true, add: true, status: stub(added: {}), commit: true))
    @document = create(:document)
    @document_path = 'storage/test/documents'
  end

  test 'create quote' do
    b = Quote.new(content: '', source: '', document: @document)
    assert b.save
    assert_equal b.alignment, 'justify'
  end

  test 'rst saving quote' do
    b = Quote.new(content: "my multiline\nquote", source: 'my source', document: @document)
    assert b.save

    block_path = "#{@document_path}/#{@document.id}/blocks/#{b.id}.rst"
    actual_rst = File.read(block_path)

    assert FileTest.exist?(block_path)
    assert_equal actual_rst, "    my multiline\n    quote\n\n    -- my source"

    b.source = nil
    b.save
    actual_rst = File.read(block_path)
    assert_equal actual_rst, "    my multiline\n    quote"
  end

  test 'rst removing quote' do
    b = Quote.new(content: 'my quote', source: 'my source', document: @document)
    assert b.save

    block_path = "#{@document_path}/#{@document.id}/blocks/#{b.id}.rst"

    assert FileTest.exist?(block_path)
    assert b.destroy
    assert_not FileTest.exist?(block_path)
  end

  test 'load rst content' do
    b = create(:quote_block, document: @document)

    rst_path = "#{@document_path}/#{@document.id}/blocks/#{b.id}.rst"
    rst_content = [
      '<block_quote>',
      '<paragraph>Do. Or do not. There is no try.</paragraph>',
      '<attribution>Yoda</attribution>',
      '</block_quote>'
    ].join('')
    mock_parse_rst(rst_path, rst_content)

    result = b.class.from_rst(b.rst_file)
    assert_equal 'Do. Or do not. There is no try.', result[:content]
    assert_equal 'Yoda', result[:source]
  end
end
