# frozen_string_literal: true

require 'test_helper'

class RawTest < ActiveSupport::TestCase
  setup do
    Git.stubs(:init).returns(stub(config: true, add: true, status: stub(added: {}), commit: true))
    @document = create(:document)
    @document_path = 'storage/test/documents'
  end

  test 'should create raw block' do
    b = Raw.new(content: '', document: @document)
    assert b.save
  end

  test 'should write rst on create' do
    b = Raw.new(content: 'my rst', document: @document)
    assert b.save

    block_path = "#{@document_path}/#{@document.id}/blocks/#{b.id}.rst"
    actual_rst = File.read(block_path)

    assert FileTest.exist?(block_path)
    assert_equal 'my rst', actual_rst
  end

  test 'should remove rst on destroy' do
    b = Raw.new(content: 'my rst', document: @document)
    assert b.save

    block_path = "#{@document_path}/#{@document.id}/blocks/#{b.id}.rst"

    assert FileTest.exist?(block_path)
    assert b.destroy
    assert_not FileTest.exist?(block_path)
  end

  test 'load rst content' do
    rst_content = 'this is the rst content'
    b = Raw.new(content: rst_content, document: @document)

    File.expects(:exist?).with(b.rst_file).returns(true)
    File.expects(:read).with(b.rst_file).returns(rst_content)

    result = b.class.from_rst(b.rst_file)
    assert_equal rst_content, result[:content]
  end
end
