# frozen_string_literal: true

require 'test_helper'

class CodeTest < ActiveSupport::TestCase
  setup do
    Git.stubs(:init).returns(stub(config: true, add: true, status: stub(added: {}), commit: true))
    @document = create(:document)
    @document_path = 'storage/test/documents'
  end

  test 'create code' do
    b = Code.new(content: '# comment', document: @document, kind: 'ruby')
    expected_html = <<~EXPECTED
      <div class="CodeRay">
        <div class="code"><pre><span style="color:#777"># comment</span></pre></div>
      </div>
    EXPECTED
    assert b.save
    assert_equal expected_html, b.to_html
  end

  test 'rst saving heading' do
    b = Code.new(content: '# comment', document: @document, kind: 'ruby')
    assert b.save

    block_path = "#{@document_path}/#{@document.id}/blocks/#{b.id}.rst"
    assert FileTest.exist?(block_path)

    actual_rst = File.read(block_path)
    expected_rst = <<~EXPECTED
      .. code-block:: ruby

          # comment
    EXPECTED
    assert_equal expected_rst.strip, actual_rst
  end

  test 'load rst content' do
    b = create(:code_block, document: @document)

    rst_path = "#{@document_path}/#{@document.id}/blocks/#{b.id}.rst"
    rst_content = [
      '<literal_block classes="code ruby" xml:space="preserve">',
      '<inline classes="comment single"># print</inline>',
      "\n",
      '<inline classes="name builtin">puts</inline> ',
      '<inline classes="literal string single">\'my test\'</inline>',
      '</literal_block>'
    ].join('')
    mock_parse_rst(rst_path, rst_content)

    result = b.class.from_rst(b.rst_file)
    assert_equal "# print\nputs 'my test'", result[:content]
    assert_equal :ruby, result[:kind]
  end
end
