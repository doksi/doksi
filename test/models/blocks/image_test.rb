# frozen_string_literal: true

require 'test_helper'

class ImageTest < ActiveSupport::TestCase
  setup do
    Git.stubs(:init).returns(stub(config: true, add: true, status: stub(added: {}), commit: true))
    @document = create(:document)
    @document_path = 'storage/test/documents'
  end

  test 'create image' do
    b = Image.new(content: 'https://picsum.photos/500/200', document: @document)
    assert b.save
    assert_equal 'https://picsum.photos/500/200', b.content
  end

  test 'rst saving image' do
    b = Image.new(content: 'https://picsum.photos/500/200', document: @document)
    assert b.save

    block_path = "#{@document_path}/#{@document.id}/blocks/#{b.id}.rst"
    assert FileTest.exist?(block_path)

    actual_rst = File.read(block_path)
    expected_rst = '.. image:: https://picsum.photos/500/200'
    assert_equal expected_rst, actual_rst
  end

  test 'rst removing image' do
    b = Image.new(content: 'https://picsum.photos/500/200', document: @document)
    assert b.save

    block_path = "#{@document_path}/#{@document.id}/blocks/#{b.id}.rst"

    assert FileTest.exist?(block_path)
    assert b.destroy
    assert_not FileTest.exist?(block_path)
  end

  test 'load rst content' do
    b = create(:image_block, document: @document)

    rst_path = "#{@document_path}/#{@document.id}/blocks/#{b.id}.rst"
    rst_content = '<image uri="https://lorempixel.com/300/300"></image>'
    mock_parse_rst(rst_path, rst_content)

    result = b.class.from_rst(b.rst_file)
    assert_equal 'https://lorempixel.com/300/300', result[:content]
  end
end
