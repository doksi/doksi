# frozen_string_literal: true

require 'test_helper'

class TableTest < ActiveSupport::TestCase
  setup do
    Git.stubs(:init).returns(stub(config: true, add: true, status: stub(added: {}), commit: true))
    @document = create(:document)
    @document_path = 'storage/test/documents'
  end

  test 'create table' do
    b = Table.new(content: '[]', rows: 1, columns: 1, document: @document)
    assert b.save
  end

  test 'rst saving table' do
    table_content = [
      [{ content: 'A' }, { content: 'B' }, { content: 'C' }],
      [{ content: '1' }, { content: '2' }, { content: '3' }]
    ]
    b = create(:table_block, content: table_content.to_json, rows: 2, columns: 3, document: @document)

    block_path = "#{@document_path}/#{@document.id}/blocks/#{b.id}.rst"
    actual_rst = File.read(block_path)
    expected_rst = <<~EXPECTED
      .. list-table:: Table
          :header-rows: 1
          
          * - A
            - B
            - C
          * - 1
            - 2
            - 3
    EXPECTED
    assert FileTest.exist?(block_path)
    assert_equal expected_rst, actual_rst
  end

  test 'rst removing table' do
    b = create(:table_block, document: @document)

    block_path = "#{@document_path}/#{@document.id}/blocks/#{b.id}.rst"

    assert FileTest.exist?(block_path)
    assert b.destroy
    assert_not FileTest.exist?(block_path)
  end

  test 'load rst content' do
    b = create(:table_block, document: @document)

    rst_path = "#{@document_path}/#{@document.id}/blocks/#{b.id}.rst"
    rst_content = [
      '<table>',
      '<title>Table</title>',
      '<tgroup cols="2">',
      '<colspec colwidth="50"></colspec>',
      '<colspec colwidth="50"></colspec>',
      '<thead><row>',
      '<entry><paragraph>1</paragraph></entry>',
      '<entry><paragraph>2</paragraph></entry>',
      '</row></thead>',
      '<tbody>',
      '<row>',
      '<entry><paragraph>One</paragraph></entry>',
      '<entry><paragraph>Two</paragraph></entry>',
      '</row>',
      '<row>',
      '<entry><paragraph>Uno</paragraph></entry>',
      '<entry><paragraph>Dos</paragraph></entry>',
      '</row>',
      '</tbody>',
      '</tgroup></table>'
    ].join('')
    mock_parse_rst(rst_path, rst_content)

    expected_content = [
      [{ content: '1' }, { content: '2' }],
      [{ content: 'One' }, { content: 'Two' }],
      [{ content: 'Uno' }, { content: 'Dos' }]
    ].to_json

    result = b.class.from_rst(b.rst_file)
    assert_equal expected_content, result[:content]
    assert_equal 3, result[:rows]
    assert_equal 2, result[:columns]
  end
end
