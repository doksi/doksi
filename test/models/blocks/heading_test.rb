# frozen_string_literal: true

require 'test_helper'

class HeadingTest < ActiveSupport::TestCase
  setup do
    Git.stubs(:init).returns(stub(config: true, add: true, status: stub(added: {}), commit: true))
    @document = create(:document)
    @document_path = 'storage/test/documents'
  end

  test 'create heading' do
    b = Heading.new(content: '', document: @document)
    assert b.save
    assert_equal b.level, 1
    assert_equal b.alignment, 'justify'
  end

  test 'rst saving heading' do
    b = Heading.new(content: 'My Title', document: @document)
    assert b.save

    block_path = "#{@document_path}/#{@document.id}/blocks/#{b.id}.rst"
    assert FileTest.exist?(block_path)

    actual_rst = File.read(block_path)
    expected_rst = <<~EXPECTED
      My Title
      ========
    EXPECTED
    assert_equal expected_rst, actual_rst

    b.update(level: 2)
    actual_rst = File.read(block_path)
    expected_rst = <<~EXPECTED
      My Title
      --------
    EXPECTED
    assert_equal expected_rst, actual_rst

    b.update(level: 3)
    actual_rst = File.read(block_path)
    expected_rst = <<~EXPECTED
      My Title
      ++++++++
    EXPECTED
    assert_equal expected_rst, actual_rst
  end

  test 'load rst content' do
    b = create(:heading_block, document: @document)

    result = parse_rst(b, 'Debeo Aeneus', '=')
    assert_equal 'Debeo Aeneus', result[:content]
    assert_equal 1, result[:level]

    result = parse_rst(b, 'Debeo Aeneus', '-')
    assert_equal 'Debeo Aeneus', result[:content]
    assert_equal 2, result[:level]

    result = parse_rst(b, 'Debeo Aeneus', '~')
    assert_equal 'Debeo Aeneus', result[:content]
    assert_equal 5, result[:level]
  end

  def parse_rst(block, heading, underline)
    rst_path = "#{@document_path}/#{@document.id}/blocks/#{block.id}.rst"
    raw_rst = "\n#{heading}\n#{underline * heading.length}\n"
    parsed_rst = "<title>#{heading}</title>"
    mock_parse_rst(rst_path, parsed_rst)
    File.expects(:read).twice.with(rst_path).returns(raw_rst)
    block.class.from_rst(block.rst_file)
  end
end
