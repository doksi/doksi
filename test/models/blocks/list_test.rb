# frozen_string_literal: true

require 'test_helper'

class ListTest < ActiveSupport::TestCase
  setup do
    Git.stubs(:init).returns(stub(config: true, add: true, status: stub(added: {}), commit: true))
    @document = create(:document)
    @document_path = 'storage/test/documents'
  end

  test 'create list' do
    b = List.new(content: '<li>Foo</li>', document: @document)
    assert b.save
    assert_equal b.kind, 'unordered'
    assert_equal b.alignment, 'justify'
  end

  test 'rst saving list' do
    b = List.new(content: '<li>Foo</li><li>Bar</li>', document: @document)
    assert b.save

    block_path = "#{@document_path}/#{@document.id}/blocks/#{b.id}.rst"
    assert FileTest.exist?(block_path)

    actual_rst = File.read(block_path)
    expected_rst = <<~EXPECTED

      * Foo
      * Bar

    EXPECTED

    assert_equal expected_rst, actual_rst
  end

  test 'load unordered rst content' do
    b = create(:unordered_list_block, document: @document)

    rst_path = "#{@document_path}/#{@document.id}/blocks/#{b.id}.rst"
    rst_content = [
      '<bullet_list bullet="*">',
      '<list_item><paragraph>foo</paragraph></list_item>',
      '<list_item><paragraph>bar</paragraph></list_item>',
      '<list_item><paragraph>baz</paragraph></list_item>',
      '</bullet_list>'
    ].join('')
    mock_parse_rst(rst_path, rst_content)

    result = b.class.from_rst(b.rst_file)
    assert_equal '<li>foo</li><li>bar</li><li>baz</li>', result[:content]
    assert_equal :unordered, result[:kind]
  end

  test 'load ordered rst content' do
    b = create(:ordered_list_block, document: @document)

    rst_path = "#{@document_path}/#{@document.id}/blocks/#{b.id}.rst"
    rst_content = [
      '<enumerated_list enumtype="arabic" prefix="" suffix=".">',
      '<list_item><paragraph>foo</paragraph></list_item>',
      '<list_item><paragraph>bar</paragraph></list_item>',
      '<list_item><paragraph>baz</paragraph></list_item>',
      '</enumerated_list>'
    ].join('')
    mock_parse_rst(rst_path, rst_content)

    result = b.class.from_rst(b.rst_file)
    assert_equal '<li>foo</li><li>bar</li><li>baz</li>', result[:content]
    assert_equal :ordered, result[:kind]
  end
end
