# frozen_string_literal: true

require 'test_helper'

class TeamTest < ActiveSupport::TestCase
  test 'should save' do
    team = build(:team)
    assert team.valid?
    assert team.save
  end

  test 'should not save team without name' do
    team = build(:team, name: '')
    assert_not team.valid?
    assert_equal 'Name is too short (minimum is 2 characters)',
                 team.errors.full_messages_for(:name).first
  end

  test 'should not save team with short name' do
    team = build(:team, name: 'x')
    assert_not team.valid?
    assert_equal 'Name is too short (minimum is 2 characters)',
                 team.errors.full_messages_for(:name).first
  end

  test 'should not save team with long name' do
    team = build(:team, name: 'x' * 31)
    assert_not team.valid?
    assert_equal 'Name is too long (maximum is 30 characters)',
                 team.errors.full_messages_for(:name).first
  end

  test 'should not save team with long description' do
    team = build(:team, description: 'x' * 101)
    assert_not team.valid?
    assert_equal 'Description is too long (maximum is 100 characters)',
                 team.errors.full_messages_for(:description).first
  end

  test 'should not save team without user' do
    team = build(:team)
    team.users = []
    assert_not team.valid?
    assert_equal 'Users is too short (minimum is 1 character)',
                 team.errors.full_messages_for(:users).first
  end

  test 'should get all where user is not a member' do
    teams = create_list(:team, 5)
    user = create(:user, teams: [teams[1], teams[2]])
    assert_equal 2, user.teams.count
    assert_equal 3, Team.without_member(user).count
    assert_equal 0, (user.teams & Team.without_member(user)).count

    user.teams << teams[0]

    assert_equal 3, user.teams.count
    assert_equal 2, Team.without_member(user).count
    assert_equal 0, (user.teams & Team.without_member(user)).count
  end
end
