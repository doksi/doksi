# frozen_string_literal: true

require 'test_helper'

class ClassificationTest < ActiveSupport::TestCase
  test 'should save classification' do
    classification = create(:classification)
    assert classification.valid?
    assert classification.save
  end

  test 'should be associated with team' do
    team = create(:team)
    classification = build(:classification, owner: team)
    assert classification.save
    assert_includes team.classifications, classification
  end

  test 'should be associated with user' do
    user = create(:user)
    classification = build(:classification, owner: user)
    assert classification.save
    assert_includes user.classifications, classification
  end

  test 'should require a name' do
    classification = build(:classification, name: '')
    assert_not classification.valid?
    assert_equal "Name can't be blank",
                 classification.errors.full_messages_for(:name).first
  end

  test 'should limit minimum name length' do
    classification = build(:classification, name: 'x')
    assert_not classification.valid?
    assert_equal 'Name is too short (minimum is 2 characters)',
                 classification.errors.full_messages_for(:name).first
  end

  test 'should limit maximum name length' do
    classification = build(:classification, name: 'x' * 31)
    assert_not classification.valid?
    assert_equal 'Name is too long (maximum is 30 characters)',
                 classification.errors.full_messages_for(:name).first
  end
end
