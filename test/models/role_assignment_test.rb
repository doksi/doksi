# frozen_string_literal: true

require 'test_helper'

class RoleAssignmentTest < ActiveSupport::TestCase
  test 'should save role_assignment assignment' do
    role_assignment = build(:role_assignment)
    assert role_assignment.valid?
    assert role_assignment.save
  end

  test 'should require a role' do
    role_assignment = build(:role_assignment, role: nil)
    assert_not role_assignment.valid?
    assert_equal 'Role must exist', role_assignment.errors.full_messages_for(:role).first
  end

  test 'should require a subject' do
    role_assignment = build(:role_assignment, subject: nil)
    assert_not role_assignment.valid?
    assert_equal 'Subject must exist', role_assignment.errors.full_messages_for(:subject).first
  end

  test 'should require an object' do
    role_assignment = build(:role_assignment, object: nil)
    assert_not role_assignment.valid?
    assert_equal 'Object must exist', role_assignment.errors.full_messages_for(:object).first
  end
end
