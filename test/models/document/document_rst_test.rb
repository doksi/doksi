# frozen_string_literal: true

require 'test_helper'

class DocumentRstTest < ActiveSupport::TestCase
  setup do
    @document_path = 'storage/test/documents'
  end

  test 'should create rst folder on create' do
    doc = create(:document)
    assert doc.save
    assert_equal "storage/test/documents/#{doc.id}", doc.rst_folder
    assert FileTest.exist?(doc.rst_folder), "The folder #{doc.rst_folder} does not exist"
  end

  test 'should create index.rst' do
    doc = create(:document)
    rst_path = "storage/test/documents/#{doc.id}/index.rst"
    expected_rst = <<~RST
      .. this is a comment i think

      My Document
      ===========

      .. toctree::
        :maxdepth: 2
        :caption: Contents:

        main

      .. only:: html

        :ref:`genindex`

    RST

    assert doc.save
    assert FileTest.exist?(rst_path), "The file #{rst_path} does not exist"
    actual_rst = File.read(rst_path)
    assert_equal expected_rst, actual_rst
  end

  test 'should create main.rst' do
    doc = create(:document)
    doc.blocks << create(:block)
    rst_path = "storage/test/documents/#{doc.id}/main.rst"
    expected_rst = '.. include:: blocks/1.rst'

    assert doc.save
    assert FileTest.exist?(rst_path), "The file #{rst_path} does not exist"
    actual_rst = File.read(rst_path)
    assert_equal expected_rst, actual_rst
  end

  test 'should remove rst folder on destroy' do
    doc = create(:document)
    assert FileTest.exist?(doc.rst_folder)
    assert doc.destroy
    assert_not FileTest.exist?(doc.rst_folder)
  end

  test 'should commit files to git' do
    doc = create(:document)
    doc.git_user = 'Alice Userson'
    doc.git_mail = 'alice@mail.com'

    git_folder = File.join('storage', 'test', 'documents', '1')
    status = mock
    status.expects(:added).returns({ 'index.rst': {} })
    git = mock
    git.expects(:config).with('user.name', 'Alice Userson')
    git.expects(:config).with('user.email', 'alice@mail.com')
    git.expects(:add).with('index.rst')
    git.expects(:add).with('main.rst')
    git.expects(:add).with('blocks')
    git.expects(:status).twice.returns(status)
    git.expects(:commit).with('Save document with Doksi')
    Git.expects(:init).with(git_folder).returns(git)
    File.expects(:exist?).with(File.join(git_folder, 'blocks')).returns(true)

    assert doc.commit
  end

  test 'should not commit files if unchanged' do
    doc = create(:document)
    doc.git_user = 'Alice Userson'
    doc.git_mail = 'alice@mail.com'

    git_folder = File.join('storage', 'test', 'documents', '1')
    status = mock
    status.expects(:added).returns({})
    git = mock
    git.expects(:config).with('user.name', 'Alice Userson')
    git.expects(:config).with('user.email', 'alice@mail.com')
    git.expects(:add).with('index.rst')
    git.expects(:add).with('main.rst')
    git.expects(:add).with('blocks')
    git.expects(:status).twice.returns(status)
    git.expects(:commit).times(0)
    Git.expects(:init).with(git_folder).returns(git)
    File.expects(:exist?).with(File.join(git_folder, 'blocks')).returns(true)

    assert doc.commit
  end
end
