# frozen_string_literal: true

require 'test_helper'

class DocumentExportTest < ActiveSupport::TestCase
  test 'should export to pdf' do
    image = 'registry.gitlab.com/doksi/sphinx:latest'
    podman_run = [
      '/usr/bin/podman',
      'run',
      '--rm',
      '-it',
      '-v',
      '$PWD/tmp/export/1/pdf:/docs',
      image,
      'make',
      'latexpdf'
    ]
    doc = create(:document)

    status = mock
    status.expects(:success?).returns(true)
    Open3.expects(:capture3).with(podman_run.join(' ')).returns(['', '', status])

    doc.export_prepare(:pdf)
    doc.export_build(:pdf)
  end

  test 'should export to epub' do
    image = 'registry.gitlab.com/doksi/sphinx:latest'
    podman_run = [
      '/usr/bin/podman',
      'run',
      '--rm',
      '-it',
      '-v',
      '$PWD/tmp/export/1/epub:/docs',
      image,
      'make',
      'epub'
    ]
    doc = create(:document)

    status = mock
    status.expects(:success?).returns(true)
    Open3.expects(:capture3).with(podman_run.join(' ')).returns(['', '', status])

    doc.export_prepare(:epub)
    doc.export_build(:epub)
  end

  test 'should export to html' do
    image = 'registry.gitlab.com/doksi/sphinx:latest'
    podman_run = [
      '/usr/bin/podman',
      'run',
      '--rm',
      '-it',
      '-v',
      '$PWD/tmp/export/1/html:/docs',
      image,
      'make',
      'html'
    ]
    doc = create(:document)

    status = mock
    status.expects(:success?).returns(true)
    Open3.expects(:capture3).with(podman_run.join(' ')).returns(['', '', status])

    doc.export_prepare(:html)
    doc.export_build(:html)
  end

  test 'should throw for invalid export format' do
    exception = assert_raises RuntimeError do
      create(:document).export_finalize('myformat')
    end
    assert_equal 'Unknown format: myformat', exception.message
  end
end
