# frozen_string_literal: true

require 'test_helper'

class PermissionTest < ActiveSupport::TestCase
  setup do
    Git.stubs(:init).returns(stub(config: true, add: true, status: stub(added: {}), commit: true))
  end

  test 'create permission' do
    user = create(:user)
    document = create(:document)
    p = Permission.new(
      subject: document,
      object: user,
      mode: :allow,
      privilege: :read
    )
    assert p.valid?
    assert p.save
  end

  test 'should allow action' do
    user = create(:user)
    document = create(:document)

    create(:read_permission, subject: user, object: document)
    assert Permission.allows?(user, :read, document)
    assert_not Permission.allows?(user, :write, document)
    assert_not Permission.allows?(user, :admin, document)

    create(:admin_permission, subject: user, object: document)
    assert Permission.allows?(user, :read, document)
    assert Permission.allows?(user, :write, document)
    assert Permission.allows?(user, :admin, document)
  end

  test 'should deny action' do
    user = create(:user)
    document = create(:document)

    create(:deny_admin_permission, subject: user, object: document)
    assert_not Permission.denies?(user, :read, document)
    assert_not Permission.denies?(user, :write, document)
    assert Permission.denies?(user, :admin, document)

    create(:deny_read_permission, subject: user, object: document)
    assert Permission.denies?(user, :read, document)
    assert Permission.denies?(user, :write, document)
    assert Permission.denies?(user, :admin, document)
  end
end
