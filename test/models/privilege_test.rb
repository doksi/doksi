# frozen_string_literal: true

require 'test_helper'

class RoleTest < ActiveSupport::TestCase
  test 'should save privilege' do
    privilege = build(:privilege)
    assert privilege.valid?
    assert privilege.save
  end

  test 'should require a role' do
    privilege = build(:privilege, role: nil)
    assert_not privilege.valid?
    assert_equal 'Role must exist', privilege.errors.full_messages_for(:role).first
  end

  test 'should enforce proper resource' do
    privilege = build(:privilege, resource: 'x')
    assert_not privilege.valid?
    assert_equal 'Resource x is not valid', privilege.errors.full_messages_for(:resource).first
  end

  test 'should enforce proper verb' do
    privilege = build(:privilege, verb: 'x')
    assert_not privilege.valid?
    assert_equal 'Verb x is not valid', privilege.errors.full_messages_for(:verb).first
  end
end
