import { shallowMount } from '@vue/test-utils'
import EditClassifications from 'Classifications/EditList.vue'

import axios from 'axios'
jest.mock('axios')

function mountForm(response) {

  // mock axios request
  const defaultResponse = [
    { name: 'TestClass', id: 1 },
    { name: 'AnotherClass', id: 2 }
  ]
  const resp = { data: response || defaultResponse }
  axios.get.mockResolvedValue(resp)

  // mock csrf token
  document.getElementsByName = jest.fn(() => {
    return [
      { getAttribute: function() { return 'my-secret-token'} }
    ]
  })

  return shallowMount(EditClassifications, { propsData: { url: 'http://example.com' } })
}

describe('EditClassifications', () => {
  it('has a created hook', () => {
    expect(typeof EditClassifications.created).toBe('function')
  })

  it('sets the correct default data', () => {
    expect(typeof EditClassifications.data).toBe('function')
    const defaultData = EditClassifications.data()
    expect(defaultData.isAddingClassification).toEqual(false)
    expect(defaultData.isLoading).toEqual(true)
    expect(defaultData.classifications).toEqual([])
    expect(defaultData.errorMessage).toEqual('')
    expect(defaultData.classificationToAdd).toEqual('')
  })

  it('mounts properly', () => {
    const wrapper = mountForm()
    expect(wrapper.vm.$data.classifications.length).toEqual(0)
  })

  it('displays error message', async () => {
    const wrapper = shallowMount(EditClassifications, { propsData: { url: '' } })
    await wrapper.vm.$nextTick()
    wrapper.vm.$data.errorMessage = 'test error'
    await wrapper.vm.$nextTick()
    expect(wrapper.text()).toContain('test error')
  })

  describe('getting classification list', () => {

    it('sets classifications from URL', async () => {
      const classifications = [ { id: 1, name: 'TestClassification' } ]
      const resp = { data: classifications }
      axios.get.mockResolvedValue(resp)

      const url = 'https://example.com/classifications.json'
      const wrapper = shallowMount(EditClassifications, { propsData: { url: url } })
      await wrapper.vm.$nextTick()
      expect(wrapper.vm.$data.classifications.length).toEqual(1)
      expect(wrapper.vm.$data.classifications[0]['id']).toEqual(1)
      expect(wrapper.vm.$data.classifications[0]['name']).toEqual('TestClassification')
    })

    it('handle API errors', async () => {
      axios.get.mockRejectedValue('my error')
      console.error = jest.fn()
  
      const url = 'https://example.com/classifications.json'
      const wrapper = shallowMount(EditClassifications, { propsData: { url: url } })
      await wrapper.vm.$nextTick()
      expect(wrapper.vm.$data.classifications.length).toEqual(0)
      expect(wrapper.vm.$data.errorMessage).toEqual('my error')
    })
  })
  
  describe('.addClassification', () => {
    it('adds a classification', async () => {
      const wrapper = mountForm()
      await wrapper.vm.$nextTick()

      const resp = { data: { name: 'TestClassification', id: 420 } }
      axios.post.mockResolvedValue(resp)

      expect(wrapper.vm.$data.classifications.length).toEqual(2)
      expect.assertions(4)
      return wrapper.vm.addClassification().then(function() {
        expect(wrapper.vm.$data.classifications.length).toEqual(3)
        expect(wrapper.vm.$data.classifications[2]['name']).toEqual('TestClassification')
        expect(wrapper.vm.$data.classifications[2]['id']).toEqual(420)
      });
    })

    it('handles errors', async () => {
      const wrapper = mountForm()
      await wrapper.vm.$nextTick()

      axios.post.mockRejectedValue('my error')

      expect(wrapper.vm.$data.classifications.length).toEqual(2)
      expect.assertions(3)
      return wrapper.vm.addClassification().then(function() {
        expect(wrapper.vm.$data.classifications.length).toEqual(2)
        expect(wrapper.vm.$data.errorMessage).toEqual('my error')
      });
    })
  })
  
  describe('.removeClassification', () => {
    it('removes a classification', async () => {
      const wrapper = mountForm()
      await wrapper.vm.$nextTick()

      axios.delete.mockResolvedValue({})

      expect(wrapper.vm.$data.classifications.length).toEqual(2)
      expect.assertions(2)
      return wrapper.vm.removeClassification(1).then(function() {
        expect(wrapper.vm.$data.classifications.length).toEqual(1)
      });
    })

    it('handles errors', async () => {
      const wrapper = mountForm()
      await wrapper.vm.$nextTick()

      axios.delete.mockRejectedValue('my error')

      expect(wrapper.vm.$data.classifications.length).toEqual(2)
      expect.assertions(3)
      return wrapper.vm.removeClassification(1).then(function() {
        expect(wrapper.vm.$data.classifications.length).toEqual(1)
        expect(wrapper.vm.$data.errorMessage).toEqual('my error')
      });
    })
  })
  
  describe('.openClassification', () => {
    it('opens a classification', async () => {
      const wrapper = mountForm()
      await wrapper.vm.$nextTick()

      let classification = wrapper.vm.$data.classifications[0]
      history.pushState = jest.fn()
      wrapper.vm.openClassification(classification)
      expect(wrapper.vm.$data.currentClassification).toBe(classification)
      expect(history.pushState.mock.calls.length).toBe(1)
      expect(history.pushState.mock.calls[0][0]).toBe(null)
      expect(history.pushState.mock.calls[0][1]).toBe(null)
      expect(history.pushState.mock.calls[0][2]).toBe('#1')
    })
  })
  
  describe('.closeClassification', () => {
    it('closes a classification', async () => {
      const wrapper = mountForm()
      await wrapper.vm.$nextTick()

      let classification = wrapper.vm.$data.classifications[0]
      wrapper.vm.openClassification(classification)

      history.pushState = jest.fn()
      wrapper.vm.closeClassification()
      expect(wrapper.vm.$data.currentClassification).toEqual({})
      expect(history.pushState.mock.calls.length).toBe(1)
      expect(history.pushState.mock.calls[0][0]).toBe(null)
      expect(history.pushState.mock.calls[0][1]).toBe(null)
      expect(history.pushState.mock.calls[0][2]).toBe('#')
    })
  })
})
