import { shallowMount } from '@vue/test-utils'
import EditClassification from 'Classifications/Edit.vue'

import axios from 'axios'
jest.mock('axios')

function mountComponent(options) {

  // mock csrf token
  document.getElementsByName = jest.fn(() => {
    return [
      { getAttribute: function() { return 'my-secret-token'} }
    ]
  })

  if (!options) {
    options = { propsData: { classification: { name: 'Classification 1', id: 1 } } }
  }

  return shallowMount(EditClassification, options)
}

describe('EditClassification', () => {

  it('sets the correct default data', () => {
    expect(typeof EditClassification.data).toBe('function')
    const defaultData = EditClassification.data()
    expect(defaultData.saving).toEqual(false)
    expect(defaultData.error).toEqual('')
    expect(defaultData.notice).toEqual('')
  })

  it('mounts properly', () => {
    const wrapper = mountComponent()
    expect(wrapper.text()).toContain('Classification 1')
  })

  it('mounts with defaults', () => {
    const wrapper = shallowMount(EditClassification)
    expect(wrapper.find('input[type="text"]').exists()).toBe(true)
  })

  it('displays error message', async () => {
    const wrapper = mountComponent()
    await wrapper.vm.$nextTick()
    wrapper.vm.$data.error = 'test error'
    await wrapper.vm.$nextTick()
    expect(wrapper.text()).toContain('test error')
  })
  
  describe('.close', () => {
    it('emits close signal', async () => {
      const wrapper = mountComponent()
      wrapper.vm.close()
      await wrapper.vm.$nextTick()
      expect(wrapper.emitted().close).toBeTruthy()
      expect(wrapper.emitted().close.length).toBe(1)
    })
  })
  
  describe('.save', () => {
    it('saves the classification', async () => {
      const wrapper = mountComponent()
      await wrapper.vm.$nextTick()

      axios.patch.mockResolvedValue({})

      expect.assertions(3)
      return wrapper.vm.save().then(function() {
        expect(axios.patch.mock.calls.length).toEqual(1)
        expect(axios.patch.mock.calls[0][0]).toEqual('/classifications/1.json')
        expect(axios.patch.mock.calls[0][1]).toEqual({ classification: { name: 'Classification 1', id: 1 }})
      });
    })

    it('handles errors', async () => {
      const wrapper = mountComponent()
      await wrapper.vm.$nextTick()

      axios.patch.mockRejectedValue('my error')

      expect.assertions(1)
      return wrapper.vm.save().then(function() {
        expect(wrapper.vm.$data.error).toEqual('my error')
      });
    })
  })
})
