global.window = window
global.$ = function() { return { focus() {} } }

import { shallowMount } from '@vue/test-utils'
import EditDocument from 'Documents/Edit.vue'

import axios from 'axios'
jest.mock('axios')

function mountDocument(document) {

  // mock axios request
  const defaultDocument = {
    id: 1,
    name: 'My Document',
    blocks: [],
  }
  const resp = { data: document || defaultDocument }
  axios.get.mockResolvedValue(resp)

  let wrapper = shallowMount(EditDocument, {
    stubs: [
      'tab-nav',
      'documents-permissions',
      'documents-edit-settings',
      'documents-edit-properties'
    ]
  })

  return wrapper
}

describe('EditDocument', () => {
  it('has a mounted hook', () => {
    expect(typeof EditDocument.mounted).toBe('function')
  })

  it('sets the correct default data', () => {
    expect(typeof EditDocument.data).toBe('function')
    const defaultData = EditDocument.data()
    expect(defaultData.drag).toEqual(false)
    expect(defaultData.isDecrypting).toEqual(false)
    expect(defaultData.decryptionError).toEqual(null)
    expect(defaultData.password).toEqual('')
  })

  it('mounts properly', () => {
    const wrapper = mountDocument()
    expect(wrapper.vm.$data.document.blocks.length).toEqual(0)
  })

  describe('.passwordName', () => {
    it('calculates unique name', () => {
      const wrapper = mountDocument()
      wrapper.vm.$data.document = { id: 1, blocks: [] }
      expect(wrapper.vm.passwordName).toEqual('document-1-password')
    })
  })

  describe('watcher', () => {
    describe('document.encrypted', () => {
      it('encrypts blocks', async () => {
        const wrapper = mountDocument()
        wrapper.vm.$data.document = { id: 1, encrypted: false, blocks: [ { encrypted: false } ] }
        expect(wrapper.vm.$data.document.encrypted).toEqual(false)
        expect(wrapper.vm.$data.document.blocks[0]['encrypted']).toEqual(false)
        wrapper.vm.$data.document.encrypted = true
        await wrapper.vm.$nextTick()
        expect(wrapper.vm.$data.document.encrypted).toEqual(true)
        expect(wrapper.vm.$data.document.blocks[0]['encrypted']).toEqual(true)
      })
    })
  })

  describe('.add', () => {
    it('adds a list', () => {
      const wrapper = mountDocument()
      wrapper.vm.add('List', 0)
      expect(wrapper.vm.$data.document.blocks.length).toEqual(1)
      expect(wrapper.vm.$data.document.blocks[0]['type']).toEqual('List')
      expect(wrapper.vm.$data.document.blocks[0]['kind']).toEqual('unordered')
      expect(wrapper.vm.$data.document.blocks[0]['content']).toEqual('<li></li>')
    })

    it('adds an image', () => {
      const wrapper = mountDocument()
      wrapper.vm.add('Image', 0)
      expect(wrapper.vm.$data.document.blocks.length).toEqual(1)
      expect(wrapper.vm.$data.document.blocks[0]['type']).toEqual('Image')
      expect(wrapper.vm.$data.document.blocks[0]['alignment']).toEqual('center')
    })

    it('adds a code block', () => {
      const wrapper = mountDocument()
      wrapper.vm.add('Code', 0)
      expect(wrapper.vm.$data.document.blocks.length).toEqual(1)
      expect(wrapper.vm.$data.document.blocks[0]['type']).toEqual('Code')
      expect(wrapper.vm.$data.document.blocks[0]['kind']).toEqual('ruby')
    })

    it('adds a table', () => {
      const wrapper = mountDocument()
      wrapper.vm.add('Table', 0)
      expect(wrapper.vm.$data.document.blocks.length).toEqual(1)
      expect(wrapper.vm.$data.document.blocks[0]['type']).toEqual('Table')
      expect(wrapper.vm.$data.document.blocks[0]['content']).toEqual('[]')
      expect(wrapper.vm.$data.document.blocks[0]['rows']).toEqual(2)
      expect(wrapper.vm.$data.document.blocks[0]['columns']).toEqual(3)
    })
  })

  describe('.remove', () => {
    it('adds a list', () => {
      const wrapper = mountDocument()
      wrapper.vm.$data.document.blocks = [
        { id: 1, type: 'Paragraph', content: 'My Paragraph' }
      ]
      expect(wrapper.vm.$data.document.blocks.length).toEqual(1)
      wrapper.vm.remove(0)
      expect(wrapper.vm.$data.document.blocks.length).toEqual(0)
    })
  })
})
