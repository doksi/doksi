import { shallowMount } from '@vue/test-utils'
import EditDocumentSettings from 'Documents/Edit/Settings.vue'

function mountComponent() {
  let propsData = { document: { blocks: [] } }
  return shallowMount(EditDocumentSettings, { propsData })
}

describe('EditDocumentSettings', () => {
  it('mounts properly', () => {
    let propsData = { document: { blocks: [], encrypted: true }, password: 's3cr3t' }
    const wrapper = shallowMount(EditDocumentSettings, { propsData })

    expect(wrapper.props('password')).toEqual('s3cr3t')

    let input = wrapper.find(".form-group label")
    expect(input.exists()).toBe(true)
    expect(input.text()).toBe('Visibility')
  })
  
  it('emits password when changed', () => {
    let emitMock = jest.fn()
    let options = {
      propsData: {
        document: {
          encrypted: true,
          blocks: []
        },
        password: 's3cr3t'
      },
      mocks: { '$emit': emitMock }
    }

    const wrapper = shallowMount(EditDocumentSettings, options)
    wrapper.vm.updatePassword()

    expect(emitMock.mock.calls.length).toBe(1)
    expect(emitMock.mock.calls[0][0]).toBe('input')
  })

  describe('refreshDataType', () => {
    it('sets to URL if there is one', () => {
      let propsData = { document: { data_url: 'http://example.com', data_string: '[1]' } }
      const wrapper = shallowMount(EditDocumentSettings, { propsData })
      wrapper.vm.refreshDataType()
      expect(wrapper.vm.$data.dynamicDataType).toEqual('URL')
    })

    it('sets to String if there is no URL', () => {
      let propsData = { document: { data_url: '', data_string: '[1]' } }
      const wrapper = shallowMount(EditDocumentSettings, { propsData })
      wrapper.vm.refreshDataType()
      expect(wrapper.vm.$data.dynamicDataType).toEqual('String')
    })
  })

  it('refreshesDataType when dynamic changes', async () => {
    let propsData = { document: { data_url: '', data_string: '[1]', dynamic: false } }
    const wrapper = shallowMount(EditDocumentSettings, { propsData })
    wrapper.setProps({ document: { dynamic: true, data_url: 'http://exmaple.com' } })
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.$data.dynamicDataType).toEqual('URL')
  })
})
