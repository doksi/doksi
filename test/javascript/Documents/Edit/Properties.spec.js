import { shallowMount } from '@vue/test-utils'
import EditDocumentProperties from 'Documents/Edit/Properties.vue'

describe('EditDocumentProperties', () => {
  it('mounts properly', () => {
    let options = {
      propsData: {
        document: {
          blocks: [],
          properties: [ { id: 1, name: 'MyProp', description: 'A prop', kind: 'String' } ]
        }
      },
      stubs: ['properties-assign']
    }
    const wrapper = shallowMount(EditDocumentProperties, options)
    let input = wrapper.find(".form-group input[type='text']")
    expect(input.exists()).toBe(true)
    expect(input.attributes('placeholder')).toBe('Copyright notice')
  })
})
