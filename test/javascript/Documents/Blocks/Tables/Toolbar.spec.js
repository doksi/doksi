import { shallowMount } from '@vue/test-utils'
import TableToolbar from 'Documents/Blocks/Tables/Toolbar.vue'

describe('TableToolbar', () => {
  it('mounts properly', () => {
    const wrapper = shallowMount(TableToolbar, { propsData: { block: { rows: 1, columns: 1 }}})
    let input = wrapper.find("input[type='number']")
    expect(input.exists()).toBe(true)
  })

  it('sets alignment', () => {
    const wrapper = shallowMount(TableToolbar, { propsData: { block: { rows: 1, columns: 1 }}})
    wrapper.vm.setAlignment('left')
    expect(wrapper.props('block').alignment).toBe('left')
  })

  it('toggles bold', () => {
    const wrapper = shallowMount(TableToolbar, { propsData: { block: { rows: 1, columns: 1 }}})
    document.execCommand = jest.fn()
    wrapper.vm.toggleBold()
    expect(document.execCommand.mock.calls.length).toBe(1)
    expect(document.execCommand.mock.calls[0][0]).toBe('bold')
  })

  it('toggles italic', () => {
    const wrapper = shallowMount(TableToolbar, { propsData: { block: { rows: 1, columns: 1 }}})
    document.execCommand = jest.fn()
    wrapper.vm.toggleItalic()
    expect(document.execCommand.mock.calls.length).toBe(1)
    expect(document.execCommand.mock.calls[0][0]).toBe('italic')
  })

  it('toggles strike', () => {
    const wrapper = shallowMount(TableToolbar, { propsData: { block: { rows: 1, columns: 1 }}})
    document.execCommand = jest.fn()
    wrapper.vm.toggleStrike()
    expect(document.execCommand.mock.calls.length).toBe(1)
    expect(document.execCommand.mock.calls[0][0]).toBe('strikethrough')
  })
})
