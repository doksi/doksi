import { shallowMount } from '@vue/test-utils'
import EditTableBlock from 'Documents/Blocks/Tables/Edit.vue'

function mountBlock(options) {
  if (!options) {
    options = {
      propsData: { block: { id: 1, content: '[[{"content": "foo"}]]', type: 'Table', columns: 1, rows: 1 } }
    }
  }
  let wrapper = shallowMount(EditTableBlock, options)
  return wrapper
}

describe('EditTableBlock', () => {
  it('has a mounted hook', () => {
    expect(typeof EditTableBlock.created).toBe('function')
  })

  it('sets the correct default data', () => {
    expect(typeof EditTableBlock.data).toBe('function')
    const defaultData = EditTableBlock.data()
    expect(defaultData.isBold).toEqual(false)
    expect(defaultData.isItalic).toEqual(false)
    expect(defaultData.isStrike).toEqual(false)
    expect(defaultData.ready).toEqual(false)
    expect(defaultData.cells).toEqual([[{}]])
  })

  it('mounts properly', () => {
    const wrapper = mountBlock()
    expect(wrapper.vm.$data.cells.length).toEqual(100)   // rows
    expect(wrapper.vm.$data.cells[0].length).toEqual(15) // columns
    expect(wrapper.vm.$data.cells[0][0]['content']).toEqual('foo')
  })

  it('mounts locked table properly', () => {
    let propsData = { block: { id: 1, content: '[]', type: 'Table', columns: 1, rows: 1, locked: true }}
    const wrapper = mountBlock({ propsData })
    expect(wrapper.props('block').content).toEqual('[]')
  })

  describe('watcher', () => {
    describe('rows', () => {
      it('refreshes content', async () => {
        const wrapper = mountBlock()
        wrapper.vm.refreshContent = jest.fn()
        wrapper.setProps({ block: { id: 1, rows: 2, columns: 1 } })
        await wrapper.vm.$nextTick()
        expect(wrapper.vm.refreshContent.mock.calls.length).toBeGreaterThan(0)
      })
    })

    describe('columns', () => {
      it('refreshes content', async () => {
        const wrapper = mountBlock()
        wrapper.vm.refreshContent = jest.fn()
        wrapper.setProps({ block: { id: 1, rows: 1, columns: 2 } })
        await wrapper.vm.$nextTick()
        expect(wrapper.vm.refreshContent.mock.calls.length).toBeGreaterThan(0)
      })
    })

    describe('block.locked', () => {
      it('refreshes cells', async () => {
        const wrapper = mountBlock()
        wrapper.vm.initCells = jest.fn()
        wrapper.setProps({ block: { id: 1, rows: 1, columns: 1, locked: true, content: '[]' } })
        await wrapper.vm.$nextTick()
        expect(wrapper.vm.initCells.mock.calls.length).toBeGreaterThan(0)
        expect(wrapper.vm.initCells.mock.calls[0][0]).toBe(100)
        expect(wrapper.vm.initCells.mock.calls[0][1]).toBe(15)
        expect(wrapper.vm.initCells.mock.calls[0][2]).toBe('[]')
      })
    })
  })

  describe('.formatChange', () => {
    it('defaults all formats to false', () => {
      const wrapper = mountBlock()
      wrapper.vm.$data.isBold = true
      wrapper.vm.$data.isItalic = true
      wrapper.vm.$data.isStrike = true
      wrapper.vm.formatChange({})
      expect(wrapper.vm.$data.isBold).toBeFalsy()
      expect(wrapper.vm.$data.isItalic).toBeFalsy()
      expect(wrapper.vm.$data.isStrike).toBeFalsy()
    })

    it('updates format data', () => {
      const wrapper = mountBlock()
      wrapper.vm.$data.isBold = false
      wrapper.vm.$data.isItalic = false
      wrapper.vm.$data.isStrike = false
      wrapper.vm.formatChange({ isBold: true, isItalic: true, isStrike: true })
      expect(wrapper.vm.$data.isBold).toBeTruthy()
      expect(wrapper.vm.$data.isItalic).toBeTruthy()
      expect(wrapper.vm.$data.isStrike).toBeTruthy()
    })
  })
})
