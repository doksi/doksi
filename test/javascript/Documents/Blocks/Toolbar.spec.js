import { shallowMount } from '@vue/test-utils'
import Toolbar from 'Documents/Blocks/Toolbar.vue'

describe('Toolbar', () => {
  it('mounts properly', () => {
    const wrapper = shallowMount(Toolbar, { propsData: { block: {}}})
    let input = wrapper.find("button")
    expect(input.exists()).toBe(true)
  })

  it('sets alignment', () => {
    const wrapper = shallowMount(Toolbar, { propsData: { block: {}}})
    wrapper.vm.setAlignment('left')
    expect(wrapper.props('block').alignment).toBe('left')
  })
})
