import { shallowMount } from '@vue/test-utils'
import EditRaw from 'Documents/Blocks/Raw/Edit.vue'

import axios from 'axios'
jest.mock('axios')

function mountComponent() {
  let block = {
    content: 'test',
    html: '<p>test</p>'
  }
  return shallowMount(EditRaw, { propsData: { block }, stubs: ['textarea-autosize']})
}

describe('EditRaw', () => {
  it('mounts correct', () => {
    const wrapper = mountComponent()
    expect(wrapper.find('div').exists()).toBe(true)
  })

  it('switches tab when losing focus', async () => {
    const wrapper = mountComponent()
    wrapper.vm.$data.currentTab = 'preview'
    expect(wrapper.vm.$data.currentTab).toEqual('preview')
    wrapper.setProps({ isFocused: false })
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.$data.currentTab).toEqual('edit')
  })

  it('generate preview when content changes', async () => {
    const wrapper = mountComponent()
    wrapper.vm.delayedGeneratePreview = jest.fn()

    wrapper.setProps({ block: { content: 'foo' } })
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.delayedGeneratePreview.mock.calls.length).toEqual(1)
  })

  it('generate preview', async () => {
    const resp = { data: { html: '<p>foo</p>' } }
    axios.get.mockResolvedValue(resp)

    const wrapper = mountComponent()
    expect.assertions(1)
    return wrapper.vm.generatePreview().then(function() {
      expect(wrapper.vm.$data.html).toEqual('<p>foo</p>')
    })
  })

  it('handles error generating preview', async () => {
    axios.get.mockRejectedValue('my error')
    console.error = jest.fn()

    const wrapper = mountComponent()
    expect.assertions(2)
    return wrapper.vm.generatePreview().then(function() {
      expect(console.error).toHaveBeenCalledTimes(1)
      expect(console.error).toHaveBeenCalledWith('my error')
    })
  })
})
