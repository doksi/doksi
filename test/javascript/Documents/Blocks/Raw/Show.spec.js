import { shallowMount } from '@vue/test-utils'
import RawBlock from 'Documents/Blocks/Raw/Show.vue'

describe('RawBlock', () => {
  it('mounts correct', () => {
    let block = {
      content: 'test',
      html: '<p>test</p>'
    }
    const wrapper = shallowMount(RawBlock, { propsData: { block }})
    expect(wrapper.text()).toContain('test')
  })
})
