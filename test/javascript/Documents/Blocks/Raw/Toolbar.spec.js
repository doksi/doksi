import { shallowMount } from '@vue/test-utils'
import Toolbar from 'Documents/Blocks/Raw/Toolbar.vue'

describe('RawToolbar', () => {
  it('mounts properly', () => {
    const wrapper = shallowMount(Toolbar, { propsData: { block: {}}})
    expect(wrapper.find("nav").exists()).toBeTruthy()
  })

  it('emits tab switch', () => {
    const wrapper = shallowMount(Toolbar)
    wrapper.vm.switchTab('preview')
    expect(wrapper.emitted().input).toBeTruthy()
    expect(wrapper.emitted().input.length).toBe(1)
    expect(wrapper.emitted().input[0]).toEqual(['preview'])
  })
})
