import { shallowMount } from '@vue/test-utils'
import AddButton from 'Documents/Blocks/AddButton.vue'

describe('AddButton', () => {
  it('mounts correct', () => {
    const wrapper = shallowMount(AddButton)
    expect(wrapper.text()).toContain('Paragraph')
    expect(wrapper.text()).toContain('Heading')
    expect(wrapper.text()).toContain('List')
    expect(wrapper.text()).toContain('Image')
    expect(wrapper.text()).toContain('Quote')
    expect(wrapper.text()).toContain('Table')
    expect(wrapper.text()).toContain('Code')
    expect(wrapper.text()).toContain('Raw')
  })

  it('emits click', () => {
    const wrapper = shallowMount(AddButton)
    wrapper.vm.click('paragraph')
    expect(wrapper.emitted().click).toBeTruthy()
    expect(wrapper.emitted().click.length).toBe(1)
    expect(wrapper.emitted().click[0]).toEqual(['paragraph'])
  })
})
