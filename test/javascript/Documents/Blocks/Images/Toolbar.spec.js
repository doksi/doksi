import { shallowMount } from '@vue/test-utils'
import Toolbar from 'Documents/Blocks/Images/Toolbar.vue'

describe('ImageToolbar', () => {
  it('mounts properly', () => {
    const wrapper = shallowMount(Toolbar, { propsData: { block: {}}})
    let input = wrapper.find("button")
    expect(input.exists()).toBe(true)
  })

  it('sets alignment', () => {
    const wrapper = shallowMount(Toolbar, { propsData: { block: {}}})
    wrapper.vm.setAlignment('left')
    expect(wrapper.props('block').alignment).toBe('left')
  })

  it('sets kind', () => {
    const wrapper = shallowMount(Toolbar, { propsData: { block: {}}})
    wrapper.vm.setKind('foo')
    expect(wrapper.props('block').kind).toBe('foo')
  })

  it('sets image from file', () => {
    const wrapper = shallowMount(Toolbar, { propsData: { block: {}}})
    FileReader.prototype.readAsDataURL = jest.fn()
    wrapper.vm.setFile({ target: { files: ['my-file'] } })
    expect(FileReader.prototype.readAsDataURL.mock.calls.length).toBe(1)
    expect(FileReader.prototype.readAsDataURL.mock.calls[0][0]).toBe('my-file')
  })

  describe('.kind', () => {
    it('returns block.kind', () => {
      const wrapper = shallowMount(Toolbar, { propsData: { block: { kind: 'foo' }}})
      expect(wrapper.vm.kind).toEqual('foo')
    })

    it('defaults to url', () => {
      const wrapper = shallowMount(Toolbar, { propsData: { block: {} }})
      expect(wrapper.vm.kind).toEqual('url')
    })
  })
})
