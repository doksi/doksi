import { shallowMount } from '@vue/test-utils'
import Toolbar from 'Documents/Blocks/Code/Toolbar.vue'

describe('CodeToolbar', () => {
  it('mounts properly', () => {
    const wrapper = shallowMount(Toolbar, { propsData: { block: {}}})
    let input = wrapper.find("button")
    expect(input.exists()).toBe(true)
  })

  it('sets language', () => {
    const wrapper = shallowMount(Toolbar, { propsData: { block: {}}})
    wrapper.vm.setLanguage('ruby')
    expect(wrapper.props('block').kind).toBe('ruby')
  })
})
