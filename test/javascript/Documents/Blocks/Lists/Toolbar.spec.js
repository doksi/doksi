import { shallowMount } from '@vue/test-utils'
import Toolbar from 'Documents/Blocks/Lists/Toolbar.vue'

describe('ListToolbar', () => {
  it('mounts properly', () => {
    const wrapper = shallowMount(Toolbar, { propsData: { block: {}}})
    let input = wrapper.find("button")
    expect(input.exists()).toBe(true)
  })

  it('sets alignment', () => {
    const wrapper = shallowMount(Toolbar, { propsData: { block: {}}})
    wrapper.vm.setAlignment('left')
    expect(wrapper.props('block').alignment).toBe('left')
  })

  it('sets kind', () => {
    const wrapper = shallowMount(Toolbar, { propsData: { block: {}}})
    wrapper.vm.setKind('unordered')
    expect(wrapper.props('block').kind).toBe('unordered')
  })
})
