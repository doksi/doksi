import { shallowMount } from '@vue/test-utils'
import Toolbar from 'Documents/Blocks/Headings/Toolbar.vue'

describe('HeadingToolbar', () => {
  it('mounts properly', () => {
    const wrapper = shallowMount(Toolbar, { propsData: { block: {}}})
    let input = wrapper.find("button")
    expect(input.exists()).toBe(true)
  })

  it('sets alignment', () => {
    const wrapper = shallowMount(Toolbar, { propsData: { block: {}}})
    wrapper.vm.setAlignment('left')
    expect(wrapper.props('block').alignment).toBe('left')
  })

  it('sets level', () => {
    const wrapper = shallowMount(Toolbar, { propsData: { block: {}}})
    wrapper.vm.setLevel(3)
    expect(wrapper.props('block').level).toBe(3)
  })
})
