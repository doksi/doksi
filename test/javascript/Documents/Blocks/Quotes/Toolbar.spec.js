import { shallowMount } from '@vue/test-utils'
import Toolbar from 'Documents/Blocks/Quotes/Toolbar.vue'

describe('QuoteToolbar', () => {
  it('mounts properly', () => {
    const wrapper = shallowMount(Toolbar, { propsData: { block: {}}})
    let input = wrapper.find("input[type='text']")
    expect(input.exists()).toBe(true)
  })

  it('sets alignment', () => {
    const wrapper = shallowMount(Toolbar, { propsData: { block: {}}})
    wrapper.vm.setAlignment('left')
    expect(wrapper.props('block').alignment).toBe('left')
  })

  it('toggles bold', () => {
    const wrapper = shallowMount(Toolbar, { propsData: { block: {}}})
    document.execCommand = jest.fn()
    wrapper.vm.toggleBold()
    expect(document.execCommand.mock.calls.length).toBe(1)
    expect(document.execCommand.mock.calls[0][0]).toBe('bold')
  })

  it('toggles italic', () => {
    const wrapper = shallowMount(Toolbar, { propsData: { block: {}}})
    document.execCommand = jest.fn()
    wrapper.vm.toggleItalic()
    expect(document.execCommand.mock.calls.length).toBe(1)
    expect(document.execCommand.mock.calls[0][0]).toBe('italic')
  })

  it('toggles strike', () => {
    const wrapper = shallowMount(Toolbar, { propsData: { block: {}}})
    document.execCommand = jest.fn()
    wrapper.vm.toggleStrike()
    expect(document.execCommand.mock.calls.length).toBe(1)
    expect(document.execCommand.mock.calls[0][0]).toBe('strikethrough')
  })
})
