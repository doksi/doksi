global.window = window
global.$ = function() { return { focus() {} } }

import { shallowMount } from '@vue/test-utils'
import NewDocument from 'Documents/New.vue'

import axios from 'axios'
jest.mock('axios')

function mountDocument(document) {

  // mock axios request
  const defaultDocument = {
    id: 1,
    title: 'My Document',
    blocks: [],
  }
  const resp = { data: document || defaultDocument }
  axios.get.mockResolvedValue(resp)

  let wrapper = shallowMount(NewDocument, {
    stubs: [
      'documents-permissions',
      'documents-edit-settings',
      'documents-edit-properties'
    ]
  })

  return wrapper
}

describe('NewDocument', () => {
  it('sets the correct default data', () => {
    expect(typeof NewDocument.data).toBe('function')
    const defaultData = NewDocument.data()
    expect(defaultData.drag).toEqual(false)
  })

  it('mounts properly', () => {
    const wrapper = mountDocument()
    expect(wrapper.vm.$data.document.blocks.length).toEqual(0)
  })

  describe('.add', () => {
    it('adds a list', () => {
      const wrapper = mountDocument()
      wrapper.vm.add('List', 0)
      expect(wrapper.vm.$data.document.blocks.length).toEqual(1)
      expect(wrapper.vm.$data.document.blocks[0]['type']).toEqual('List')
      expect(wrapper.vm.$data.document.blocks[0]['kind']).toEqual('unordered')
      expect(wrapper.vm.$data.document.blocks[0]['content']).toEqual('<li></li>')
    })

    it('adds an image', () => {
      const wrapper = mountDocument()
      wrapper.vm.add('Image', 0)
      expect(wrapper.vm.$data.document.blocks.length).toEqual(1)
      expect(wrapper.vm.$data.document.blocks[0]['type']).toEqual('Image')
      expect(wrapper.vm.$data.document.blocks[0]['alignment']).toEqual('center')
    })

    it('adds a code block', () => {
      const wrapper = mountDocument()
      wrapper.vm.add('Code', 0)
      expect(wrapper.vm.$data.document.blocks.length).toEqual(1)
      expect(wrapper.vm.$data.document.blocks[0]['type']).toEqual('Code')
      expect(wrapper.vm.$data.document.blocks[0]['kind']).toEqual('ruby')
    })

    it('adds a table', () => {
      const wrapper = mountDocument()
      wrapper.vm.add('Table', 0)
      expect(wrapper.vm.$data.document.blocks.length).toEqual(1)
      expect(wrapper.vm.$data.document.blocks[0]['type']).toEqual('Table')
      expect(wrapper.vm.$data.document.blocks[0]['content']).toEqual('[]')
      expect(wrapper.vm.$data.document.blocks[0]['rows']).toEqual(2)
      expect(wrapper.vm.$data.document.blocks[0]['columns']).toEqual(3)
    })
  })

  describe('.remove', () => {
    it('adds a list', () => {
      const wrapper = mountDocument()
      wrapper.vm.$data.document.blocks = [
        { id: 1, type: 'Paragraph', content: 'My Paragraph' }
      ]
      expect(wrapper.vm.$data.document.blocks.length).toEqual(1)
      wrapper.vm.remove(0)
      expect(wrapper.vm.$data.document.blocks.length).toEqual(0)
    })
  })
})
