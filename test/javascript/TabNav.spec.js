global.window = window
global.$ = function() {
  return { tab() {} }
}

import { shallowMount } from '@vue/test-utils'
import TabNav from 'TabNav.vue'

describe('TabNav', () => {
  it('has a mounted hook', () => {
    expect(typeof TabNav.mounted).toBe('function')
  })

  it('mounts properly', () => {
    const wrapper = shallowMount(TabNav)
    expect(wrapper.find('nav.nav').exists()).toBe(true)
  })

  it('sets explicit tab when mounted', () => {
    global.$ = jest.fn(() => { return { tab: jest.fn() } })
    document.location.hash = '#bar'
    const wrapper = shallowMount(TabNav, { propsData: {
      tabs: [{ name: 'foo', label: 'Foo' }, { name: 'bar', label: 'Bar' }]
    }})
    expect(wrapper.find('a.nav-link#fooTab').exists()).toBe(true)
    expect(wrapper.find('a.nav-link#fooTab').text()).toBe('Foo')
    expect(wrapper.find('a.nav-link#barTab').exists()).toBe(true)
    expect(wrapper.find('a.nav-link#barTab').text()).toBe('Bar')
    expect(global.$.mock.calls.length).toBe(1)
    expect(global.$.mock.calls[0][0]).toBe('a#barTab')
  })

  it('sets default tab when mounted', () => {
    global.$ = jest.fn(() => { return { tab: jest.fn() } })
    document.location.hash = ''
    const wrapper = shallowMount(TabNav, { propsData: {
      tabs: [{ name: 'foo', label: 'Foo' }, { name: 'bar', label: 'Bar' }]
    }})
    expect(wrapper.find('a.nav-link#fooTab').exists()).toBe(true)
    expect(wrapper.find('a.nav-link#fooTab').text()).toBe('Foo')
    expect(wrapper.find('a.nav-link#barTab').exists()).toBe(true)
    expect(wrapper.find('a.nav-link#barTab').text()).toBe('Bar')
    expect(global.$.mock.calls.length).toBe(1)
    expect(global.$.mock.calls[0][0]).toBe('a#fooTab')
  })

  describe('switchTab', () => {

    it('switches tab', async () => {
      const wrapper = shallowMount(TabNav, { propsData: {
        tabs: [{ name: 'mytab', label: 'MyTab' }]
      }})
      let tabMock = jest.fn()
      history.pushState = jest.fn()
      global.$ = jest.fn(() => { return { tab: tabMock } })
      wrapper.vm.switchTab('#mytab')
      expect(tabMock.mock.calls.length).toBe(1)
      expect(tabMock.mock.calls[0][0]).toBe('show')
      expect(history.pushState.mock.calls.length).toBe(1)
      expect(history.pushState.mock.calls[0][0]).toBe(null)
      expect(history.pushState.mock.calls[0][1]).toBe(null)
      expect(history.pushState.mock.calls[0][2]).toBe('#mytab')
    })

    it('skips switch when incorrect tab', async () => {
      const wrapper = shallowMount(TabNav, { propsData: {
        tabs: [{ name: 'mytab', label: 'MyTab' }]
      }})
      history.pushState = jest.fn()
      global.$ = jest.fn(() => { return null })
      wrapper.vm.switchTab('#invalid')
      expect(history.pushState.mock.calls.length).toBe(0)
    })
  })
})
