import { shallowMount } from '@vue/test-utils'
import Autocomplete from 'Autocomplete.vue'

const wrapper = shallowMount(Autocomplete)

function getMountedComponent(Component, propsData) {
  return shallowMount(Component, {
    propsData
  })
}

describe('Autocomplete', () => {
  it('has a mounted hook', () => {
    expect(typeof Autocomplete.mounted).toBe('function')
  })

  it('has a destroyed hook', () => {
    expect(typeof Autocomplete.destroyed).toBe('function')
  })

  it('sets the correct default data', () => {
    expect(typeof Autocomplete.data).toBe('function')
    const defaultData = Autocomplete.data()
    expect(defaultData.isOpen).toBe(false)
    expect(defaultData.value).toBe('')
    expect(defaultData.arrowCounter).toBe(0)
  })

  it('correctly sets the addEventListener when mounted', () => {
    document.addEventListener = jest.fn()
    const autocomplete = getMountedComponent(Autocomplete)
    expect(document.addEventListener).toHaveBeenCalledTimes(1)
  })

  it('correctly removes the addEventListener when destroyed', () => {
    document.removeEventListener = jest.fn()
    const autocomplete = getMountedComponent(Autocomplete)
    autocomplete.destroy()
    expect(document.removeEventListener).toHaveBeenCalledTimes(1)
  })

  it('renders the correct message', () => {
    expect(wrapper.text()).toBe('No matches found.')
  })

  describe('.onChange', () => {
    it('sets a string', () => {
      function getResults() {
        return new Promise((resolve, reject) => {
          expect(autocomplete.vm.$data.isLoading).toBe(true)
          resolve(['one', 'two'])
        })
      }
      const autocomplete = getMountedComponent(Autocomplete, { getResults: getResults })
      expect.assertions(4)
      return autocomplete.vm.onChange().then(data => {
        expect(autocomplete.vm.$data.results[0]).toBe('one')
        expect(autocomplete.vm.$data.results[1]).toBe('two')
        expect(autocomplete.vm.$data.isLoading).toBe(false)
      });
    })
    it('handlers an error', () => {
      function getResults() {
        return new Promise((resolve, reject) => {
          expect(autocomplete.vm.$data.isLoading).toBe(true)
          reject('my error')
        })
      }
      const autocomplete = getMountedComponent(Autocomplete, { getResults: getResults })

      console.error = jest.fn()

      expect.assertions(5)
      return autocomplete.vm.onChange().then(data => {
        expect(autocomplete.vm.$data.results).toEqual([])
        expect(autocomplete.vm.$data.isLoading).toBe(false)
        expect(console.error).toHaveBeenCalledTimes(1)
        expect(console.error).toHaveBeenCalledWith('my error')
      });
    })
  })

  describe('.setResult', () => {
    it('sets a string', () => {
      const result = 'test'
      wrapper.vm.setResult(result)
      expect(wrapper.vm.$data.value).toBe('test')
      expect(wrapper.vm.displayValue).toBe('test')
      expect(wrapper.vm.isOpen).toBe(false)
    })
    it('sets an object', () => {
      const result = {
        'foo': 'fooValue',
        'name': 'nameValue'
      }
      wrapper.vm.setResult(result)
      expect(wrapper.vm.$data.value).toBe(result)
      expect(wrapper.vm.displayValue).toBe('nameValue')
      expect(wrapper.vm.isOpen).toBe(false)
    })
  })

  describe('.renderResult', () => {
    it('renders a string', () => {
      expect(wrapper.vm.renderResult('test')).toBe('test')
    })
    it('renders an object', () => {
      const result = {
        'foo': 'fooValue',
        'name': 'nameValue'
      }
      expect(wrapper.vm.renderResult(result)).toBe('nameValue')
    })
  })

  describe('.onArrowDown', () => {
    it('steps up counter', () => {
      wrapper.vm.$data.results = ['one', 'two']
      wrapper.vm.$data.arrowCounter = 0
      wrapper.vm.onArrowDown(null)
      expect(wrapper.vm.$data.arrowCounter).toBe(1)
    })
    it('stops at end', () => {
      wrapper.vm.$data.results = ['one', 'two']
      wrapper.vm.$data.arrowCounter = 1
      wrapper.vm.onArrowDown(null)
      expect(wrapper.vm.$data.arrowCounter).toBe(1)
    })
  })

  describe('.onArrowUp', () => {
    it('steps down counter', () => {
      wrapper.vm.$data.results = ['one', 'two']
      wrapper.vm.$data.arrowCounter = 1
      wrapper.vm.onArrowUp(null)
      expect(wrapper.vm.$data.arrowCounter).toBe(0)
    })
    it('stops at start', () => {
      wrapper.vm.$data.results = ['one', 'two']
      wrapper.vm.$data.arrowCounter = 0
      wrapper.vm.onArrowUp(null)
      expect(wrapper.vm.$data.arrowCounter).toBe(0)
    })
  })

  describe('.onEnter', () => {
    it('sets value from results', () => {
      wrapper.vm.$data.results = ['myTest']
      wrapper.vm.$data.arrowCounter = 0
      wrapper.vm.onEnter(null)
      expect(wrapper.vm.$data.value).toBe('myTest')
    })
  })

  describe('.handleClickOutside', () => {
    it('sets value from results', () => {
      wrapper.vm.$data.isOpen = true
      wrapper.vm.$data.arrowCounter = 0
      wrapper.vm.$el.contains = jest.fn(() => { return false })
      wrapper.vm.handleClickOutside({ target: 'foobar' })
      expect(wrapper.vm.$data.isOpen).toBe(false)
      expect(wrapper.vm.$data.arrowCounter).toBe(-1)
    })
    it('skips when click is not outside', () => {
      wrapper.vm.$data.isOpen = true
      wrapper.vm.$data.arrowCounter = 1
      wrapper.vm.$el.contains = jest.fn(() => { return true })
      wrapper.vm.handleClickOutside({ target: 'foobar' })
      expect(wrapper.vm.$data.isOpen).toBe(true)
      expect(wrapper.vm.$data.arrowCounter).toBe(1)
    })
  })
})
