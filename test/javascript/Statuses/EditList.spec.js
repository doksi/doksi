import { shallowMount } from '@vue/test-utils'
import EditStatuses from 'Statuses/EditList.vue'

import axios from 'axios'
jest.mock('axios')

function mountForm(response) {

  // mock axios request
  const defaultResponse = [
    { name: 'TestClass', id: 1 },
    { name: 'AnotherClass', id: 2 }
  ]
  const resp = { data: response || defaultResponse }
  axios.get.mockResolvedValue(resp)

  // mock csrf token
  document.getElementsByName = jest.fn(() => {
    return [
      { getAttribute: function() { return 'my-secret-token'} }
    ]
  })

  return shallowMount(EditStatuses, { propsData: { url: 'http://example.com' } })
}

describe('EditStatuses', () => {
  it('has a created hook', () => {
    expect(typeof EditStatuses.created).toBe('function')
  })

  it('sets the correct default data', () => {
    expect(typeof EditStatuses.data).toBe('function')
    const defaultData = EditStatuses.data()
    expect(defaultData.isAddingStatus).toEqual(false)
    expect(defaultData.isLoading).toEqual(true)
    expect(defaultData.statuses).toEqual([])
    expect(defaultData.errorMessage).toEqual('')
    expect(defaultData.statusToAdd).toEqual('')
  })

  it('mounts properly', () => {
    const wrapper = mountForm()
    expect(wrapper.vm.$data.statuses.length).toEqual(0)
  })

  it('displays error message', async () => {
    const wrapper = shallowMount(EditStatuses, { propsData: { url: '' } })
    await wrapper.vm.$nextTick()
    wrapper.vm.$data.errorMessage = 'test error'
    await wrapper.vm.$nextTick()
    expect(wrapper.text()).toContain('test error')

  })

  describe('getting status list', () => {

    it('sets statuses from URL', async () => {
      const statuses = [ { id: 1, name: 'TestStatus' } ]
      const resp = { data: statuses }
      axios.get.mockResolvedValue(resp)

      const url = 'https://example.com/statuses.json'
      const wrapper = shallowMount(EditStatuses, { propsData: { url: url } })
      await wrapper.vm.$nextTick()
      expect(wrapper.vm.$data.statuses.length).toEqual(1)
      expect(wrapper.vm.$data.statuses[0]['id']).toEqual(1)
      expect(wrapper.vm.$data.statuses[0]['name']).toEqual('TestStatus')
    })

    it('handle API errors', async () => {
      axios.get.mockRejectedValue('my error')
      console.error = jest.fn()
  
      const url = 'https://example.com/statuses.json'
      const wrapper = shallowMount(EditStatuses, { propsData: { url: url } })
      await wrapper.vm.$nextTick()
      expect(wrapper.vm.$data.statuses.length).toEqual(0)
      expect(wrapper.vm.$data.errorMessage).toEqual('my error')
    })
  })
  
  describe('.addStatus', () => {
    it('adds a status', async () => {
      const wrapper = mountForm()
      await wrapper.vm.$nextTick()

      const resp = { data: { name: 'TestStatus', id: 420 } }
      axios.post.mockResolvedValue(resp)

      expect(wrapper.vm.$data.statuses.length).toEqual(2)
      expect.assertions(4)
      return wrapper.vm.addStatus().then(function() {
        expect(wrapper.vm.$data.statuses.length).toEqual(3)
        expect(wrapper.vm.$data.statuses[2]['name']).toEqual('TestStatus')
        expect(wrapper.vm.$data.statuses[2]['id']).toEqual(420)
      });
    })

    it('handles errors', async () => {
      const wrapper = mountForm()
      await wrapper.vm.$nextTick()

      axios.post.mockRejectedValue('my error')

      expect(wrapper.vm.$data.statuses.length).toEqual(2)
      expect.assertions(3)
      return wrapper.vm.addStatus().then(function() {
        expect(wrapper.vm.$data.statuses.length).toEqual(2)
        expect(wrapper.vm.$data.errorMessage).toEqual('my error')
      });
    })
  })
  
  describe('.removeStatus', () => {
    it('removes a status', async () => {
      const wrapper = mountForm()
      await wrapper.vm.$nextTick()

      axios.delete.mockResolvedValue({})

      expect(wrapper.vm.$data.statuses.length).toEqual(2)
      expect.assertions(2)
      return wrapper.vm.removeStatus(1).then(function() {
        expect(wrapper.vm.$data.statuses.length).toEqual(1)
      });
    })

    it('handles errors', async () => {
      const wrapper = mountForm()
      await wrapper.vm.$nextTick()

      axios.delete.mockRejectedValue('my error')

      expect(wrapper.vm.$data.statuses.length).toEqual(2)
      expect.assertions(3)
      return wrapper.vm.removeStatus(1).then(function() {
        expect(wrapper.vm.$data.statuses.length).toEqual(1)
        expect(wrapper.vm.$data.errorMessage).toEqual('my error')
      });
    })
  })
  
  describe('.openStatus', () => {
    it('opens a status', async () => {
      const wrapper = mountForm()
      await wrapper.vm.$nextTick()

      let status = wrapper.vm.$data.statuses[0]
      history.pushState = jest.fn()
      wrapper.vm.openStatus(status)
      expect(wrapper.vm.$data.currentStatus).toBe(status)
      expect(history.pushState.mock.calls.length).toBe(1)
      expect(history.pushState.mock.calls[0][0]).toBe(null)
      expect(history.pushState.mock.calls[0][1]).toBe(null)
      expect(history.pushState.mock.calls[0][2]).toBe('#1')
    })
  })
  
  describe('.closeStatus', () => {
    it('closes a status', async () => {
      const wrapper = mountForm()
      await wrapper.vm.$nextTick()

      let status = wrapper.vm.$data.statuses[0]
      wrapper.vm.openStatus(status)

      history.pushState = jest.fn()
      wrapper.vm.closeStatus()
      expect(wrapper.vm.$data.currentStatus).toEqual({})
      expect(history.pushState.mock.calls.length).toBe(1)
      expect(history.pushState.mock.calls[0][0]).toBe(null)
      expect(history.pushState.mock.calls[0][1]).toBe(null)
      expect(history.pushState.mock.calls[0][2]).toBe('#')
    })
  })
})
