import { shallowMount } from '@vue/test-utils'
import AssignProperties from 'Properties/Assign.vue'

describe('AssignProperties', () => {
  it('mounts properly', () => {
    let document = {
      properties: [
        { id: 1, name: 'MyProp', kind: 'String', default: 'MyValue', description: 'A property' }
      ]
    }
    const wrapper = shallowMount(AssignProperties, { propsData: { document: document }})
    let input = wrapper.find(".form-group input[type='text']")
    expect(input.exists()).toBe(true)
    expect(input.attributes('placeholder')).toBe('A property')

    let label = wrapper.find(".form-group label")
    expect(label.exists()).toBe(true)
    expect(label.text()).toBe('MyProp')
  })
})
