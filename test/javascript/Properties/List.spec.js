import { shallowMount } from '@vue/test-utils'
import PropertyList from 'Properties/List.vue'

describe('PropertyList', () => {
  it('mounts properly', () => {
    const wrapper = shallowMount(PropertyList)
    expect(wrapper.find("dl").exists()).toBe(true)
  })

  it('creates list', () => {
    let properties = [
      { id: 1, name: 'MyProp', kind: 'String', default: 'MyValue', value: 'Foo' }
    ]
    const wrapper = shallowMount(PropertyList, { propsData: { properties }})
    let dt = wrapper.find("dl dt")
    expect(dt.exists()).toBe(true)
    expect(dt.text()).toBe('MyProp')
    let dd = wrapper.find("dl dd")
    expect(dd.exists()).toBe(true)
    expect(dd.text()).toBe('Foo')
  })
})
