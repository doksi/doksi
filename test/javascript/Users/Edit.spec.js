global.window = window
global.$ = function() {
  return { tab() {} }
}

import { shallowMount } from '@vue/test-utils'
import EditUser from 'Users/Edit.vue'
import Consumer from 'channels/consumer'

import axios from 'axios'
jest.mock('axios')

function mountUser(user) {
  const stubs = ['edit-statuses', 'classifications-edit-list', 'edit-properties']

  // mock axios request
  const defaultUser = { fullname: 'Alice', id: 420, classifications: [], statuses: [], properties: [] }
  const resp = { data: user || defaultUser }
  axios.get.mockResolvedValue(resp)

  // mock csrf token
  document.getElementsByName = jest.fn(() => {
    return [
      { getAttribute: function() { return 'my-secret-token'} }
    ]
  })

  return shallowMount(EditUser, { stubs: stubs })
}

describe('EditUser', () => {
  it('has a created hook', () => {
    expect(typeof EditUser.created).toBe('function')
  })

  it('sets the correct default data', () => {
    expect(typeof EditUser.data).toBe('function')
    const defaultData = EditUser.data()
    expect(defaultData.isLoading).toEqual(true)
  })

  it('mounts properly', () => {
    const wrapper = mountUser()
    expect(wrapper.vm.$data.user.classifications.length).toEqual(0)
  })

  describe('getting user data', () => {

    it('sets user from URL', async () => {
      const user = { fullname: 'Alice', id: 42, classifications: [], statuses: [], properties: [] }
      const resp = { data: user }
      axios.get.mockResolvedValue(resp)

      const url = 'https://example.com/users.json'
      const stubs = ['edit-statuses', 'classifications-edit-list', 'edit-properties']
      const wrapper = shallowMount(EditUser, { propsData: { url: url }, stubs: stubs })
      await wrapper.vm.$nextTick()
      expect(wrapper.vm.$data.user.fullname).toEqual('Alice')
      expect(wrapper.vm.$data.user.id).toEqual(42)
    })

    it('handle API errors', async () => {
      axios.get.mockRejectedValue('my error')
      console.error = jest.fn()
  
      const url = 'https://example.com/users.json'
      const stubs = ['edit-statuses', 'classifications-edit-list', 'edit-properties']
      const wrapper = shallowMount(EditUser, { propsData: { url: url }, stubs: stubs })
      await wrapper.vm.$nextTick()
      expect(wrapper.vm.$data.user.fullname).toEqual(undefined)
      expect(wrapper.vm.$data.error).toEqual('my error')
    })
  })

  describe('switchTab', () => {

    it('switches tab', async () => {
      const wrapper = mountUser()
      let tabMock = jest.fn()
      history.pushState = jest.fn()
      global.$ = jest.fn(() => { return { tab: tabMock } })
      wrapper.vm.switchTab('#profile')
      expect(tabMock.mock.calls.length).toBe(1)
      expect(tabMock.mock.calls[0][0]).toBe('show')
      expect(history.pushState.mock.calls.length).toBe(1)
      expect(history.pushState.mock.calls[0][0]).toBe(null)
      expect(history.pushState.mock.calls[0][1]).toBe(null)
      expect(history.pushState.mock.calls[0][2]).toBe('#profile')
    })

    it('skips switch when incorrect tab', async () => {
      const wrapper = mountUser()
      history.pushState = jest.fn()
      global.$ = jest.fn(() => { return null })
      wrapper.vm.switchTab('#invalid')
      expect(history.pushState.mock.calls.length).toBe(0)
    })
  })

  it('receives channel data', () => {
    const newUser = {
      id: 1,
      fullname: 'UpdatedUser',
      classifications: [],
      statuses: [],
      documents: []
    }
    Consumer.subscriptions.create = jest.fn((spec, callbacks) => {
      callbacks.received({ user: newUser })
    })
    const wrapper = mountUser()
    expect(wrapper.vm.$data.user.fullname).toEqual('UpdatedUser')
  })
})
