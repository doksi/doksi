import { shallowMount } from '@vue/test-utils'
import UserItem from 'Users/Item.vue'

describe('UserItem', () => {
  it('mounts properly', () => {
    const wrapper = shallowMount(UserItem)
    expect(wrapper.find("a").exists()).toBe(true)
  })

  it('creates link', () => {
    let user = { id: 1, fullname: 'My User', show_url: '/foo' }
    const wrapper = shallowMount(UserItem, { propsData: { user }})
    let link = wrapper.find("a")
    expect(link.exists()).toBe(true)
    expect(link.text()).toBe('My User')
    expect(link.attributes('href')).toBe('/foo')
  })
})
