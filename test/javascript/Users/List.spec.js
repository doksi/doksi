import { shallowMount } from '@vue/test-utils'
import UserList from 'Users/List.vue'
import axios from 'axios'

jest.mock('axios')

describe('UserList', () => {
  it('has a created hook', () => {
    expect(typeof UserList.created).toBe('function')
  })

  it('sets the correct default data', () => {
    expect(typeof UserList.data).toBe('function')
    const defaultData = UserList.data()
    expect(defaultData.users).toEqual([])
  })

  it('sets users from property', () => {
    const users = [ { id: 1, fullname: 'Alice', documents: [], teams: [] } ]
    const wrapper = shallowMount(UserList, { propsData: { json: JSON.stringify(users) } })
    expect(wrapper.vm.$data.users.length).toEqual(1)
    expect(wrapper.vm.$data.users[0]['id']).toEqual(1)
    expect(wrapper.vm.$data.users[0]['fullname']).toEqual('Alice')
  })

  it('sets users from URL', async () => {
    const users = [ { id: 1, fullname: 'Alice', documents: [], teams: [] } ]
    const resp = { data: users }
    axios.get.mockResolvedValue(resp)

    const url = 'https://example.com/users.json'
    const wrapper = shallowMount(UserList, { propsData: { url: url } })
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.$data.users.length).toEqual(1)
    expect(wrapper.vm.$data.users[0]['id']).toEqual(1)
    expect(wrapper.vm.$data.users[0]['fullname']).toEqual('Alice')
  })

  it('sets users without URL', async () => {
    const users = [ { id: 1, fullname: 'Alice', documents: [], teams: [] } ]
    const resp = { data: users }
    axios.get.mockResolvedValue(resp)

    const wrapper = shallowMount(UserList)
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.$data.users.length).toEqual(1)
    expect(wrapper.vm.$data.users[0]['id']).toEqual(1)
    expect(wrapper.vm.$data.users[0]['fullname']).toEqual('Alice')
  })

  it('handle API errors', async () => {
    axios.get.mockRejectedValue('my error')
    console.error = jest.fn()

    const url = 'https://example.com/users.json'
    const wrapper = shallowMount(UserList, { propsData: { url: url } })
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.$data.users.length).toEqual(0)
    expect(console.error).toHaveBeenCalledTimes(1)
    expect(console.error).toHaveBeenCalledWith('my error')
  })
})
