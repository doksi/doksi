import { shallowMount } from '@vue/test-utils'
import UserMenu from 'Users/Menu.vue'

describe('UserMenu', () => {
  it('has a created hook', () => {
    expect(typeof UserMenu.created).toBe('function')
  })

  it('sets the correct default data', () => {
    expect(typeof UserMenu.data).toBe('function')
    const defaultData = UserMenu.data()
    expect(defaultData.user).toEqual({})
  })

  it('mounts properly', () => {
    let user = {
      id: 1,
      email: 'alice@mail.com'
    }
    let propsData = {
      json: JSON.stringify(user),
      profile_path: '/profile',
      settings_path: '/settings',
      sign_out_paath: '/sign_out'
    }
    const wrapper = shallowMount(UserMenu, { propsData })
    expect(wrapper.vm.$data.user.id).toEqual(1)
    expect(wrapper.vm.$data.user.email).toEqual('alice@mail.com')
  })
})
