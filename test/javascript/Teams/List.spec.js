import { shallowMount } from '@vue/test-utils'
import TeamList from 'Teams/List.vue'
import Consumer from 'channels/consumer'

import axios from 'axios'
jest.mock('axios')

describe('TeamList', () => {
  it('has a created hook', () => {
    expect(typeof TeamList.created).toBe('function')
  })

  it('sets the correct default data', () => {
    expect(typeof TeamList.data).toBe('function')
    const defaultData = TeamList.data()
    expect(defaultData.teams).toEqual([])
  })

  it('mounts properly', () => {
    const wrapper = shallowMount(TeamList, { propsData: { json: '[]' } })
    expect(wrapper.vm.$data.teams.length).toEqual(0)
  })

  describe('getting team list', () => {

    it('sets teams from property' , () => {
      const teams = [ { id: 1, name: 'TestTeam', documents: [], members: [] } ]
      const wrapper = shallowMount(TeamList, { propsData: { json: JSON.stringify(teams) } })
      expect(wrapper.vm.$data.teams.length).toEqual(1)
      expect(wrapper.vm.$data.teams[0]['id']).toEqual(1)
      expect(wrapper.vm.$data.teams[0]['name']).toEqual('TestTeam')
    })

    it('sets teams from URL', async () => {
      const teams = [ { id: 1, name: 'TestTeam', documents: [], members: [] } ]
      const resp = { data: teams }
      axios.get.mockResolvedValue(resp)

      const url = 'https://example.com/teams.json'
      const wrapper = shallowMount(TeamList, { propsData: { url: url } })
      await wrapper.vm.$nextTick()
      expect(wrapper.vm.$data.teams.length).toEqual(1)
      expect(wrapper.vm.$data.teams[0]['id']).toEqual(1)
      expect(wrapper.vm.$data.teams[0]['name']).toEqual('TestTeam')
    })

    it('sets teams without URL', async () => {
      const teams = [ { id: 1, name: 'TestTeam', documents: [], members: [] } ]
      const resp = { data: teams }
      axios.get.mockResolvedValue(resp)

      const wrapper = shallowMount(TeamList)
      await wrapper.vm.$nextTick()
      expect(wrapper.vm.$data.teams.length).toEqual(1)
      expect(wrapper.vm.$data.teams[0]['id']).toEqual(1)
      expect(wrapper.vm.$data.teams[0]['name']).toEqual('TestTeam')
    })

    it('handle API errors', async () => {
      axios.get.mockRejectedValue('my error')
      console.error = jest.fn()
  
      const url = 'https://example.com/teams.json'
      const wrapper = shallowMount(TeamList, { propsData: { url: url } })
      await wrapper.vm.$nextTick()
      expect(wrapper.vm.$data.teams.length).toEqual(0)
      expect(console.error).toHaveBeenCalledTimes(1)
      expect(console.error).toHaveBeenCalledWith('my error')
    })
  })

  describe('channel subscription', () => {
    it('creates new team', () => {
      const newTeam = {
        id: 42,
        name: 'NewTeam',
        members: [],
        classifications: [],
        statuses: [],
        documents: []
      }
      var channelCallbacks = null
      Consumer.subscriptions.create = jest.fn((spec, callbacks) => { channelCallbacks = callbacks })
      const wrapper = shallowMount(TeamList, { propsData: { json: '[]' }})
      expect(wrapper.vm.$data.teams.length).toEqual(0)
      channelCallbacks.received({ action: 'create', team: newTeam })
      expect(wrapper.vm.$data.teams.length).toEqual(1)
      expect(wrapper.vm.$data.teams[0]['name']).toEqual('NewTeam')
    })

    it('destroys team', () => {
      var channelCallbacks = null
      Consumer.subscriptions.create = jest.fn((spec, callbacks) => { channelCallbacks = callbacks })
      const teams = [ { id: 1, name: 'TestTeam', documents: [], members: [] } ]
      const wrapper = shallowMount(TeamList, { propsData: { json: JSON.stringify(teams) } })
      expect(wrapper.vm.$data.teams.length).toEqual(1)
      channelCallbacks.received({ action: 'destroy', team: { id: 42 } })
      expect(wrapper.vm.$data.teams.length).toEqual(1)
      channelCallbacks.received({ action: 'destroy', team: { id: 1 } })
      expect(wrapper.vm.$data.teams.length).toEqual(0)
    })

    it('updates team', () => {
      var channelCallbacks = null
      Consumer.subscriptions.create = jest.fn((spec, callbacks) => { channelCallbacks = callbacks })
      const teams = [ { id: 1, name: 'TestTeam', documents: [], members: [] } ]
      const wrapper = shallowMount(TeamList, { propsData: { json: JSON.stringify(teams) } })
      expect(wrapper.vm.$data.teams.length).toEqual(1)
      expect(wrapper.vm.$data.teams[0]['name']).toEqual('TestTeam')
      channelCallbacks.received({ action: 'update', team: { id: 42, name: 'BetterTeam' } })
      expect(wrapper.vm.$data.teams[0]['name']).toEqual('TestTeam')
      channelCallbacks.received({ action: 'update', team: { id: 1, name: 'BetterTeam' } })
      expect(wrapper.vm.$data.teams[0]['name']).toEqual('BetterTeam')
    })
  })
})
