import { shallowMount } from '@vue/test-utils'
import TeamItem from 'Teams/Item.vue'

describe('TeamItem', () => {
  it('mounts properly', () => {
    const wrapper = shallowMount(TeamItem)
    expect(wrapper.find("a").exists()).toBe(true)
  })

  it('creates link', () => {
    let team = { id: 1, name: 'My Team', show_url: '/foo' }
    const wrapper = shallowMount(TeamItem, { propsData: { team }})
    let link = wrapper.find("a")
    expect(link.exists()).toBe(true)
    expect(link.text()).toBe('My Team')
    expect(link.attributes('href')).toBe('/foo')
  })
})
