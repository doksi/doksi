import { shallowMount } from '@vue/test-utils'
import Members from 'Teams/Members.vue'

import axios from 'axios'
jest.mock('axios')

function mockCsrf(token) {
  document.getElementsByName = jest.fn(() => {
    return [
      { getAttribute: function() { return token} }
    ]
  })
}

function mountComponent(options) {

  const defaultTeam = {
    id: 1,
    name: 'Team',
    members: [
      { id: 1, fullname: 'Alice', role: { role_id: 1 } },
      { id: 9, fullname: 'Bob', role: { role_id: 1 } },
    ],
    available_roles: [
      { id: 1, name: 'Role 1' }
    ]
  }

  mockCsrf('my-secret-token')

  if (!options) {
    options = { propsData: { team: defaultTeam } }
  }

  return shallowMount(Members, options)
}

describe('Members', () => {
  it('has a created hook', () => {
    expect(typeof Members.created).toBe('function')
  })

  it('sets the correct default data', () => {
    expect(typeof Members.data).toBe('function')
    const defaultData = Members.data()
    expect(defaultData.adding).toEqual(false)
    expect(defaultData.error).toEqual('')
    expect(defaultData.notice).toEqual('')
    expect(defaultData.memberToAdd).toEqual({})
  })

  it('mounts properly', () => {
    const wrapper = mountComponent()
    expect(wrapper.text()).toContain('Add new member')
    expect(wrapper.text()).toContain('Alice')
  })

  it('mounts with defaults', () => {
    mockCsrf('my-secret-token')
    const wrapper = shallowMount(Members)
    expect(wrapper.find('button').exists()).toBe(true)
  })

  it('watches memberToAdd', async () => {
    mockCsrf('my-secret-token')
    const wrapper = shallowMount(Members, { propsData: { team: { members: [], available_roles: [] } }})
    expect(wrapper.vm.$data.memberToAdd).toEqual({})
    wrapper.setProps({ team: { members: [], available_roles: [{ id: 42, name: 'TestRole' }] } })
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.$data.memberToAdd).toEqual({role: 42})
  })
  
  describe('.updateMember', () => {
    it('changes role', async () => {
      const wrapper = mountComponent()

      const resp = { data: {} }
      axios.post.mockResolvedValue(resp)

      expect.assertions(3)
      return wrapper.vm.updateMember({ id: 10, role: { role_id: 20 }}).then(function() {
        expect(axios.post.mock.calls.length).toEqual(1)
        expect(axios.post.mock.calls[0][0]).toEqual('/teams/1/members.json')
        expect(axios.post.mock.calls[0][1]).toEqual({ member: { role: 20, user: { id: 10 } } })
      })
    })

    it('handles errors', () => {
      const wrapper = mountComponent()
      axios.post.mockRejectedValue('my error')
      expect.assertions(1)
      return wrapper.vm.updateMember({ id: 10, role: { id: 20 }}).then(function() {
        expect(wrapper.vm.$data.error).toEqual('my error')
      })
    })
  })
  
  describe('.searchUsers', () => {
    it('find users', async () => {
      const wrapper = mountComponent()
      const resp = { data: { results: [ { id: 1 }, { id: 2 }, { id: 3 }]} }
      axios.get.mockResolvedValue(resp)

      expect.assertions(4)
      return wrapper.vm.searchUsers('test').then(function(data) {
        expect(axios.get.mock.calls.length).toEqual(1)
        expect(axios.get.mock.calls[0][0]).toEqual('/users.json')
        expect(axios.get.mock.calls[0][1]).toEqual({ params: { q: 'test' } })
        expect(data).toEqual([{"id": 2}, {"id": 3}])
      })
    })

    it('handles errors', () => {
      const wrapper = mountComponent()
      axios.get.mockRejectedValue('my error')
      expect.assertions(1)
      return wrapper.vm.searchUsers('test').catch(function(error) {
        expect(error).toEqual('my error')
      })
    })
  })
  
  describe('.addMember', () => {
    it('adds a member', () => {
      const wrapper = mountComponent()
      wrapper.vm.$data.memberToAdd = { user: { id: 42 }, role: 69 }

      const resp = { data: { members: [] } }
      axios.post.mockClear()
      axios.post.mockResolvedValue(resp)

      expect.assertions(3)
      return wrapper.vm.addMember().then(function() {
        expect(axios.post.mock.calls.length).toEqual(1)
        expect(axios.post.mock.calls[0][0]).toEqual('/teams/1/members.json')
        expect(axios.post.mock.calls[0][1]).toEqual({ member: { role: 69, user: { id: 42 } } })
      })
    })

    it('handles errors', () => {
      const wrapper = mountComponent()
      wrapper.vm.$data.memberToAdd = { user: { id: 42 }, role: 69 }

      axios.post.mockRejectedValue('my error')
      expect.assertions(1)
      return wrapper.vm.addMember().then(function() {
        expect(wrapper.vm.$data.error).toEqual('my error')
      })
    })
  })
  
  describe('.removeMember', () => {
    it('removes a member', () => {
      const wrapper = mountComponent()
      axios.delete.mockResolvedValue({})
      expect.assertions(5)
      expect(wrapper.props('team').members.length).toBe(2)
      return wrapper.vm.removeMember(0).then(function() {
        expect(axios.delete.mock.calls.length).toEqual(1)
        expect(axios.delete.mock.calls[0][0]).toEqual('/teams/1/members.json')
        expect(axios.delete.mock.calls[0][1]).toEqual({ params: { 'member[user][id]': 1 }})
        expect(wrapper.props('team').members.length).toBe(1)
      })
    })

    it('handles errors', () => {
      const wrapper = mountComponent()
      axios.delete.mockRejectedValue('my error')
      expect.assertions(2)
      expect(wrapper.props('team').members.length).toBe(2)
      return wrapper.vm.removeMember(0).then(function() {
        expect(wrapper.vm.$data.error).toEqual('my error')
      })
    })
  })
  
  // describe('.close', () => {
  //   it('emits close signal', async () => {
  //     const wrapper = mountComponent()
  //     wrapper.vm.close()
  //     await wrapper.vm.$nextTick()
  //     expect(wrapper.emitted().close).toBeTruthy()
  //     expect(wrapper.emitted().close.length).toBe(1)
  //   })
  // })
  
  // describe('.save', () => {
  //   it('saves the role', async () => {
  //     const wrapper = mountComponent()
  //     await wrapper.vm.$nextTick()

  //     axios.patch.mockResolvedValue({})

  //     expect.assertions(3)
  //     return wrapper.vm.save().then(function() {
  //       expect(axios.patch.mock.calls.length).toEqual(1)
  //       expect(axios.patch.mock.calls[0][0]).toEqual('/roles/1.json')
  //       expect(axios.patch.mock.calls[0][1]).toEqual({ role: { name: 'Role 1', id: 1, privileges: []}})
  //     });
  //   })

  //   it('handles errors', async () => {
  //     const wrapper = mountComponent()
  //     await wrapper.vm.$nextTick()

  //     axios.patch.mockRejectedValue('my error')

  //     expect.assertions(1)
  //     return wrapper.vm.save().then(function() {
  //       expect(wrapper.vm.$data.error).toEqual('my error')
  //     });
  //   })
  // })
})
