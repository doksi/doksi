import { shallowMount } from '@vue/test-utils'
import EditTeam from 'Teams/Edit.vue'
import Consumer from 'channels/consumer'

import axios from 'axios'
jest.mock('axios')

function mountTeam(team) {

  // mock axios request
  const defaultTeam = {
    id: 1,
    name: 'TestTeam',
    documents: [],
    classifications: [ { name: 'TestClass', id: 1 }, { name: 'AnotherClass', id: 2 } ],
    statuses: [ { name: 'TestStatus', id: 1 }, { name: 'AnotherStatus', id: 2 } ],
    members: [
      { id: 1, email: 'alice@example.com' }
    ]
  }
  const resp = { data: team || defaultTeam }
  axios.get.mockResolvedValue(resp)

  // mock csrf token
  document.getElementsByName = jest.fn(() => {
    return [
      { getAttribute: function() { return 'my-secret-token'} }
    ]
  })

  return shallowMount(EditTeam, { stubs: ['tab-nav'] })
}

describe('EditTeam', () => {
  it('has a created hook', () => {
    expect(typeof EditTeam.created).toBe('function')
  })

  it('sets the correct default data', () => {
    expect(typeof EditTeam.data).toBe('function')
    const defaultData = EditTeam.data()
    expect(defaultData.loading).toEqual(true)
  })

  it('mounts properly', () => {
    const wrapper = mountTeam()
    expect(wrapper.vm.$data.team.documents.length).toEqual(0)
  })

  describe('getting team data', () => {

    it('sets team from URL', async () => {
      const team = { name: 'MyTeam', id: 42, classifications: [], statuses: [], properties: [] }
      const resp = { data: team }
      axios.get.mockResolvedValue(resp)

      const url = 'https://example.com/teams.json'
      const stubs = ['edit-statuses', 'classifications-edit-list', 'edit-properties', 'tab-nav']
      const wrapper = shallowMount(EditTeam, { propsData: { url: url }, stubs: stubs })
      await wrapper.vm.$nextTick()
      expect(wrapper.vm.$data.team.name).toEqual('MyTeam')
      expect(wrapper.vm.$data.team.id).toEqual(42)
    })

    it('handle API errors', async () => {
      axios.get.mockRejectedValue('my error')
      console.error = jest.fn()
  
      const url = 'https://example.com/teams.json'
      const stubs = ['edit-statuses', 'classifications-edit-list', 'edit-properties', 'tab-nav']
      const wrapper = shallowMount(EditTeam, { propsData: { url: url }, stubs: stubs })
      await wrapper.vm.$nextTick()
      expect(wrapper.vm.$data.team.name).toEqual('My New Team')
      expect(wrapper.vm.$data.error).toEqual('my error')
    })
  })

  it('receives channel data', () => {
    const newTeam = {
      id: 1,
      name: 'UpdatedTeam',
      members: [],
      classifications: [],
      statuses: [],
      documents: []
    }
    Consumer.subscriptions.create = jest.fn((spec, callbacks) => {
      callbacks.received({ team: newTeam })
    })
    const wrapper = mountTeam()
    expect(wrapper.vm.$data.team.name).toEqual('UpdatedTeam')
  })
  
  describe('.searchUsers', () => {
    it('returns a list of new users', async () => {
      const wrapper = mountTeam()
      await wrapper.vm.$nextTick()

      const resp = { data: {
        results: [
          { id: 1, email: 'alice@mail.com' },
          { id: 2, email: 'bob@mail.com' }
        ]
      } }
      axios.get.mockResolvedValue(resp)

      expect.assertions(2)
      return wrapper.vm.searchUsers('query').then(function(data) {
        expect(data.length).toEqual(1)
        expect(data[0]).toEqual('bob@mail.com')
      });
    })

    it('handles errors', async () => {
      const wrapper = mountTeam()
      await wrapper.vm.$nextTick()
      axios.get.mockRejectedValue('my error')
      expect.assertions(1)
      return wrapper.vm.searchUsers('query').catch(function(error) {
        expect(error).toEqual('my error')
      });
    })
  })
  
  describe('.addMember', () => {
    it('adds a member', async () => {
      const wrapper = mountTeam()
      await wrapper.vm.$nextTick()

      const resp = { data: { results: [
        { id: 420, email: 'bob@mail.com', fullname: 'Bob' }
      ] } }
      axios.get.mockResolvedValue(resp)

      expect(wrapper.vm.$data.team.members.length).toEqual(1)
      expect.assertions(6)
      return wrapper.vm.addMember().then(function() {
        expect(wrapper.vm.$data.team.members.length).toEqual(2)
        expect(wrapper.vm.$data.team.members[1]['id']).toEqual(420)
        expect(wrapper.vm.$data.team.members[1]['email']).toEqual('bob@mail.com')
        expect(wrapper.vm.$data.team.members[1]['fullname']).toEqual('Bob')
        expect(wrapper.vm.$data.isAddingMember).toEqual(false)
      });
    })

    it('skips when no results', async () => {
      const wrapper = mountTeam()
      await wrapper.vm.$nextTick()

      const resp = { data: { results: [] } }
      axios.get.mockResolvedValue(resp)

      expect(wrapper.vm.$data.team.members.length).toEqual(1)
      expect.assertions(3)
      return wrapper.vm.addMember().then(function() {
        expect(wrapper.vm.$data.team.members.length).toEqual(1)
        expect(wrapper.vm.$data.isAddingMember).toEqual(false)
      });
    })

    it('handles errors', async () => {
      const wrapper = mountTeam()
      await wrapper.vm.$nextTick()

      axios.get.mockRejectedValue('my error')

      expect(wrapper.vm.$data.team.members.length).toEqual(1)
      expect.assertions(3)
      return wrapper.vm.addMember().then(function() {
        expect(wrapper.vm.$data.team.members.length).toEqual(1)
        expect(wrapper.vm.$data.error).toEqual('my error')
      });
    })
  })
  
  describe('.removeMember', () => {
    it('removes a member', async () => {
      const wrapper = mountTeam()
      await wrapper.vm.$nextTick()

      expect(wrapper.vm.$data.team.members.length).toEqual(1)
      wrapper.vm.removeMember(0)
      expect(wrapper.vm.$data.team.members.length).toEqual(0)
    })
  })
})
