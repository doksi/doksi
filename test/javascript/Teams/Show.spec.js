import { shallowMount } from '@vue/test-utils'
import ShowTeam from 'Teams/Show.vue'
import Consumer from 'channels/consumer'

import axios from 'axios'
jest.mock('axios')

describe('ShowTeam', () => {
  it('has a created hook', () => {
    expect(typeof ShowTeam.created).toBe('function')
  })

  it('sets the correct default data', () => {
    expect(typeof ShowTeam.data).toBe('function')
    const defaultData = ShowTeam.data()
    expect(defaultData.team.members.length).toEqual(0)
    expect(defaultData.team.documents.length).toEqual(0)
  })

  describe('getting team data', () => {

    it('sets teams from URL', async () => {
      const team = { id: 1, name: 'TestTeam', documents: [], members: [] }
      axios.get.mockResolvedValue({ data: team })

      const url = 'https://example.com/team/1.json'
      const wrapper = shallowMount(ShowTeam, { propsData: { url: url } })
      await wrapper.vm.$nextTick()
      expect(wrapper.vm.$data.team['id']).toEqual(1)
      expect(wrapper.vm.$data.team['name']).toEqual('TestTeam')
    })

    it('handle API errors', async () => {
      axios.get.mockRejectedValue('my error')
      console.error = jest.fn()
  
      const url = 'https://example.com/team/1.json'
      const wrapper = shallowMount(ShowTeam, { propsData: { url: url } })
      await wrapper.vm.$nextTick()
      expect(wrapper.vm.$data.team.name).toEqual(undefined)
      expect(console.error).toHaveBeenCalledTimes(1)
      expect(console.error).toHaveBeenCalledWith('my error')
    })
  })

  it('receives channel data', () => {
    axios.get.mockRejectedValue('my error')
    const newTeam = {
      id: 1,
      name: 'UpdatedTeam',
      members: [],
      documents: []
    }
    var channelCallbacks = null
    Consumer.subscriptions.create = jest.fn((spec, callbacks) => { channelCallbacks = callbacks })
    const wrapper = shallowMount(ShowTeam)
    expect(wrapper.vm.$data.team.name).toEqual(undefined)
    channelCallbacks.received({ team: newTeam })
    expect(wrapper.vm.$data.team.name).toEqual('UpdatedTeam')
  })
})
