import { shallowMount } from '@vue/test-utils'
import NewTeam from 'Teams/New.vue'

function mountTeam() {
  return shallowMount(NewTeam)
}

describe('EditTeam', () => {
  it('sets the correct default data', () => {
    expect(typeof NewTeam.data).toBe('function')
    const defaultData = NewTeam.data()
    expect(defaultData.error).toEqual('')
    expect(defaultData.team.name).toEqual('')
  })

  it('mounts properly', () => {
    const wrapper = mountTeam()
    expect(wrapper.vm.$data.team.name).toEqual('')
  })
})
