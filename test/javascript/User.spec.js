import { shallowMount } from '@vue/test-utils'
import User from 'User.vue'

function getMountedComponent(Component, propsData) {
  return shallowMount(Component, {
    propsData
  })
}

describe('User', () => {
  it('has a created hook', () => {
    expect(typeof User.created).toBe('function')
  })
})
