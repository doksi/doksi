import { shallowMount } from '@vue/test-utils'
import ProgressTrackerStep from 'ProgressTrackerStep.vue'

function getMountedComponent(Component, propsData) {
  return shallowMount(Component, {
    propsData
  })
}

describe('ProgressTrackerStep', () => {
  it('has a leftLineClass method', () => {
    const wrapper = getMountedComponent(ProgressTrackerStep)
    expect(typeof wrapper.vm.leftLineClass).toBe('string')
  })
})
