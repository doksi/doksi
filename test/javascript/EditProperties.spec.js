global.window = window
global.$ = function() {
  return { collapse() {} }
}

import { shallowMount } from '@vue/test-utils'
import EditProperties from 'EditProperties.vue'

import axios from 'axios'
jest.mock('axios')

function mountForm(response, options) {

  // mock axios request
  const defaultResponse = [
    { name: 'TestProp', id: 1 },
    { name: 'AnotherProp', id: 2 }
  ]
  const resp = { data: response || defaultResponse }
  axios.get.mockResolvedValue(resp)

  // mock csrf token
  document.getElementsByName = jest.fn(() => {
    return [
      { getAttribute: function() { return 'my-secret-token'} }
    ]
  })

  if (!options) {
    options = { propsData: { url: 'http://example.com' } }
  }

  return shallowMount(EditProperties, options)
}

describe('EditProperties', () => {
  it('has a created hook', () => {
    expect(typeof EditProperties.created).toBe('function')
  })

  it('sets the correct default data', () => {
    expect(typeof EditProperties.data).toBe('function')
    const defaultData = EditProperties.data()
    expect(defaultData.isAdding).toEqual(false)
    expect(defaultData.isLoading).toEqual(true)
    expect(defaultData.properties).toEqual([])
    expect(defaultData.error).toEqual('')
    expect(defaultData.propertyToAdd).toEqual({ kind: 'String' })
  })

  it('mounts properly', () => {
    const wrapper = mountForm()
    expect(wrapper.vm.$data.properties.length).toEqual(0)
  })

  it('displays error message', async () => {
    const wrapper = shallowMount(EditProperties, { propsData: { url: '' } })
    await wrapper.vm.$nextTick()
    wrapper.vm.$data.error = 'test error'
    await wrapper.vm.$nextTick()
    expect(wrapper.text()).toContain('test error')

  })

  describe('getting property list', () => {

    it('sets properties from URL', async () => {
      const properties = [ { id: 1, name: 'TestProperty' } ]
      const resp = { data: properties }
      axios.get.mockResolvedValue(resp)

      const url = 'https://example.com/properties.json'
      const wrapper = shallowMount(EditProperties, { propsData: { url: url } })
      await wrapper.vm.$nextTick()
      expect(wrapper.vm.$data.properties.length).toEqual(1)
      expect(wrapper.vm.$data.properties[0]['id']).toEqual(1)
      expect(wrapper.vm.$data.properties[0]['name']).toEqual('TestProperty')
    })

    it('handle API errors', async () => {
      axios.get.mockRejectedValue('my error')
      console.error = jest.fn()
  
      const url = 'https://example.com/properties.json'
      const wrapper = shallowMount(EditProperties, { propsData: { url: url } })
      await wrapper.vm.$nextTick()
      expect(wrapper.vm.$data.properties.length).toEqual(0)
      expect(wrapper.vm.$data.error).toEqual('my error')
    })
  })
  
  describe('.addProperty', () => {
    it('adds a property', async () => {
      const wrapper = mountForm()
      await wrapper.vm.$nextTick()

      const resp = { data: { name: 'TestProperty', id: 420 } }
      axios.post.mockResolvedValue(resp)

      expect(wrapper.vm.$data.properties.length).toEqual(2)
      expect.assertions(4)
      return wrapper.vm.addProperty().then(function() {
        expect(wrapper.vm.$data.properties.length).toEqual(3)
        expect(wrapper.vm.$data.properties[2]['name']).toEqual('TestProperty')
        expect(wrapper.vm.$data.properties[2]['id']).toEqual(420)
      });
    })

    it('handles errors', async () => {
      const wrapper = mountForm()
      await wrapper.vm.$nextTick()

      axios.post.mockRejectedValue('my error')

      expect(wrapper.vm.$data.properties.length).toEqual(2)
      expect.assertions(3)
      return wrapper.vm.addProperty().then(function() {
        expect(wrapper.vm.$data.properties.length).toEqual(2)
        expect(wrapper.vm.$data.error).toEqual('my error')
      });
    })
  })
  
  describe('.removeProperty', () => {
    it('removes a property', async () => {
      const wrapper = mountForm()
      await wrapper.vm.$nextTick()

      axios.delete.mockResolvedValue({})

      expect(wrapper.vm.$data.properties.length).toEqual(2)
      expect.assertions(2)
      return wrapper.vm.removeProperty(1).then(function() {
        expect(wrapper.vm.$data.properties.length).toEqual(1)
      });
    })

    it('handles errors', async () => {
      const wrapper = mountForm()
      await wrapper.vm.$nextTick()

      axios.delete.mockRejectedValue('my error')

      expect(wrapper.vm.$data.properties.length).toEqual(2)
      expect.assertions(3)
      return wrapper.vm.removeProperty(1).then(function() {
        expect(wrapper.vm.$data.properties.length).toEqual(1)
        expect(wrapper.vm.$data.error).toEqual('my error')
      });
    })
  })
  
  describe('.toggleProperty', () => {
    it('shows a property item', async () => {
      const wrapper = mountForm()
      await wrapper.vm.$nextTick()

      expect(wrapper.vm.$data.properties.length).toEqual(2)
      expect(wrapper.vm.$data.properties[0].expanded).toEqual(false)
      expect(wrapper.vm.$data.properties[1].expanded).toEqual(false)
      wrapper.vm.toggleProperty(1)
      expect(wrapper.vm.$data.properties[0].expanded).toEqual(false)
      expect(wrapper.vm.$data.properties[1].expanded).toEqual(true)
    })

    it('hides a property item', async () => {
      const wrapper = mountForm()
      await wrapper.vm.$nextTick()
      wrapper.vm.$data.properties[1].expanded = true

      expect(wrapper.vm.$data.properties[0].expanded).toEqual(false)
      expect(wrapper.vm.$data.properties[1].expanded).toEqual(true)
      wrapper.vm.toggleProperty(1)
      expect(wrapper.vm.$data.properties[0].expanded).toEqual(false)
      expect(wrapper.vm.$data.properties[1].expanded).toEqual(false)
    })
  })
  
  describe('.saveProperty', () => {
    it('submits a property', async () => {
      const wrapper = mountForm()
      await wrapper.vm.$nextTick()

      const resp = { data: { name: 'TestProperty', id: 2 } }
      axios.put.mockClear()
      axios.put.mockResolvedValue(resp)

      wrapper.vm.$data.properties[1] = {
        id: 42,
        name: 'MyProp',
        default: 'MyDefault',
        kind: 'String'
      }

      expect.assertions(4)
      return wrapper.vm.saveProperty(1).then(function() {
        expect(wrapper.vm.$data.properties.length).toEqual(2)
        expect(axios.put.mock.calls.length).toBe(1)
        expect(axios.put.mock.calls[0][0]).toBe('/properties/42.json')
        expect(axios.put.mock.calls[0][1]).toEqual({
          property: {
            id: 42,
            name: 'MyProp',
            isSaving: false,
            default: 'MyDefault',
            kind: 'String',
            error: '',
            notice: 'Property has been saved'
          }
        })
      });
    })

    it('handles errors', async () => {
      const wrapper = mountForm()
      await wrapper.vm.$nextTick()

      axios.put.mockClear()
      axios.put.mockRejectedValue('my error')

      expect(wrapper.vm.$data.properties.length).toEqual(2)
      expect.assertions(3)
      return wrapper.vm.saveProperty(1).then(function() {
        expect(wrapper.vm.$data.properties.length).toEqual(2)
        expect(wrapper.vm.$data.properties[1].error).toEqual('my error')
      });
    })
  })
})
