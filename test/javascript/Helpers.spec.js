import { nodeWalk } from 'Helpers.js'

describe('nodeWalk', () => {
  it('finds first node', () => {
    let results = []
    let haystack = {
      name: 'needle',
      id: 1
    }
    nodeWalk(haystack, function (node) {
      if (node.name == 'needle')
        return false
      results.push(node.id)
      return true
    })
    expect(results).toEqual([])
  })

  it('finds child', () => {
    let results = []
    let haystack = {
      name: 'node',
      id: 1,
      firstChild: {
        name: 'needle',
        id: 2
      }
    }
    nodeWalk(haystack, function (node) {
      if (node.name == 'needle')
        return false
      results.push(node.id)
      return true
    })
    expect(results).toEqual([1])
  })

  it('finds sibling', () => {
    let results = []
    let haystack = {
      name: 'node',
      id: 1,
      firstChild: {
        name: 'node',
        id: 2,
        nextSibling: {
          name: 'node',
          id: 3
        }
      },
      nextSibling: {
        name: 'needle',
        id: 4
      }
    }
    nodeWalk(haystack, function (node) {
      if (node.name == 'needle')
        return false
      results.push(node.id)
      return true
    })
    expect(results).toEqual([1, 2, 3])
  })
})
