import { shallowMount } from '@vue/test-utils'
import ProgressTracker from 'ProgressTracker.vue'

describe('ProgressTracker', () => {
  it('has a stepClass method', () => {
    const wrapper = shallowMount(ProgressTracker, { stubs: ['progress-tracker-step'] })
    expect(typeof wrapper.vm.stepClass).toBe('string')
  })
})
