import { shallowMount } from '@vue/test-utils'
import Editable from 'Editable.vue'

const wrapper = shallowMount(Editable)

function getMountedComponent(Component, propsData) {
  return shallowMount(Component, {
    propsData
  })
}

describe('Editable', () => {
  it('has a mounted hook', () => {
    expect(typeof Editable.mounted).toBe('function')
  })
})
