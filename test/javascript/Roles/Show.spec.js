import { shallowMount } from '@vue/test-utils'
import Role from 'Roles/Show.vue'

import axios from 'axios'
jest.mock('axios')

function mountRole(options) {

  // mock csrf token
  document.getElementsByName = jest.fn(() => {
    return [
      { getAttribute: function() { return 'my-secret-token'} }
    ]
  })

  if (!options) {
    options = { propsData: { role: { name: 'Role 1', id: 1, privileges: [] } } }
  }

  return shallowMount(Role, options)
}

describe('Role', () => {
  it('has a created hook', () => {
    expect(typeof Role.created).toBe('function')
  })

  it('sets the correct default data', () => {
    expect(typeof Role.data).toBe('function')
    const defaultData = Role.data()
    expect(defaultData.adding).toEqual(false)
    expect(defaultData.saving).toEqual(false)
    expect(defaultData.error).toEqual('')
    expect(defaultData.notice).toEqual('')
    expect(defaultData.privilegeToAdd).toEqual({ verb: 'edit', resource: 'Team' })
  })

  it('mounts properly', () => {
    const wrapper = mountRole()
    expect(wrapper.text()).toContain('Role 1')
  })

  it('mounts with defaults', () => {
    const wrapper = shallowMount(Role)
    expect(wrapper.find('input[type="text"]').exists()).toBe(true)
  })

  it('displays error message', async () => {
    const wrapper = mountRole()
    await wrapper.vm.$nextTick()
    wrapper.vm.$data.error = 'test error'
    await wrapper.vm.$nextTick()
    expect(wrapper.text()).toContain('test error')
  })
  
  describe('.addPrivilege', () => {
    it('adds a privilege', async () => {
      const wrapper = mountRole()
      await wrapper.vm.$nextTick()

      const resp = { data: { resource: 'Document', verb: 'destroy' } }
      axios.post.mockResolvedValue(resp)

      expect(wrapper.props('role').privileges.length).toEqual(0)
      expect.assertions(4)
      return wrapper.vm.addPrivilege().then(function() {
        expect(wrapper.props('role').privileges.length).toEqual(1)
        expect(wrapper.props('role').privileges[0]['resource']).toEqual('Document')
        expect(wrapper.props('role').privileges[0]['verb']).toEqual('destroy')
      });
    })

    it('handles errors', async () => {
      const wrapper = mountRole()
      await wrapper.vm.$nextTick()

      axios.post.mockRejectedValue('my error')

      expect(wrapper.props('role').privileges.length).toEqual(0)
      expect.assertions(3)
      return wrapper.vm.addPrivilege().then(function() {
        expect(wrapper.props('role').privileges.length).toEqual(0)
        expect(wrapper.vm.$data.error).toEqual('my error')
      });
    })
  })
  
  describe('.removePrivilege', () => {
    it('removes a privilege', async () => {
      let options = { propsData: { role: { name: 'Role 1', id: 1, privileges: [
        { resource: 'Document', verb: 'show' },
        { resource: 'Document', verb: 'edit' }
      ] } } }
      const wrapper = mountRole(options)
      await wrapper.vm.$nextTick()

      axios.delete.mockResolvedValue({})

      expect(wrapper.props('role').privileges.length).toEqual(2)
      expect.assertions(2)
      return wrapper.vm.removePrivilege(1).then(function() {
        expect(wrapper.props('role').privileges.length).toEqual(1)
      });
    })

    it('handles errors', async () => {
      let options = { propsData: { role: { name: 'Role 1', id: 1, privileges: [
        { resource: 'Document', verb: 'show' },
        { resource: 'Document', verb: 'edit' }
      ] } } }
      const wrapper = mountRole(options)
      await wrapper.vm.$nextTick()

      axios.delete.mockRejectedValue('my error')

      expect(wrapper.props('role').privileges.length).toEqual(2)
      expect.assertions(3)
      return wrapper.vm.removePrivilege(1).then(function() {
        expect(wrapper.props('role').privileges.length).toEqual(1)
        expect(wrapper.vm.$data.error).toEqual('my error')
      });
    })
  })
  
  describe('.close', () => {
    it('emits close signal', async () => {
      const wrapper = mountRole()
      wrapper.vm.close()
      await wrapper.vm.$nextTick()
      expect(wrapper.emitted().close).toBeTruthy()
      expect(wrapper.emitted().close.length).toBe(1)
    })
  })
  
  describe('.save', () => {
    it('saves the role', async () => {
      const wrapper = mountRole()
      await wrapper.vm.$nextTick()

      axios.patch.mockResolvedValue({})

      expect.assertions(3)
      return wrapper.vm.save().then(function() {
        expect(axios.patch.mock.calls.length).toEqual(1)
        expect(axios.patch.mock.calls[0][0]).toEqual('/roles/1.json')
        expect(axios.patch.mock.calls[0][1]).toEqual({ role: { name: 'Role 1', id: 1, privileges: []}})
      });
    })

    it('handles errors', async () => {
      const wrapper = mountRole()
      await wrapper.vm.$nextTick()

      axios.patch.mockRejectedValue('my error')

      expect.assertions(1)
      return wrapper.vm.save().then(function() {
        expect(wrapper.vm.$data.error).toEqual('my error')
      });
    })
  })
})
