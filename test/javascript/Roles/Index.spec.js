import { shallowMount } from '@vue/test-utils'
import Roles from 'Roles/Index.vue'

import axios from 'axios'
jest.mock('axios')

function mountRoles(response, options) {

  // mock axios request
  const defaultResponse = [
    { name: 'Role 1', id: 1, privileges: [] },
    { name: 'Role 2', id: 2, privileges: [] }
  ]
  const resp = { data: response || defaultResponse }
  axios.get.mockResolvedValue(resp)

  // mock csrf token
  document.getElementsByName = jest.fn(() => {
    return [
      { getAttribute: function() { return 'my-secret-token'} }
    ]
  })

  if (!options) {
    options = { propsData: { url: 'http://example.com' }, stubs: ['roles-show'] }
  }

  return shallowMount(Roles, options)
}

describe('Roles', () => {
  it('has a created hook', () => {
    expect(typeof Roles.created).toBe('function')
  })

  it('sets the correct default data', () => {
    expect(typeof Roles.data).toBe('function')
    const defaultData = Roles.data()
    expect(defaultData.adding).toEqual(false)
    expect(defaultData.loading).toEqual(true)
    expect(defaultData.roles).toEqual([])
    expect(defaultData.error).toEqual('')
    expect(defaultData.roleToAdd).toEqual({ name: '' })
  })

  it('mounts properly', () => {
    const wrapper = mountRoles()
    expect(wrapper.vm.$data.roles.length).toEqual(0)
  })

  it('displays error message', async () => {
    const wrapper = shallowMount(Roles, { propsData: { url: '' }, stubs: ['roles-show'] })
    await wrapper.vm.$nextTick()
    wrapper.vm.$data.error = 'test error'
    await wrapper.vm.$nextTick()
    expect(wrapper.text()).toContain('test error')

  })

  describe('getting property list', () => {

    it('sets roles from URL', async () => {
      const roles = [ { id: 1, name: 'TestRole' } ]
      const resp = { data: roles }
      axios.get.mockResolvedValue(resp)

      const url = 'https://example.com/roles.json'
      const wrapper = shallowMount(Roles, { propsData: { url: url }, stubs: ['roles-show'] })
      await wrapper.vm.$nextTick()
      expect(wrapper.vm.$data.roles.length).toEqual(1)
      expect(wrapper.vm.$data.roles[0]['id']).toEqual(1)
      expect(wrapper.vm.$data.roles[0]['name']).toEqual('TestRole')
    })

    it('switches to role', async () => {
      const roles = [ { id: 1, name: 'TestRole' } ]
      const resp = { data: roles }
      axios.get.mockResolvedValue(resp)
      document.location.hash = '#1'

      const url = 'https://example.com/roles.json'
      const wrapper = shallowMount(Roles, { propsData: { url: url }, stubs: ['roles-show'] })
      await wrapper.vm.$nextTick()
      expect(wrapper.vm.$data.currentRole).not.toEqual({})
      expect(wrapper.vm.$data.currentRole.id).toEqual(1)
    })

    it('handle API errors', async () => {
      axios.get.mockRejectedValue('my error')
      console.error = jest.fn()
  
      const url = 'https://example.com/roles.json'
      const wrapper = shallowMount(Roles, { propsData: { url: url }, stubs: ['roles-show'] })
      await wrapper.vm.$nextTick()
      expect(wrapper.vm.$data.roles.length).toEqual(0)
      expect(wrapper.vm.$data.error).toEqual('my error')
    })
  })
  
  describe('.addRole', () => {
    it('adds a role', async () => {
      const wrapper = mountRoles()
      await wrapper.vm.$nextTick()

      const resp = { data: { name: 'TestRole', id: 420 } }
      axios.post.mockResolvedValue(resp)

      expect(wrapper.vm.$data.roles.length).toEqual(2)
      expect.assertions(4)
      return wrapper.vm.addRole().then(function() {
        expect(wrapper.vm.$data.roles.length).toEqual(3)
        expect(wrapper.vm.$data.roles[2]['name']).toEqual('TestRole')
        expect(wrapper.vm.$data.roles[2]['id']).toEqual(420)
      });
    })

    it('handles errors', async () => {
      const wrapper = mountRoles()
      await wrapper.vm.$nextTick()

      axios.post.mockRejectedValue('my error')

      expect(wrapper.vm.$data.roles.length).toEqual(2)
      expect.assertions(3)
      return wrapper.vm.addRole().then(function() {
        expect(wrapper.vm.$data.roles.length).toEqual(2)
        expect(wrapper.vm.$data.error).toEqual('my error')
      });
    })
  })
  
  describe('.removeRole', () => {
    it('removes a role', async () => {
      const wrapper = mountRoles()
      await wrapper.vm.$nextTick()

      axios.delete.mockResolvedValue({})

      expect(wrapper.vm.$data.roles.length).toEqual(2)
      expect.assertions(2)
      return wrapper.vm.removeRole(1).then(function() {
        expect(wrapper.vm.$data.roles.length).toEqual(1)
      });
    })

    it('handles errors', async () => {
      const wrapper = mountRoles()
      await wrapper.vm.$nextTick()

      axios.delete.mockRejectedValue('my error')

      expect(wrapper.vm.$data.roles.length).toEqual(2)
      expect.assertions(3)
      return wrapper.vm.removeRole(1).then(function() {
        expect(wrapper.vm.$data.roles.length).toEqual(1)
        expect(wrapper.vm.$data.error).toEqual('my error')
      });
    })
  })
  
  describe('.openRole', () => {
    it('opens a role', async () => {
      const wrapper = mountRoles()
      await wrapper.vm.$nextTick()

      let role = wrapper.vm.$data.roles[0]
      history.pushState = jest.fn()
      wrapper.vm.openRole(role)
      expect(wrapper.vm.$data.currentRole).toBe(role)
      expect(history.pushState.mock.calls.length).toBe(1)
      expect(history.pushState.mock.calls[0][0]).toBe(null)
      expect(history.pushState.mock.calls[0][1]).toBe(null)
      expect(history.pushState.mock.calls[0][2]).toBe('#1')
    })
  })
  
  describe('.closeRole', () => {
    it('closes a role', async () => {
      const wrapper = mountRoles()
      await wrapper.vm.$nextTick()

      let role = wrapper.vm.$data.roles[0]
      wrapper.vm.openRole(role)

      history.pushState = jest.fn()
      wrapper.vm.closeRole()
      expect(wrapper.vm.$data.currentRole).toEqual({})
      expect(history.pushState.mock.calls.length).toBe(1)
      expect(history.pushState.mock.calls[0][0]).toBe(null)
      expect(history.pushState.mock.calls[0][1]).toBe(null)
      expect(history.pushState.mock.calls[0][2]).toBe('#')
    })
  })
})
