# frozen_string_literal: true

FactoryBot.define do
  factory :block, class: Paragraph do
    content { 'My Block' }
    type { 'Paragraph' }
    document

    factory :heading_block, class: Heading do
      content { 'My Heading' }
      type { 'Heading' }
      level { 1 }
    end

    factory :table_block, class: Table do
      content do
        [
          [{ content: 'A' }, { content: 'B' }, { content: 'C' }],
          [{ content: '1' }, { content: '2' }, { content: '3' }]
        ].to_json
      end
      type { 'Table' }
      rows { 2 }
      columns { 3 }
    end

    factory :paragraph_block, class: Paragraph do
      content { 'My Paragraph' }
      type { 'Paragraph' }
    end

    factory :unordered_list_block, class: List do
      content { '<li>foo</li><li>bar</li>' }
      type { 'List' }
      kind { 'unordered' }
    end

    factory :ordered_list_block, class: List do
      content { '<li>foo</li><li>bar</li>' }
      type { 'List' }
      kind { 'ordered' }
    end

    factory :quote_block, class: Quote do
      content { 'My Quote' }
      source { 'My Source' }
      type { 'Quote' }
    end

    factory :image_block, class: Image do
      content { 'http://example.com/foo.jpg' }
      type { 'Image' }
    end

    factory :code_block, class: Code do
      content { "# print\nputs 'foobar'" }
      kind { 'ruby' }
      type { 'Code' }
    end
  end
end
