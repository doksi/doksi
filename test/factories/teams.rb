# frozen_string_literal: true

FactoryBot.define do
  factory :team do
    name { 'Team' }
    description { 'The team' }

    transient do
      member_count { 1 }
    end

    after(:build) do |team, evaluator|
      team.users = if evaluator.users && !evaluator.users.empty?
                     evaluator.users
                   else
                     create_list(:user, evaluator.member_count)
                   end
    end
  end
end
