# frozen_string_literal: true

FactoryBot.define do
  factory :role_assignment do
    role { create(:role) }
    subject { create(:user) }
    object { create(:team) }
  end
end
