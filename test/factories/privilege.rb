# frozen_string_literal: true

FactoryBot.define do
  factory :privilege do
    resource { 'Team' }
    verb { 'show' }
    role { create(:role, name: 'TeamViewer') }
  end
end
