# frozen_string_literal: true

FactoryBot.define do
  factory :classification do
    name { 'Open' }
  end
end
