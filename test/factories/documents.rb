# frozen_string_literal: true

FactoryBot.define do
  factory :document do
    title { 'My Document' }
    owner factory: :user

    factory :team_document do
      owner factory: :team
    end

    factory :open_document do
      classification { create(:classification, name: 'Open') }
    end

    factory :public_document do
      visibility { :public }
    end

    factory :private_document do
      visibility { :private }
    end

    factory :draft_document do
      status { create(:status, name: 'Draft') }
    end
  end
end
