# frozen_string_literal: true

FactoryBot.define do
  factory :property do
    name { 'MyProperty' }
    kind { 'String' }
    default { 'MyValue' }
    options { nil }
    owner { nil }

    factory :boolean_property do
      kind { 'Boolean' }
      default { 'true' }
    end

    factory :integer_property do
      kind { 'Integer' }
      default { '0' }
    end

    factory :enum_property do
      kind { 'Enum' }
      default { 'Foo' }
      options { %w[Foo Bar] }
    end
  end
end
