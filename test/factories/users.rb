# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    sequence(:username) { |n| "john#{n}" }
    fullname { 'John Doe' }
    email { "#{username}@mail.com" }
    time_zone { 'Stockholm' }

    factory :admin do
      admin { true }
    end
  end
end
