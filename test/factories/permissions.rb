# frozen_string_literal: true

FactoryBot.define do
  factory :permission do
    subject { create(:user) }
    object { create(:document) }
    privilege { :read }
    mode { :allow }

    factory :read_permission do
      privilege { :read }
    end

    factory :write_permission do
      privilege { :write }
    end

    factory :admin_permission do
      privilege { :admin }
    end

    factory :deny_read_permission do
      mode { :deny }
      privilege { :read }
    end

    factory :deny_write_permission do
      mode { :deny }
      privilege { :write }
    end

    factory :deny_admin_permission do
      mode { :deny }
      privilege { :admin }
    end
  end
end
