# frozen_string_literal: true

require 'test_helper'

class AuthTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    OmniAuth.config.test_mode = true
  end

  test 'login as new user' do
    auth = {
      uid: 12_345,
      info: {
        email: 'test@mail.com',
        name: 'Test User',
        first_name: 'Test',
        last_name: 'User'
      }
    }
    OmniAuth.config.add_mock :google_oauth2, auth

    # go to login and ensure we see sign in link
    get new_user_session_path
    assert_response :success
    assert_select '#nav-auth a.nav-link', count: 1, text: 'Sign in'
    assert_select '#nav-auth users-menu', count: 0

    # sign in as new user
    assert_difference 'User.count' do
      get user_google_oauth2_omniauth_authorize_path
      follow_redirect!
    end
    assert_redirected_to root_path

    # follow through the redirect and verify we see a sign out link
    follow_redirect!
    assert_response :success
    assert_select 'div.alert', /Successfully authenticated from Google account./
    assert_select '#nav-auth a.nav-link', count: 0, text: 'Sign in'
    assert_select '#nav-auth users-menu', count: 1

    # sign out and verify we are no longer signed in
    delete destroy_user_session_path
    assert_redirected_to root_path

    follow_redirect!
    assert_response :success
    assert_select '#nav-auth a.nav-link', count: 1, text: 'Sign in'
    assert_select '#nav-auth users-menu', count: 0
  end

  test 'login as existing user' do
    user = create(:user)

    auth = {
      uid: 12_345,
      info: {
        email: user.email,
        name: 'Test User',
        first_name: 'Test',
        last_name: 'User'
      }
    }
    OmniAuth.config.add_mock :google_oauth2, auth

    # go to login and ensure we see sign in link
    get new_user_session_path
    assert_response :success
    assert_select '#nav-auth a.nav-link', count: 1, text: 'Sign in'
    assert_select '#nav-auth users-menu', count: 0

    # sign in as existing user
    assert_no_difference 'User.count' do
      get user_google_oauth2_omniauth_authorize_path
      follow_redirect!
    end
    assert_redirected_to root_path

    # follow through the redirect and verify we see a sign out link
    follow_redirect!
    assert_response :success
    assert_select 'div.alert', /Successfully authenticated from Google account./
    assert_select '#nav-auth a.nav-link', count: 0, text: 'Sign in'
    assert_select '#nav-auth users-menu', count: 1

    # sign out and verify we are no longer signed in
    delete destroy_user_session_path
    assert_redirected_to root_path

    follow_redirect!
    assert_response :success
    assert_select '#nav-auth a.nav-link', count: 1, text: 'Sign in'
    assert_select '#nav-auth users-menu', count: 0
  end
end
