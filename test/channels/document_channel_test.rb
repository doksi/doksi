# frozen_string_literal: true

require 'test_helper'

class DocumentChannelTest < ActionCable::Channel::TestCase
  setup do
    Git.stubs(:init).returns(stub(config: true, add: true, status: stub(added: {}), commit: true))
  end

  test 'subscribe and stream from document' do
    doc = create(:document)
    subscribe id: doc.id
    assert subscription.confirmed?
    assert_has_stream_for doc
  end
end
