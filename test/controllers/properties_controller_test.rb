# frozen_string_literal: true

require 'test_helper'

class PropertiesControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  include ActionCable::TestHelper

  test 'should get index' do
    sign_in create(:user)
    get properties_url(format: :json)
    assert_response :success
  end

  test 'should show property' do
    sign_in create(:user)
    property = create(:property)
    get property_url(property, format: :json)
    assert_response :success
  end

  test 'should redirect create when logged out' do
    assert_no_difference('Property.count') do
      post properties_url, params: { property: { name: 'test' } }
    end
    assert_redirected_to new_user_session_path
  end

  test 'should redirect update when logged out' do
    property = create(:property)
    patch property_url(property), params: { property: { name: 'test' } }
    assert_redirected_to new_user_session_path
  end

  test 'should redirect destroy when logged out' do
    property = create(:property)
    assert_no_difference('Property.count') do
      delete property_url(property)
    end
    assert_redirected_to new_user_session_path
  end

  test 'should create property' do
    sign_in create(:user)

    assert_difference('Property.count') do
      post properties_url, params: { property: { name: 'test' } }
    end

    assert_redirected_to property_url(Property.last)
  end

  test 'should show error when failing to create' do
    sign_in create(:user)

    assert_no_difference('Property.count') do
      post properties_url(format: :json), params: { property: { name: '' } }
    end

    assert_response :unprocessable_entity
    json = JSON.parse response.body
    assert_equal "can't be blank", json['name'][0]
    assert_equal 'is too short (minimum is 2 characters)', json['name'][1]
  end

  test 'should create team property' do
    sign_in create(:user)
    team = create(:team)

    assert_difference('team.properties.count') do
      post properties_url, params: { property: { name: 'test', owner_id: team.id, owner_type: 'Team' } }
    end

    assert_redirected_to property_url(Property.last)
    assert_equal team, Property.last.owner
    assert_includes team.properties, Property.last
  end

  test 'should update property' do
    sign_in create(:user)
    property = create(:property)

    patch property_url(property), params: { property: { name: 'New name' } }
    assert_redirected_to property_url(property)
    assert_equal 'New name', Property.find(property.id).name
  end

  test 'should show error when failing to update' do
    sign_in create(:user)
    property = create(:property)

    patch property_url(property, format: :json), params: { property: { name: '' } }

    assert_response :unprocessable_entity
    json = JSON.parse response.body
    assert_equal "can't be blank", json['name'][0]
    assert_equal 'is too short (minimum is 2 characters)', json['name'][1]
  end

  test 'should destroy property' do
    sign_in create(:user)
    team = create(:team)
    property = create(:property, owner: team)

    assert_difference('team.properties.count', -1) do
      delete property_url(property)
    end

    assert_redirected_to properties_url
  end
end
