# frozen_string_literal: true

require 'test_helper'

class RoleAssignmentsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @role = create(:role)
  end

  def setup_params
    @user = create(:user, admin: true)
    @team = create(:team, users: [@user])
    sign_in @user

    @params = {
      subject_id: @user.id,
      subject_type: @user.class.name,
      object_id: @team.id,
      object_type: @team.class.name
    }
  end

  test 'should require authentication for index' do
    get role_assignments_url(@role, format: :json)
    assert_response :unauthorized
  end

  test 'should require authentication for show' do
    assignment = create(:role_assignment, role: @role)
    get role_assignment_url(@role, assignment, format: :json)
    assert_response :unauthorized
  end

  test 'should require authentication for create' do
    assert_no_difference('RoleAssignment.count') do
      post role_assignments_url(@role, format: :json),
           params: { role_assignment: { name: 'test' } }
    end
    assert_response :unauthorized
  end

  test 'should require authentication for update' do
    assignment = create(:role_assignment, role: @role)
    patch role_assignment_url(@role, assignment, format: :json),
          params: { role_assignment: { name: 'test' } }
    assert_response :unauthorized
  end

  test 'should require authentication for destroy' do
    assignment = create(:role_assignment, role: @role)
    assert_no_difference('RoleAssignment.count') do
      delete role_assignment_url(@role, assignment, format: :json)
    end
    assert_response :unauthorized
  end

  # test 'should require admin rights for index' do
  #   sign_in create(:user)
  #   get role_assignments_url(@role, format: :json)
  #   assert_response :forbidden
  # end

  # test 'should require admin rights for show' do
  #   sign_in create(:user)
  #   assignment = create(:role_assignment, role: @role)
  #   get role_assignment_url(@role, assignment, format: :json)
  #   assert_response :forbidden
  # end

  # test 'should require admin rights for create' do
  #   sign_in create(:user)
  #   assert_no_difference('RoleAssignment.count') do
  #     post role_assignments_url(@role, format: :json),
  #          params: { role_assignment: { name: 'test' } }
  #   end
  #   assert_response :forbidden
  # end

  # test 'should require admin rights for update' do
  #   sign_in create(:user)
  #   assignment = create(:role_assignment, role: @role)
  #   patch role_assignment_url(@role, assignment, format: :json),
  #         params: { role_assignment: { name: 'test' } }
  #   assert_response :forbidden
  # end

  # test 'should require admin rights for destroy' do
  #   sign_in create(:user)
  #   assignment = create(:role_assignment, role: @role)
  #   assert_no_difference('RoleAssignment.count') do
  #     delete role_assignment_url(@role, assignment, format: :json)
  #   end
  #   assert_response :forbidden
  # end

  test 'should get index' do
    sign_in create(:user, admin: true)
    get role_assignments_url(@role, format: :json)
    assert_response :success
  end

  test 'should show assignment' do
    sign_in create(:user, admin: true)
    assignment = create(:role_assignment, role: @role)
    get role_assignment_url(@role, assignment, format: :json)
    assert_response :success
  end

  test 'should create assignment' do
    user = create(:user, admin: true)
    team = create(:team, users: [user])
    sign_in user

    params = {
      subject_id: user.id,
      subject_type: user.class.name,
      object_id: team.id,
      object_type: team.class.name
    }

    assert_difference('RoleAssignment.count') do
      post role_assignments_url(@role, format: :json), params: { role_assignment: params }
    end

    assert_response :success
  end

  test 'should show error when using wrong subject id' do
    setup_params
    @params[:subject_id] = 420
    assert_no_difference('RoleAssignment.count') do
      post role_assignments_url(@role, format: :json), params: { role_assignment: @params }
      assert_response :unprocessable_entity
      assert_equal '420 is not valid', JSON.parse(response.body)['subject_id'][0]
    end
  end

  test 'should show error when using wrong subject type' do
    setup_params
    @params[:subject_type] = 'foo'
    assert_no_difference('RoleAssignment.count') do
      post role_assignments_url(@role, format: :json), params: { role_assignment: @params }
      assert_response :unprocessable_entity
      assert_equal 'foo is not valid', JSON.parse(response.body)['subject_type'][0]
    end
  end

  test 'should show error when using wrong object id' do
    setup_params
    @params[:object_id] = 420
    assert_no_difference('RoleAssignment.count') do
      post role_assignments_url(@role, format: :json), params: { role_assignment: @params }
      assert_response :unprocessable_entity
      assert_equal '420 is not valid', JSON.parse(response.body)['object_id'][0]
    end
  end

  test 'should show error when using wrong object type' do
    setup_params
    @params[:object_type] = 'foo'
    assert_no_difference('RoleAssignment.count') do
      post role_assignments_url(@role, format: :json), params: { role_assignment: @params }
      assert_response :unprocessable_entity
      assert_equal 'foo is not valid', JSON.parse(response.body)['object_type'][0]
    end
  end

  test 'should show error when missing object' do
    setup_params
    @params[:object_id] = nil
    @params[:object_type] = nil
    assert_no_difference('RoleAssignment.count') do
      post role_assignments_url(@role, format: :json), params: { role_assignment: @params }
      assert_response :unprocessable_entity
      assert_equal 'must exist', JSON.parse(response.body)['object'][0]
    end
  end

  test 'should update assignment' do
    user = create(:user, admin: true)
    team = create(:team, users: [user])
    sign_in user

    assignment = create(:role_assignment, role: @role, subject: user, object: create(:team))
    params = { object_type: team.class.name, object_id: team.id }

    patch role_assignment_url(@role, assignment, format: :json), params: { role_assignment: params }
    assert_response :success
    assert_equal team, RoleAssignment.find(assignment.id).object
  end

  test 'should destroy assignment' do
    sign_in create(:user, admin: true)
    assignment = create(:role_assignment, role: @role)

    assert_difference('RoleAssignment.count', -1) do
      delete role_assignment_url(@role, assignment, format: :json)
    end

    assert_response :success
  end
end
