# frozen_string_literal: true

require 'test_helper'

class AdminControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  test 'should get dashboard' do
    sign_in create(:admin)
    get admin_dashboard_url
    assert_response :success
  end

  test 'should get classifications' do
    sign_in create(:admin)
    get admin_classifications_url
    assert_response :success
  end

  test 'should get statuses' do
    sign_in create(:admin)
    get admin_statuses_url
    assert_response :success
  end

  test 'should redirect non-admins' do
    sign_in create(:user)
    get admin_dashboard_url
    assert_redirected_to root_url
  end

  test 'should require authentication' do
    get admin_dashboard_url
    assert_redirected_to new_user_session_path
  end
end
