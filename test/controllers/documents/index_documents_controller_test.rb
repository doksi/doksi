# frozen_string_literal: true

require 'test_helper'

class DocumentsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  include ActionCable::TestHelper

  setup do
    Git.stubs(:init).returns(stub(config: true, add: true, status: stub(added: {}), commit: true))
  end

  test 'should get index' do
    get documents_url
    assert_response :success
  end

  test 'should get index with search' do
    get documents_url, params: { search: create(:document).title }
    assert_response :success
  end

  test 'should get team documents' do
    user = create(:user)
    team = create(:team)
    create(:public_document, owner: team)
    create(:public_document, owner: team)
    create(:public_document, owner: user)

    get team_documents_url(team, format: :json)
    assert_response :success

    body = JSON.parse(response.body)
    assert_equal team.documents.count, body.count
    assert_equal(team.documents.ids, body.map { |x| x['id'] })
  end

  test 'should get all team documents' do
    user = create(:user)
    team1 = create(:team, users: [user])
    team2 = create(:team, users: [user])
    team3 = create(:team)
    doc1 = create(:public_document, owner: team1)
    doc2 = create(:public_document, owner: team1)
    create(:public_document, owner: user)
    doc4 = create(:public_document, owner: team2)
    create(:public_document, owner: team3)

    sign_in user
    get documents_url(scope: :teams, format: :json)
    assert_response :success

    body = JSON.parse(response.body)
    assert_equal 3, body.count
    assert_equal doc1.id, body[0]['id']
    assert_equal doc2.id, body[1]['id']
    assert_equal doc4.id, body[2]['id']
  end

  test 'should get user documents' do
    user = create(:user)
    team = create(:team)
    create(:public_document, owner: team)
    create(:public_document, owner: team)
    create(:public_document, owner: user)

    get user_documents_url(user, format: :json)
    assert_response :success

    body = JSON.parse(response.body)
    assert_equal user.documents.count, body.count
    assert_equal(user.documents.ids, body.map { |x| x['id'] })
  end

  test 'should get public documents' do
    user = create(:user)
    team1 = create(:team, users: [user])
    team2 = create(:team)
    team3 = create(:team)

    # should not be included
    create(:public_document, owner: team1)
    create(:public_document, owner: team1)
    create(:public_document, owner: user)
    create(:private_document)

    # should be included
    doc5 = create(:public_document, owner: team2)
    doc6 = create(:public_document, owner: team3)
    doc7 = create(:public_document)

    sign_in user
    get documents_url(scope: :public, format: :json)
    assert_response :success

    body = JSON.parse(response.body)
    assert_equal 3, body.count
    assert_equal doc5.id, body[0]['id']
    assert_equal doc6.id, body[1]['id']
    assert_equal doc7.id, body[2]['id']
  end
end
