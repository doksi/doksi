# frozen_string_literal: true

require 'test_helper'

class DocumentsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  include ActionCable::TestHelper

  setup do
    Git.stubs(:init).returns(stub(config: true, add: true, status: stub(added: {}), commit: true))
  end

  test 'should redirect new when logged out' do
    get new_document_url
    assert_redirected_to new_user_session_path

    assert_no_difference('Document.count') do
      post documents_url, params: { document: { title: build(:document).title } }
    end

    assert_redirected_to new_user_session_path
  end

  test 'should get new' do
    sign_in create(:user)
    get new_document_url
    assert_response :success
  end

  test 'should get new for team' do
    team = create(:team)
    sign_in create(:user)
    get new_document_url, params: { owner: { type: Team, id: team.id } }
    assert_response :success
  end

  test 'should create document' do
    sign_in create(:user)
    document = create(:document)
    assert_difference('Document.count') do
      post documents_url, params: { document: { title: document.title } }
    end

    assert_redirected_to edit_document_url(Document.last)
  end

  test 'should create document with blocks' do
    sign_in create(:user)
    document = create(:document)
    assert_difference('Document.count') do
      post documents_url, params: {
        document: {
          title: document.title,
          blocks: [
            {
              id: 123,
              type: 'Paragraph',
              alignment: 'justify',
              content: 'My Block',
              level: '1'
            }
          ]
        }
      }
    end

    document = Document.last
    assert_redirected_to edit_document_url(document)
    assert_equal 1, document.blocks.count
    assert_equal 'My Block', document.blocks.first.content
  end

  test 'should create document with properties' do
    sign_in create(:user)
    prop = create(:property, default: 'Default Value')

    document_params = {
      title: 'My Document',
      properties: [
        { id: prop.id, value: 'Prop Value' }
      ]
    }

    assert_difference('Document.count') do
      post documents_url, params: { document: document_params }
    end

    document = Document.last
    assert_redirected_to edit_document_url(document)
    assert_equal 'My Document', document.title
    assert_equal 1, document.properties.count
    assert_equal 1, document.assigned_properties.count
    assert_equal prop, document.assigned_properties.first.property
    assert_equal 'Prop Value', document.assigned_properties.first.value
  end

  test 'should create document for team' do
    team = create(:team)
    document = create(:document)

    sign_in create(:user)
    assert_difference('Document.count') do
      post documents_url, params: { document: {
        title: document.title,
        owner_type: 'Team',
        owner_id: team.id
      } }
    end

    assert_redirected_to edit_document_url(Document.last)
    assert_equal Document.last.owner, team
  end

  test 'should broadcast after create' do
    sign_in create(:user)
    post documents_url, params: { document: { title: 'New document' } }
    document = Document.last
    doc_params = JSON.parse ApplicationController.render(document)
    assert_broadcast_on('documents_channel', document: doc_params, action: :create)
  end

  test 'should show error when failing to create' do
    sign_in create(:user)

    post documents_url(format: :json), params: { document: { title: '' } }

    assert_response :unprocessable_entity
    json = JSON.parse response.body
    assert_equal 'is too short (minimum is 2 characters)', json['title'][0]
  end
end
