# frozen_string_literal: true

require 'test_helper'

class DocumentsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  include ActionCable::TestHelper

  setup do
    Git.stubs(:init).returns(stub(config: true, add: true, status: stub(added: {}), commit: true))
  end

  test 'should redirect destroy when logged out' do
    document = create(:document)
    assert_no_difference('Document.count') do
      delete document_url(document)
    end
    assert_redirected_to new_user_session_path
  end

  test 'should destroy document' do
    document = create(:document, owner: create(:user))
    sign_in document.owner
    assert_difference('Document.count', -1) do
      delete document_url(document)
    end

    assert_redirected_to documents_url
  end

  test 'should broadcast after destroy' do
    document = create(:document, owner: create(:user))
    sign_in document.owner
    delete document_url(document)
    doc_params = JSON.parse ApplicationController.render(document)
    assert_broadcast_on('documents_channel', document: doc_params, action: :destroy)
  end

  test 'should not destroy unless owner' do
    document = create(:document, owner: create(:user))
    sign_in create(:user)
    assert_raise Pundit::NotAuthorizedError do
      delete document_url(document)
    end
  end
end
