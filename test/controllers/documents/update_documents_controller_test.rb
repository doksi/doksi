# frozen_string_literal: true

require 'test_helper'

class DocumentsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  include ActionCable::TestHelper

  setup do
    Git.stubs(:init).returns(stub(config: true, add: true, status: stub(added: {}), commit: true))
  end

  test 'should redirect edit when logged out' do
    document = create(:document)

    get edit_document_url(document)
    assert_redirected_to new_user_session_path

    patch document_url(document), params: { document: { title: document.title } }
    assert_redirected_to new_user_session_path
  end

  test 'should get edit' do
    document = create(:document, owner: create(:user))
    sign_in document.owner
    get edit_document_url(document)
    assert_response :success
  end

  test 'should update document' do
    status = create(:status)
    document = create(:open_document, owner: create(:user))
    sign_in document.owner
    patch document_url(document), params: { document: {
      title: document.title,
      classification_id: document.classifications.first.id,
      status_id: status.id
    } }
    assert_redirected_to edit_document_url(document)
    assert_equal document.classifications.first, Document.find(document.id).classification
    assert_equal status, Document.find(document.id).status
  end

  test 'should broadcast after update' do
    document = create(:document, owner: create(:user))
    sign_in document.owner
    document.title = 'New title'

    patch document_url(document), params: { document: { title: document.title } }

    document = Document.find_by id: document.id
    doc_params = JSON.parse ApplicationController.render(document)
    doc_channel = DocumentChannel.broadcasting_for(document)

    assert_broadcast_on('documents_channel', document: doc_params, action: :update)
    assert_broadcast_on(doc_channel, document: doc_params)
  end

  test 'should add blocks to document' do
    document = create(:document, owner: create(:user))
    sign_in document.owner
    blocks = [
      { type: 'Paragraph', content: 'first block' },
      { type: 'Paragraph', content: 'second block' }
    ]

    assert_difference('document.blocks.count', 2) do
      patch document_url(document), params: { document: { blocks: blocks } }
    end

    # reload document
    document = Document.find(document.id)

    assert_equal 'Paragraph', document.blocks.first.type
    assert_equal 'first block', document.blocks.first.content
    assert_equal 'Paragraph', document.blocks.last.type
    assert_equal 'second block', document.blocks.last.content
  end

  test 'should reorder blocks for document' do
    document = create(:document, owner: create(:user))
    block1 = create(:block)
    block2 = create(:heading_block)
    document.blocks << block1
    document.blocks << block2
    sign_in document.owner

    blocks = [block1, block2]

    reverse_blocks = JSON.parse blocks.reverse.to_json
    patch document_url(document), params: { document: {
      blocks: reverse_blocks
    } }

    assert_equal 2, document.blocks.count
    assert_equal 0, Block.find(blocks[1].id).order
    assert_equal 1, Block.find(blocks[0].id).order
  end

  test 'should add quotes to document' do
    document = create(:document, owner: create(:user))
    sign_in document.owner

    blocks = [
      { type: 'Quote', content: 'first quote' },
      { type: 'Quote', content: 'second quote', source: 'my source' }
    ]

    assert_difference('document.blocks.count', 2) do
      patch document_url(document), params: { document: { blocks: blocks } }
    end

    # reload document
    document = Document.find(document.id)

    assert_equal 'Quote', document.blocks.first.type
    assert_equal 'first quote', document.blocks.first.content
    assert_equal 'Quote', document.blocks.last.type
    assert_equal 'second quote', document.blocks.last.content
  end

  test 'should not edit unless owner' do
    document = create(:document, owner: create(:user))
    sign_in create(:user)
    assert_raise Pundit::NotAuthorizedError do
      get edit_document_url(document)
    end
  end

  test 'should not update unless owner' do
    status = create(:status)
    document = create(:open_document, owner: create(:user))
    sign_in create(:user)
    assert_raise Pundit::NotAuthorizedError do
      patch document_url(document), params: { document: {
        title: 'New Title',
        classification_id: document.classifications.first.id,
        status_id: status.id
      } }
    end
  end

  test 'should update document with properties' do
    prop = create(:property, default: 'Default Value')
    document = create(:document)
    sign_in document.owner

    document_params = {
      properties: [
        { id: prop.id, value: 'Prop Value' }
      ]
    }

    assert_difference('document.properties.count', 1) do
      patch document_url(document), params: { document: document_params }
    end

    assert_equal prop, document.assigned_properties.first.property
    assert_equal 'Prop Value', document.assigned_properties.first.value
  end

  test 'should show error when failing to update' do
    document = create(:document)
    sign_in document.owner

    patch document_url(document, format: :json), params: { document: { title: '' } }

    assert_response :unprocessable_entity
    json = JSON.parse response.body
    assert_equal 'is too short (minimum is 2 characters)', json['title'][0]
  end
end
