# frozen_string_literal: true

require 'test_helper'

class DocumentsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  include ActionCable::TestHelper

  setup do
    Git.stubs(:init).returns(stub(config: true, add: true, status: stub(added: {}), commit: true))
  end

  test 'should show document' do
    document = create(:document, visibility: :public)
    get document_url(document)
    assert_response :success
  end

  test 'should not show private unless owner' do
    document = create(:document, owner: create(:user), visibility: :private)
    sign_in create(:user)
    assert_raise Pundit::NotAuthorizedError do
      get document_url(document)
    end
  end
end
