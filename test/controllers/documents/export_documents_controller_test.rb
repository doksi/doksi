# frozen_string_literal: true

require 'test_helper'

class DocumentsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  test 'should export to pdf' do
    document = create(:document)
    job = mock
    job.expects(:job_id).returns(123)
    ExportDocumentJob.expects(:perform_later).returns(job)
    get document_export_url(document, format: :json)
    assert_response :success
  end

  test 'should fetch export job details' do
    document = create(:document)
    ActiveJob::Status.expects(:get).with('123').returns(status)
    get document_export_url(document, format: :json), params: { job_id: 123 }
    assert_response :success
  end

  test 'should export synchronous to pdf' do
    document = create(:document)
    status = {
      status: {
        error: 'my-error',
        step: 42,
        state: 'my-state'
      },
      result: {
        data: Base64.encode64('my-result'),
        filename: '/my/file'
      }
    }
    ExportDocumentJob.expects(:perform_now).returns(status)
    get document_export_url(document)
    assert_response :success
  end
end
