# frozen_string_literal: true

require 'test_helper'

class TeamsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  include ActionCable::TestHelper

  test 'should redirect new when logged out' do
    get new_team_url
    assert_redirected_to new_user_session_path

    assert_no_difference('Team.count') do
      post teams_url, params: { team: { description: 'My new team', name: 'Test Team' } }
    end

    assert_redirected_to new_user_session_path
  end

  test 'should get new' do
    sign_in create(:user)
    get new_team_url
    assert_response :success
  end

  test 'should create team' do
    user = create(:user)
    role = create(:role, name: 'Team Admin')
    sign_in user

    assert_difference('Team.count') do
      post teams_url, params: { team: { description: 'My new team', name: 'Test Team' } }
    end

    assert_redirected_to team_url(Team.last)
    assert_includes Team.last.users, user
    assert_equal role, RoleAssignment.where(subject: user, object: Team.last).first.role
  end

  test 'should show error when failing to create' do
    sign_in create(:user)
    post teams_url(format: :json), params: { team: { name: '' } }
    assert_response :unprocessable_entity
    json = JSON.parse response.body
    assert_equal 'is too short (minimum is 2 characters)', json['name'][0]
  end
end
