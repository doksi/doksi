# frozen_string_literal: true

require 'test_helper'

class TeamsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  include ActionCable::TestHelper

  test 'should show team' do
    sign_in create(:user)
    get team_url(create(:team))
    assert_response :success
  end
end
