# frozen_string_literal: true

require 'test_helper'

class TeamsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  include ActionCable::TestHelper

  test 'should redirect destroy when logged out' do
    team = create(:team)
    assert_no_difference('Team.count') do
      delete team_url(team)
    end
    assert_redirected_to new_user_session_path
  end

  test 'should destroy team' do
    team = create(:team)
    sign_in team.users.first
    assert_difference('Team.count', -1) do
      delete team_url(team)
    end

    assert_redirected_to teams_url
  end

  test 'should not destroy unless member' do
    team = create(:team)
    sign_in create(:user)
    assert_raise Pundit::NotAuthorizedError do
      delete team_url(team)
    end
  end
end
