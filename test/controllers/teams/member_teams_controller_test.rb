# frozen_string_literal: true

require 'test_helper'

class TeamsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  include ActionCable::TestHelper

  test 'should redirect members when logged out' do
    team = create(:team)
    post members_team_url(team)
    assert_redirected_to new_user_session_path
    delete members_team_url(team)
    assert_redirected_to new_user_session_path
  end

  test 'should add members' do
    user = create(:user, admin: true)
    role = create(:role, privileges: [
                    create(:privilege, resource: Document, verb: :show)
                  ])
    team = create(:team)

    sign_in user
    assert_difference('team.users.count') do
      post members_team_url(team, format: :json), params: { member: { user: { id: user.id }, role: role.id } }
    end

    assert_response :success
    json = JSON.parse response.body
    assert_equal team.id, json['id']
    assert_equal user.id, json['members'][0]['id']
    assert_equal role.id, json['members'][0]['role']['role_id']
  end

  test 'should change role' do
    user = create(:user, admin: true)
    team = create(:team)
    team.users << user

    role1 = create(:role, name: 'Role1')
    create(:privilege, resource: Team, verb: :edit, role: role1)

    role2 = create(:role, name: 'Role2')
    create(:privilege, resource: Team, verb: :show, role: role2)

    RoleAssignment.create(
      role: role1,
      subject: user,
      object: team
    )

    sign_in user
    post members_team_url(team, format: :json), params: { member: { user: { id: user.id }, role: role2.id } }

    assert_response :success
    json = JSON.parse response.body
    assert_equal team.id, json['id']
    assert_equal user.id, json['members'][0]['id']
    assert_equal role2.id, json['members'][0]['role']['role_id']
  end

  test 'should remove members' do
    user = create(:user, admin: true)
    role = create(:role, privileges: [
                    create(:privilege, resource: Document, verb: :show)
                  ])
    team = create(:team)
    team.users << user
    RoleAssignment.create(
      role: role,
      subject: user,
      object: team
    )

    sign_in user
    assert_difference('team.users.count', -1) do
      assert_difference('RoleAssignment.count', -1) do
        delete members_team_url(team, format: :json), params: { member: { user: { id: user.id } } }
      end
    end

    assert_response :success
    json = JSON.parse response.body
    assert_equal team.id, json['id']
    assert_equal 1, json['members'].length
    assert_not_equal user.id, json['members'][0]['id']
  end
end
