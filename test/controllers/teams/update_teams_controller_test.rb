# frozen_string_literal: true

require 'test_helper'

class TeamsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  include ActionCable::TestHelper

  test 'should redirect edit when logged out' do
    team = create(:team)
    get edit_team_url(team)
    assert_redirected_to new_user_session_path

    patch team_url(team), params: { team: { description: team.description, name: team.name } }
    assert_redirected_to new_user_session_path
  end

  test 'should get edit' do
    team = create(:team)
    sign_in team.users.first
    get edit_team_url(team)
    assert_response :success
  end

  test 'should not edit unless member' do
    team = create(:team)
    sign_in create(:user)
    assert_raise Pundit::NotAuthorizedError do
      get edit_team_url(team)
    end
  end

  test 'should update team' do
    team = create(:team)
    user1 = create(:user)
    user2 = create(:user)
    sign_in team.users.first
    users = [user1.id, user2.id]
    assert_difference('Team.find(team.id).users.length') do
      patch team_url(team),
            params: { team: { name: 'New name', user_ids: users } }
    end
    assert_redirected_to team_url(team)
    assert_equal Team.find(team.id).name, 'New name'
  end

  test 'should not update unless member' do
    team = create(:team)
    sign_in create(:user)
    assert_raise Pundit::NotAuthorizedError do
      patch team_url(team), params: { team: { name: 'New name' } }
    end
  end

  test 'should show error when failing to update' do
    team = create(:team)
    sign_in team.users.first

    patch team_url(team, format: :json), params: { team: { name: '' } }

    assert_response :unprocessable_entity
    json = JSON.parse response.body
    assert_equal 'is too short (minimum is 2 characters)', json['name'][0]
  end
end
