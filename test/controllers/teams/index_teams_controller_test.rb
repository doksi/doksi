# frozen_string_literal: true

require 'test_helper'

class TeamsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  include ActionCable::TestHelper

  test 'should get index' do
    sign_in create(:user)
    get teams_url
    assert_response :success
  end

  test 'should get user teams' do
    user = create(:user)
    create(:team, users: [user])
    create(:team)

    sign_in user
    get user_teams_url(user, format: :json)
    assert_response :success

    body = JSON.parse(response.body)
    assert_equal user.teams.count, body.count
    assert_equal(user.teams.ids, body.map { |x| x['id'] })
  end

  test 'should get teams not joined' do
    user = create(:user)
    create(:team, users: [user])
    team2 = create(:team)
    team3 = create(:team)

    sign_in user
    get teams_url(scope: :joinable, format: :json)
    assert_response :success

    body = JSON.parse(response.body)
    assert_equal 2, body.count
    assert_equal team2.id, body[0]['id']
    assert_equal team3.id, body[1]['id']
  end
end
