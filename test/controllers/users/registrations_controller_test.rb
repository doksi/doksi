# frozen_string_literal: true

require 'test_helper'

class Users::RegistrationsControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  setup do
    Git.stubs(:init).returns(stub(config: true, add: true, status: stub(added: {}), commit: true))
    @request.env['devise.mapping'] = Devise.mappings[:user]
  end

  test 'should index users' do
    create_list(:user, 5)
    create(:team, users: [User.first])
    create(:document, owner: User.first)
    create(:document, owner: Team.first)
    get :index, format: :json
    assert_response :success

    body = JSON.parse(response.body)
    assert_equal User.all.count, body.count
    assert_equal(User.all.ids, body.map { |x| x['id'] })
  end

  test 'should index users logged in' do
    user = create(:user)
    other_users = create_list(:user, 5)

    sign_in user
    get :index, format: :json
    assert_response :success

    body = JSON.parse(response.body)
    assert_equal other_users.count, body.count
    assert_equal(other_users.map(&:id), body.map { |x| x['id'] })
  end

  test 'should show user' do
    user = create(:user)
    get :show, params: { id: user }
    assert_response :success
    assert_select 'user'
  end

  test 'should redirect settings when logged out' do
    get :edit
    assert_redirected_to new_user_session_path

    put :edit, params: { user: { email: 'test@mail.com' } }
    assert_redirected_to new_user_session_path
  end

  test 'should show settings when logged in' do
    user = create(:user)
    sign_in user
    get :edit
    assert_response :success
  end

  test 'should update settings when logged in' do
    user = create(:user)
    sign_in user
    assert_not_equal 'test@mail.com', user.email
    put :update, params: { user: { email: 'test@mail.com' } }
    assert_redirected_to settings_path
    assert_equal 'test@mail.com', User.find(user.id).email
  end

  test 'should redirect remove when logged out' do
    delete :destroy
    assert_redirected_to new_user_session_path
  end

  test 'should remove user when logged in' do
    user = create(:user)
    sign_in user
    assert_difference 'User.count', -1 do
      delete :destroy
      assert_redirected_to root_path
    end
  end

  test 'search should find all' do
    create_list(:user, 5)
    get :index, params: { q: '', format: :json }
    json_response = ActiveSupport::JSON.decode @response.body
    assert_equal json_response['results'].length, User.all.length
  end

  test 'search should match exact' do
    user = create(:user)
    get :index, params: { q: user.email, format: :json }
    json_response = ActiveSupport::JSON.decode @response.body
    assert_equal json_response['results'].length, 1
  end
end
