# frozen_string_literal: true

require 'test_helper'

class Users::OmniauthCallbacksControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  def omniauth_for(provider)
    json = File.read("test/fixtures/files/omniauth/#{provider}.json")
    JSON.parse json, object_class: OpenStruct
  end

  test 'should create user from google' do
    auth = omniauth_for(:google)
    @request.env['devise.mapping'] = Devise.mappings[:user]
    @request.env['omniauth.auth'] = auth

    assert_difference 'User.count' do
      get :google_oauth2
    end
    assert_redirected_to root_path
    assert_equal 'Successfully authenticated from Google account.',
                 flash[:notice]
  end

  test 'should create user from office 365' do
    auth = omniauth_for(:office365)
    @request.env['devise.mapping'] = Devise.mappings[:user]
    @request.env['omniauth.auth'] = auth

    assert_difference 'User.count' do
      get :microsoft_office365
    end
    assert_redirected_to root_path
    assert_equal 'Successfully authenticated from Office 365 account.',
                 flash[:notice]
  end

  test 'should log in using office 365' do
    auth = omniauth_for(:office365)
    @request.env['devise.mapping'] = Devise.mappings[:user]
    @request.env['omniauth.auth'] = auth

    user = User.from_omniauth(auth)

    assert_no_difference 'User.count' do
      get :microsoft_office365
    end
    assert_redirected_to root_path
  end

  test 'should create user from keycloak' do
    auth = omniauth_for(:keycloak)
    @request.env['devise.mapping'] = Devise.mappings[:user]
    @request.env['omniauth.auth'] = auth

    assert_difference 'User.count' do
      get :keycloakopenid
    end
    assert_redirected_to root_path
    assert_equal 'Successfully authenticated from Keycloak account.',
                 flash[:notice]
  end

  test 'should log in using keycloak' do
    auth = omniauth_for(:keycloak)
    @request.env['devise.mapping'] = Devise.mappings[:user]
    @request.env['omniauth.auth'] = auth

    user = User.from_omniauth(auth)

    assert_no_difference 'User.count' do
      get :keycloakopenid
    end
    assert_redirected_to root_path
  end
end
