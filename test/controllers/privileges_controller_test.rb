# frozen_string_literal: true

require 'test_helper'

class PrivilegesControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @role = create(:role)
  end

  test 'should require authentication for index' do
    get role_privileges_url(@role, format: :json)
    assert_response :unauthorized
  end

  test 'should require authentication for show' do
    privilege = create(:privilege, role: @role)
    get role_privilege_url(@role, privilege, format: :json)
    assert_response :unauthorized
  end

  test 'should require authentication for create' do
    assert_no_difference('Privilege.count') do
      post role_privileges_url(@role, format: :json), params: { privilege: { verb: 'show' } }
    end
    assert_response :unauthorized
  end

  test 'should require authentication for update' do
    privilege = create(:privilege, role: @role)
    patch role_privilege_url(@role, privilege, format: :json), params: { privilege: { verb: 'show' } }
    assert_response :unauthorized
  end

  test 'should require authentication for destroy' do
    privilege = create(:privilege, role: @role)
    assert_no_difference('Privilege.count') do
      delete role_privilege_url(@role, privilege, format: :json)
    end
    assert_response :unauthorized
  end

  test 'should require admin rights for index' do
    sign_in create(:user)
    get role_privileges_url(@role, format: :json)
    assert_response :forbidden
  end

  test 'should require admin rights for show' do
    sign_in create(:user)
    privilege = create(:privilege, role: @role)
    get role_privilege_url(@role, privilege, format: :json)
    assert_response :forbidden
  end

  test 'should require admin rights for create' do
    sign_in create(:user)
    assert_no_difference('Privilege.count') do
      post role_privileges_url(@role, format: :json), params: { privilege: { name: 'test' } }
    end
    assert_response :forbidden
  end

  test 'should require admin rights for update' do
    sign_in create(:user)
    privilege = create(:privilege, role: @role)
    patch role_privilege_url(@role, privilege, format: :json), params: { privilege: { name: 'test' } }
    assert_response :forbidden
  end

  test 'should require admin rights for destroy' do
    sign_in create(:user)
    privilege = create(:privilege, role: @role)
    assert_no_difference('Privilege.count') do
      delete role_privilege_url(@role, privilege, format: :json)
    end
    assert_response :forbidden
  end

  test 'should get index' do
    sign_in create(:user, admin: true)
    get role_privileges_url(@role, format: :json)
    assert_response :success
  end

  test 'should show privilege' do
    sign_in create(:user, admin: true)
    privilege = create(:privilege, role: @role)
    get role_privilege_url(@role, privilege, format: :json)
    assert_response :success
  end

  test 'should create privilege' do
    sign_in create(:user, admin: true)

    assert_difference('Privilege.count') do
      post role_privileges_url(@role, format: :json), params: { privilege: { verb: 'show', resource: 'Team' } }
    end

    assert_response :success
  end

  test 'should show error when failing to create' do
    sign_in create(:user, admin: true)

    assert_no_difference('Privilege.count') do
      post role_privileges_url(@role, format: :json), params: { privilege: { verb: 'foo', resource: 'Team' } }
    end

    assert_response :unprocessable_entity
    json = JSON.parse response.body
    assert_equal 'foo is not valid', json['verb'][0]
  end

  test 'should update privilege' do
    sign_in create(:user, admin: true)
    privilege = create(:privilege, role: @role)

    patch role_privilege_url(@role, privilege, format: :json), params: { privilege: { verb: 'show', resource: 'Team' } }
    assert_response :success
    assert_equal 'show', Privilege.find(privilege.id).verb
    assert_equal 'Team', Privilege.find(privilege.id).resource
  end

  test 'should show error when failing to update' do
    sign_in create(:user, admin: true)
    privilege = create(:privilege, role: @role)

    patch role_privilege_url(@role, privilege, format: :json), params: { privilege: { verb: 'foo' } }

    assert_response :unprocessable_entity
    json = JSON.parse response.body
    assert_equal 'foo is not valid', json['verb'][0]
  end

  test 'should destroy privilege' do
    sign_in create(:user, admin: true)
    privilege = create(:privilege, role: @role)

    assert_difference('Privilege.count', -1) do
      delete role_privilege_url(@role, privilege, format: :json)
    end

    assert_response :success
  end
end
