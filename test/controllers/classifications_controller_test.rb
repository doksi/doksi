# frozen_string_literal: true

require 'test_helper'

class ClassificationsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  include ActionCable::TestHelper

  test 'should get index' do
    get classifications_url(format: :json)
    assert_response :success
  end

  test 'should show classification' do
    classification = create(:classification)
    get classification_url(classification, format: :json)
    assert_response :success
  end

  test 'should redirect create when logged out' do
    assert_no_difference('Classification.count') do
      post classifications_url, params: { classification: { name: 'test' } }
    end
    assert_redirected_to new_user_session_path
  end

  test 'should redirect update when logged out' do
    classification = create(:classification)
    patch classification_url(classification), params: { classification: { name: 'test' } }
    assert_redirected_to new_user_session_path
  end

  test 'should redirect destroy when logged out' do
    classification = create(:classification)
    assert_no_difference('Classification.count') do
      delete classification_url(classification)
    end
    assert_redirected_to new_user_session_path
  end

  test 'should create classification' do
    sign_in create(:user)

    assert_difference('Classification.count') do
      post classifications_url, params: { classification: { name: 'test' } }
    end

    assert_redirected_to classification_url(Classification.last)
  end

  test 'should create team classification' do
    sign_in create(:user)
    team = create(:team)

    assert_difference('team.classifications.count') do
      post classifications_url, params: { classification: { name: 'test', owner_id: team.id, owner_type: 'Team' } }
    end

    assert_redirected_to classification_url(Classification.last)
    assert_equal team, Classification.last.owner
    assert_includes team.classifications, Classification.last
  end

  test 'should broadcast after create' do
    sign_in create(:user)
    team = create(:team)

    post classifications_url, params: { classification: { name: 'test', owner_id: team.id, owner_type: 'Team' } }

    team_channel = TeamChannel.broadcasting_for(Classification.last.owner)
    team_params = JSON.parse ApplicationController.render(Classification.last.owner)

    assert_broadcast_on(team_channel, team: team_params)
  end

  test 'should update classification' do
    sign_in create(:user)
    classification = create(:classification)

    patch classification_url(classification), params: { classification: { name: 'New name' } }
    assert_redirected_to classification_url(classification)
    assert_equal 'New name', Classification.find(classification.id).name
  end

  test 'should broadcast after update' do
    sign_in create(:user)
    classification = create(:classification, owner: create(:team))

    patch classification_url(classification), params: { classification: { name: 'New name' } }

    team_channel = TeamChannel.broadcasting_for(classification.owner)
    team_params = JSON.parse ApplicationController.render(classification.owner)

    assert_broadcast_on(team_channel, team: team_params)
  end

  test 'should destroy classification' do
    sign_in create(:user)
    team = create(:team)
    classification = create(:classification, owner: team)

    assert_difference('team.classifications.count', -1) do
      delete classification_url(classification)
    end

    assert_redirected_to classifications_url
  end

  test 'should broadcast after destroy' do
    sign_in create(:user)
    classification = create(:classification, owner: create(:team))

    team_channel = TeamChannel.broadcasting_for(classification.owner)
    team_params = JSON.parse ApplicationController.render(classification.owner)

    delete classification_url(classification)
    assert_broadcast_on(team_channel, team: team_params)
  end
end
