# frozen_string_literal: true

require 'test_helper'

class PagesControllerTest < ActionDispatch::IntegrationTest
  test 'should get root' do
    get root_url
    assert_response :success
  end

  test 'should login with special param' do
    user = create(:user)
    get root_url, params: { _login: user.id }
    assert_response :success
    assert_select '#nav-auth a.nav-link', count: 0, text: 'Sign in'
    assert_select '#nav-auth users-menu', count: 1
  end
end
