# frozen_string_literal: true

require 'test_helper'

class RolesControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  test 'should require authentication for index' do
    get roles_url(format: :json)
    assert_response :unauthorized
  end

  test 'should require authentication for show' do
    role = create(:role)
    get role_url(role, format: :json)
    assert_response :unauthorized
  end

  test 'should require authentication for create' do
    assert_no_difference('Role.count') do
      post roles_url(format: :json), params: { role: { name: 'test' } }
    end
    assert_response :unauthorized
  end

  test 'should require authentication for update' do
    role = create(:role)
    patch role_url(role, format: :json), params: { role: { name: 'test' } }
    assert_response :unauthorized
  end

  test 'should require authentication for destroy' do
    role = create(:role)
    assert_no_difference('Role.count') do
      delete role_url(role, format: :json)
    end
    assert_response :unauthorized
  end

  test 'should require admin rights for index' do
    sign_in create(:user)
    get roles_url(format: :json)
    assert_response :forbidden
  end

  test 'should require admin rights for show' do
    sign_in create(:user)
    role = create(:role)
    get role_url(role, format: :json)
    assert_response :forbidden
  end

  test 'should require admin rights for create' do
    sign_in create(:user)
    assert_no_difference('Role.count') do
      post roles_url(format: :json), params: { role: { name: 'test' } }
    end
    assert_response :forbidden
  end

  test 'should require admin rights for update' do
    sign_in create(:user)
    role = create(:role)
    patch role_url(role, format: :json), params: { role: { name: 'test' } }
    assert_response :forbidden
  end

  test 'should require admin rights for destroy' do
    sign_in create(:user)
    role = create(:role)
    assert_no_difference('Role.count') do
      delete role_url(role, format: :json)
    end
    assert_response :forbidden
  end

  test 'should get index' do
    sign_in create(:user, admin: true)
    get roles_url(format: :json)
    assert_response :success
  end

  test 'should show role' do
    sign_in create(:user, admin: true)
    role = create(:role)
    get role_url(role, format: :json)
    assert_response :success
  end

  test 'should create role' do
    sign_in create(:user, admin: true)

    assert_difference('Role.count') do
      post roles_url(format: :json), params: { role: { name: 'test' } }
    end

    assert_response :success
  end

  test 'should show error when failing to create' do
    sign_in create(:user, admin: true)

    assert_no_difference('Role.count') do
      post roles_url(format: :json), params: { role: { name: '' } }
    end

    assert_response :unprocessable_entity
    json = JSON.parse response.body
    assert_equal "can't be blank", json['name'][0]
    assert_equal 'is too short (minimum is 2 characters)', json['name'][1]
  end

  test 'should update role' do
    sign_in create(:user, admin: true)
    role = create(:role)

    patch role_url(role, format: :json), params: { role: { name: 'New name' } }
    assert_response :success
    assert_equal 'New name', Role.find(role.id).name
  end

  test 'should show error when failing to update' do
    sign_in create(:user, admin: true)
    role = create(:role)

    patch role_url(role, format: :json), params: { role: { name: '' } }

    assert_response :unprocessable_entity
    json = JSON.parse response.body
    assert_equal "can't be blank", json['name'][0]
    assert_equal 'is too short (minimum is 2 characters)', json['name'][1]
  end

  test 'should destroy role' do
    sign_in create(:user, admin: true)
    role = create(:role)

    assert_difference('Role.count', -1) do
      delete role_url(role, format: :json)
    end

    assert_response :success
  end
end
