# frozen_string_literal: true

require 'test_helper'

class PermissionsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  include ActionCable::TestHelper

  setup do
    Git.stubs(:init).returns(stub(config: true, add: true, status: stub(added: {}), commit: true))
    @document = create(:document)
    @user = create(:user)
    @team = create(:team)
  end

  test 'should deny index when logged out' do
    get document_permissions_url(@document, format: :json)
    assert_response :unauthorized
  end

  test 'should deny show when logged out' do
    permission = create(:permission, object: @document)
    get document_permission_url(@document, permission, format: :json)
    assert_response :unauthorized
  end

  test 'should deny create when logged out' do
    assert_no_difference('Permission.count') do
      post document_permissions_url(@document, format: :json),
           params: { permission: { action: 'admin' } }
    end
    assert_response :unauthorized
  end

  test 'should deny update when logged out' do
    permission = create(:permission)
    patch document_permission_url(permission.object, permission, format: :json),
          params: { permission: { action: 'admin' } }
    assert_response :unauthorized
  end

  test 'should deny destroy when logged out' do
    permission = create(:permission)
    assert_no_difference('Permission.count') do
      delete document_permission_url(permission.object, permission, format: :json)
    end
    assert_response :unauthorized
  end

  test 'should get index' do
    sign_in @user
    get document_permissions_url(@document, format: :json)
    assert_response :success
  end

  test 'should show permission' do
    sign_in @user
    permission = create(:permission, object: @document)
    get document_permission_url(@document, permission, format: :json)
    assert_response :success
  end

  test 'should create permission' do
    sign_in @user
    assert_difference('Permission.count') do
      post document_permissions_url(@document, format: :json),
           params: { permission: { privilege: 'admin', subject_id: @user.id, subject_type: 'User' } }
    end
    assert_response :created
    assert_equal @user, Permission.last.subject
    assert_equal @document, Permission.last.object
    assert_equal 'admin', Permission.last.privilege
    assert_equal 'allow', Permission.last.mode
  end

  test 'should create team permission' do
    sign_in @user

    assert_difference('Permission.count') do
      post document_permissions_url(@document, format: :json),
           params: { permission: { privilege: 'admin', subject_id: @team.id, subject_type: 'Team' } }
    end

    assert_response :created
    assert_equal @team, Permission.last.subject
    assert_equal @document, Permission.last.object
    assert_equal 'admin', Permission.last.privilege
    assert_equal 'allow', Permission.last.mode
  end

  test 'should update permission' do
    sign_in @user
    permission = create(:permission, object: @document)

    patch document_permission_url(@document, permission, format: :json),
          params: { permission: { privilege: 'admin' } }
    assert_response :success
    assert_equal 'admin', Permission.find(permission.id).privilege
  end

  test 'should destroy permission' do
    sign_in @user
    permission = create(:permission, object: @document)

    assert_difference('Permission.count', -1) do
      delete document_permission_url(@document, permission, format: :json)
    end

    assert_response :success
  end
end
