# frozen_string_literal: true

require 'test_helper'

class StatusesControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  include ActionCable::TestHelper

  test 'should get index' do
    get statuses_url(format: :json)
    assert_response :success
  end

  test 'should show status' do
    status = create(:status)
    get status_url(status, format: :json)
    assert_response :success
  end

  test 'should redirect create when logged out' do
    assert_no_difference('Status.count') do
      post statuses_url, params: { status: { name: 'test' } }
    end
    assert_redirected_to new_user_session_path
  end

  test 'should redirect update when logged out' do
    status = create(:status)
    patch status_url(status), params: { status: { name: 'test' } }
    assert_redirected_to new_user_session_path
  end

  test 'should redirect destroy when logged out' do
    status = create(:status)
    assert_no_difference('Status.count') do
      delete status_url(status)
    end
    assert_redirected_to new_user_session_path
  end

  test 'should create status' do
    sign_in create(:user)

    assert_difference('Status.count') do
      post statuses_url, params: { status: { name: 'test' } }
    end

    assert_redirected_to status_url(Status.last)
  end

  test 'should create team status' do
    sign_in create(:user)
    team = create(:team)

    assert_difference('team.statuses.count') do
      post statuses_url, params: { status: { name: 'test', owner_id: team.id, owner_type: 'Team' } }
    end

    assert_redirected_to status_url(Status.last)
    assert_equal team, Status.last.owner
    assert_includes team.statuses, Status.last
  end

  test 'should broadcast after create' do
    sign_in create(:user)
    team = create(:team)

    post statuses_url, params: { status: { name: 'test', owner_id: team.id, owner_type: 'Team' } }

    team_channel = TeamChannel.broadcasting_for(Status.last.owner)
    team_params = JSON.parse ApplicationController.render(Status.last.owner)

    assert_broadcast_on(team_channel, team: team_params)
  end

  test 'should update status' do
    sign_in create(:user)
    status = create(:status)

    patch status_url(status), params: { status: { name: 'New name' } }
    assert_redirected_to status_url(status)
    assert_equal 'New name', Status.find(status.id).name
  end

  test 'should broadcast after update' do
    sign_in create(:user)
    status = create(:status, owner: create(:team))

    patch status_url(status), params: { status: { name: 'New name' } }

    team_channel = TeamChannel.broadcasting_for(status.owner)
    team_params = JSON.parse ApplicationController.render(status.owner)

    assert_broadcast_on(team_channel, team: team_params)
  end

  test 'should destroy status' do
    sign_in create(:user)
    team = create(:team)
    status = create(:status, owner: team)

    assert_difference('team.statuses.count', -1) do
      delete status_url(status)
    end

    assert_redirected_to statuses_url
  end

  test 'should broadcast after destroy' do
    sign_in create(:user)
    status = create(:status, owner: create(:team))

    team_channel = TeamChannel.broadcasting_for(status.owner)
    team_params = JSON.parse ApplicationController.render(status.owner)

    delete status_url(status)
    assert_broadcast_on(team_channel, team: team_params)
  end
end
