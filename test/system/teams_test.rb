# frozen_string_literal: true

require 'application_system_test_case'

class TeamsTest < ApplicationSystemTestCase
  include Devise::Test::IntegrationHelpers

  setup do
    sign_in create(:user)
    @team = create(:team)
  end

  test 'visiting the index' do
    visit teams_url
    assert_selector 'h1', text: 'Teams'
  end

  test 'creating a Team' do
    visit teams_url
    click_on 'New Team'

    fill_in 'Description', with: @team.description
    fill_in 'Name', with: @team.name
    click_on 'Save'

    assert_text 'Team was successfully created'
  end

  test 'updating a Team' do
    visit teams_url
    click_on 'Edit', match: :first

    fill_in 'Description', with: @team.description
    fill_in 'Name', with: @team.name
    click_on 'Save'

    assert_text 'Team was successfully updated'
  end

  test 'destroying a Team' do
    visit teams_url
    page.accept_confirm do
      click_on 'Delete', match: :first
    end

    assert_text 'Team was successfully destroyed'
  end
end
