# frozen_string_literal: true

require 'application_system_test_case'

class DocumentsTest < ApplicationSystemTestCase
  include Devise::Test::IntegrationHelpers

  setup do
    Git.stubs(:init).returns(stub(config: true, add: true, status: stub(added: {}), commit: true))
    sign_in create(:user)
    @document = create(:document)
  end

  test 'visiting the index' do
    visit documents_url
    assert_selector 'h1', text: 'Documents'
  end

  test 'creating a Document' do
    visit documents_url
    click_on 'New Document'

    fill_in 'document_title', with: @document.title
    click_on 'Save'

    assert_text 'Document was successfully created'
    click_on 'Cancel'
  end

  test 'creating a Document for team' do
    visit team_url(Team.last)

    click_on 'New document'

    fill_in 'document_title', with: @document.title
    click_on 'Save'

    assert_text 'Document was successfully created'
    click_on 'Cancel'
  end

  test 'updating a Document' do
    visit documents_url
    click_on 'Edit', match: :first

    fill_in 'document_title', with: @document.title
    click_on 'Save'

    assert_text 'Document was successfully updated'
    click_on 'Cancel'
  end

  test 'destroying a Document' do
    visit documents_url
    page.accept_confirm do
      click_on 'Delete', match: :first
    end

    assert_text 'Document was successfully destroyed'
  end
end
