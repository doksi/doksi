# frozen_string_literal: true

require 'application_system_test_case'
require 'mobile_system_test_case'

def test_navbar_visibility(visibility = :visible)
  %w[Pricing Documents Teams].each do |link|
    assert_selector 'nav .nav-link', text: link, visible: visibility
  end
end

def test_user_menu_visibility(visibility = :visible)
  ['Profile', 'Settings', 'Sign out'].each do |link|
    assert_selector 'nav .dropdown-item', text: link, visible: visibility
  end
end

class NavigationsTest < ApplicationSystemTestCase
  include Devise::Test::IntegrationHelpers

  test 'visiting the root' do
    visit root_url
    assert_selector 'nav .navbar-brand', text: 'DOKSI'
    assert_selector 'nav .navbar-toggler', visible: :hidden
    test_navbar_visibility
  end

  test 'toggling user menu' do
    sign_in create(:user)
    visit root_url
    test_user_menu_visibility :hidden
    click_on 'Avatar'
    test_user_menu_visibility :visible
    click_on 'Avatar'
    test_user_menu_visibility :hidden
  end
end

class NavigationsMobileTest < MobileSystemTestCase
  include Devise::Test::IntegrationHelpers

  test 'visiting the root' do
    sign_in create(:user)
    visit root_url
    assert_selector 'nav .navbar-brand', text: 'DOKSI'
    assert_selector 'nav .navbar-toggler'

    test_navbar_visibility :hidden

    find('nav .navbar-toggler').click
    test_navbar_visibility :visible
    test_user_menu_visibility :hidden

    click_on 'Avatar'
    test_user_menu_visibility :visible

    click_on 'Avatar'
    test_user_menu_visibility :hidden

    click_on 'Avatar'
    test_user_menu_visibility :visible

    find('nav .navbar-toggler').click
    test_navbar_visibility :hidden
    test_user_menu_visibility :hidden

    find('nav .navbar-toggler').click
    test_navbar_visibility :visible
    test_user_menu_visibility :hidden
  end
end
