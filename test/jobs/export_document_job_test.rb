# frozen_string_literal: true

require 'test_helper'

class ExportDocumentJobTest < ActiveJob::TestCase
  setup do
    Git.stubs(:init).returns(stub(config: true, add: true, status: stub(added: {}), commit: true))
  end

  def setup_mocks(document, format, archive, passphrase)
    document.expects(:export_prepare).with(format, passphrase)
    document.expects(:export_build).with(format)
    document.expects(:export_finalize).with(format, archive).returns('/my/path')

    file = mock
    file.expects(:read).returns('export-result')
    File.expects(:open).once.with('/my/path').returns(file)

    FileUtils.expects(:rm_rf).with("tmp/export/#{document.id}")
  end

  test 'should export to pdf' do
    document = create(:document)
    setup_mocks document, :pdf, :zip, 's3cr3t'
    ExportDocumentJob.perform_now(document, { format: :pdf, archive: :zip }, 's3cr3t')
  end

  test 'should export to epub' do
    document = create(:document)
    setup_mocks document, :epub, :zip, 's3cr3t'
    ExportDocumentJob.perform_now(document, { format: :epub, archive: :zip }, 's3cr3t')
  end

  test 'should export to html+zip' do
    document = create(:document)
    setup_mocks document, :html, :zip, 's3cr3t'
    ExportDocumentJob.perform_now(document, { format: :html, archive: :zip }, 's3cr3t')
  end

  test 'should export to html+tgz' do
    document = create(:document)
    setup_mocks document, :html, :tgz, 's3cr3t'
    ExportDocumentJob.perform_now(document, { format: :html, archive: :tgz }, 's3cr3t')
  end

  test 'should export to html+txz' do
    document = create(:document)
    setup_mocks document, :html, :txz, 's3cr3t'
    ExportDocumentJob.perform_now(document, { format: :html, archive: :txz }, 's3cr3t')
  end

  test 'should raise for unknown format' do
    document = create(:document)
    document.expects(:export_prepare).with(:foo, 's3cr3t')
    document.expects(:export_build).with(:foo)
    document.expects(:export_finalize).with(:foo, :bar).returns('/my/path')

    file = mock
    file.expects(:read).returns('export-result')
    File.expects(:open).once.with('/my/path').returns(file)

    status = mock
    status.expects(:update).once.with do |opts|
      opts[:state] == :error && opts[:error].include?('Unknown format foo')
    end
    status.expects(:update).at_least_once.with do |opts|
      opts[:state] != :error
    end
    ExportDocumentJob.any_instance.stubs(:status).returns(status)

    ExportDocumentJob.perform_now(document, { format: :foo, archive: :bar }, 's3cr3t')
  end

  test 'should raise for unknown archive' do
    document = create(:document)
    document.expects(:export_prepare).with(:html, 's3cr3t')
    document.expects(:export_build).with(:html)
    document.expects(:export_finalize).with(:html, :bar).returns('/my/path')

    file = mock
    file.expects(:read).returns('export-result')
    File.expects(:open).once.with('/my/path').returns(file)

    status = mock
    status.expects(:update).once.with do |opts|
      opts[:state] == :error && opts[:error].include?('Unknown archive format bar')
    end
    status.expects(:update).at_least_once.with do |opts|
      opts[:state] != :error
    end
    ExportDocumentJob.any_instance.stubs(:status).returns(status)

    ExportDocumentJob.perform_now(document, { format: :html, archive: :bar }, 's3cr3t')
  end
end
