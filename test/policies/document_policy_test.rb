# frozen_string_literal: true

require 'test_helper'

class DocumentPolicyTest < ActiveSupport::TestCase
  setup do
    Git.stubs(:init).returns(stub(config: true, add: true, status: stub(added: {}), commit: true))
    @alice = create(:user, username: 'alice')
    @bob = create(:user, username: 'bob')
    @dev = create(:team, name: 'dev')
    @ops = create(:team, name: 'ops')
    @document = create(:document)
  end

  test 'users can read public documents' do
    assert_permit @document.owner, @document, :show
    refute_permit @alice, @document, :show
    @document.visibility_public!
    assert_permit @user, @document, :show
  end

  test 'only owners can update documents' do
    assert_permit @document.owner, @document, :update
    refute_permit @alice, @document, :update
  end

  test 'only owners can destroy documents' do
    assert_permit @document.owner, @document, :destroy
    refute_permit @alice, @document, :destroy
  end

  test 'permissions allow user to access document' do
    refute_permit @alice, @document, :show
    refute_permit @alice, @document, :edit
    refute_permit @alice, @document, :destroy

    p = Permission.create(
      subject: @alice,
      object: @document,
      mode: :allow,
      privilege: :read
    )
    assert_permit @alice, @document, :show
    refute_permit @alice, @document, :edit
    refute_permit @alice, @document, :destroy

    p.update(privilege: :write)
    assert_permit @alice, @document, :show
    assert_permit @alice, @document, :edit
    refute_permit @alice, @document, :destroy

    p.update(privilege: :admin)
    assert_permit @alice, @document, :show
    assert_permit @alice, @document, :edit
    assert_permit @alice, @document, :destroy
  end

  test 'permissions allow team to access document' do
    @ops.users << @alice

    refute_permit @alice, @document, :show
    refute_permit @alice, @document, :edit
    refute_permit @alice, @document, :destroy

    p = Permission.create(
      subject: @ops,
      object: @document,
      mode: :allow,
      privilege: :read
    )
    assert_permit @alice, @document, :show
    refute_permit @alice, @document, :edit
    refute_permit @alice, @document, :destroy

    p.update(privilege: :write)
    assert_permit @alice, @document, :show
    assert_permit @alice, @document, :edit
    refute_permit @alice, @document, :destroy

    p.update(privilege: :admin)
    assert_permit @alice, @document, :show
    assert_permit @alice, @document, :edit
    assert_permit @alice, @document, :destroy
  end

  test 'roles allow user to access document' do
    user = create(:user)
    document = create(:document)
    role = create(:role)
    privilege = create(:privilege, role: role, resource: Team)
    create(:role_assignment, subject: user, object: document, role: role)

    refute_permit user, document, :show
    refute_permit user, document, :edit
    refute_permit user, document, :update
    refute_permit user, document, :destroy

    privilege.update(verb: :show, resource: Document)
    assert_permit user, document, :show
    refute_permit user, document, :edit
    refute_permit user, document, :update
    refute_permit user, document, :destroy

    privilege.update(verb: :edit)
    refute_permit user, document, :show
    assert_permit user, document, :edit
    assert_permit user, document, :update
    refute_permit user, document, :destroy

    privilege.update(verb: :destroy)
    refute_permit user, document, :show
    refute_permit user, document, :edit
    refute_permit user, document, :update
    assert_permit user, document, :destroy
  end
end
