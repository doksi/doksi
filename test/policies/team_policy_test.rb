# frozen_string_literal: true

require 'test_helper'

class TeamPolicyTest < ActiveSupport::TestCase
  setup do
    @team = create(:team)
    @user = create(:user)
    @member = @team.users.first
  end

  def test_scope; end

  def test_show
    assert_permit @member, @team, :show
    assert_permit @user, @team, :show
  end

  def test_update
    assert_permit @member, @team, :update
    refute_permit @user, @team, :update
  end

  def test_destroy
    assert_permit @member, @team, :destroy
    refute_permit @user, @team, :destroy
  end

  test 'roles allow user to access team' do
    user = create(:user)
    team = create(:team)
    role = create(:role)
    privilege = create(:privilege, role: role, resource: Team)
    create(:role_assignment, subject: user, object: team, role: role)

    refute_permit user, team, :edit
    refute_permit user, team, :update
    refute_permit user, team, :destroy

    privilege.update(verb: :edit)
    assert_permit user, team, :edit
    assert_permit user, team, :update
    refute_permit user, team, :destroy

    privilege.update(verb: :destroy)
    refute_permit user, team, :edit
    refute_permit user, team, :update
    assert_permit user, team, :destroy
  end
end
