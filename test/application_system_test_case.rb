# frozen_string_literal: true

require 'test_helper'

class ApplicationSystemTestCase < ActionDispatch::SystemTestCase
  driven_by :selenium, using: :headless_firefox, screen_size: [1400, 1400]

  def setup
    Capybara.server = :puma, { Silent: true, Threads: '1:1' }
  end
end
