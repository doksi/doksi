# frozen_string_literal: true

require 'test_helper'

class MobileSystemTestCase < ActionDispatch::SystemTestCase
  driven_by :selenium, using: :headless_firefox, screen_size: [375, 667]

  def setup
    Capybara.server = :puma, { Silent: true, Threads: '1:1' }
  end
end
