# Change log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Upcoming]
### Added
- Exported PDF documents are styled with a blue front page and font without serifs.
- Classified documents can now carry stamps and markings, ensuring that the reader is aware that the document contains
  classified information.
- Documents without a classification or status are shown as "Unclassified" and "No status".
- Documents without a visibility are shown as "Public".
- A small message is shown when document contains no content.
- The current user is automatically assigned role "Team Admin" when creating a new team.
- Buttons for applying formatting such as bold and italics are shown when editing table cell content.
- The Image block now supports uploading local files and inserting into the document.
- The page for editing documents has been redesigned with a pill navbar like the other pages.
- The Status of a Document can now carry a watermark.
- The container image is reduced in size by more than half.
- It is now possible to change the container manager used for starting export containers.

### Changed
- The repository was moved on GitLab, from `ephracis/doksi` to `doksi/doksi`. This means that all container images on `registry.gitlab.io/ephracis/doksi` are no longer available.

### Fixed
- Jinja templates were not processed when exporting documents.
- When creating a new document, controls for associations such as permissions and properties, where shown even though it
  is not possible to associate anything until the document is created.
- When creating a new team, controls for associations such as members and properties, where shown even though it is not
  possible to associate anything until the team is created.
- When deleting a document the user was not redirected to the listing page.
- When deleting a team the user was not redirected to the listing page.
- No errors were shown when trying to create an invalid document.
- No errors were shown when trying to create an invalid team.
- Documents without blocks were incorrectly assumed to be encrypted.
- The text "undefined" was shown when document had no copyright line.
- The navbar would disappear after scrolling one window height.
- The export button was misaligned.
- When running Doksi without a cache (default), the export job would hang.

## [0.7.0] - 2020-08-14
### Added
- The team and user settings page have been redesigned featuring a tabbed interface.
- It is possible to create custom document properties belonging to users and teams.
- Meta data is listed when showing a document.
- Statistics is shown alongside other meta data for documents, such as number of words and sentences.
- Membership in Teams use Roles to manage what the member can access.
- The new block type "Raw" allows you to add any ReST markup to your document.
- Make documents dynamic by using Jinja and JSON data.

### Fixed
- The user gravatar in the navbar menu was the same for all users.
- The LinkedIn link on user pages did not work.
- It was not possible to edit team documents.
- Paragraphs of types *tips*, *warnings* and *notes* were not rendered properly when exported.
- Buttons for edit and delete was shown on teams even though the user was not allowed to perform those actions.

## [0.6.0] - 2020-08-05
### Added
- Doksi can be configured using the file `config/settings.yml`.
- Loading indicators when listing documents, teams and users.
- Empty lists of teams and users are replaced with a message saying that the list is empty.
- An admin area where administrators can manage global classifications and statuses, and see version numbers, latest
  users, teams and documents.
- The ability to create custom properties that can be assigned to documents.
- Settings (such as visibility and encryption) and meta data (such as status, classification) have been split into two
  separate tabs when editing documents.

### Fixed
- No users were displayed on the dashboard when signed out.
- It was not possible to create a new document with blocks.
- When editing a document, the list of possible classifications and statuses was sometimes incorrect.

## [0.5.0] - 2020-07-31
### Added
- [Live demo environment](https://doksi.herokuapp.com).
- Documents can be exported to ePUB.
- Documents can be exported to HTML and compressed with Zip, GZip and LZMA.
- Export is now asynchronous, with a progress tracker showing the status of the export job.
- Encrypted document can be exported. Though the server will get access to plain text documents in order to build them,
  we never store the passphrase and delete the build files as soon as possible after the build.

## [0.4.0] - 2020-07-21
### Added
- Client side encryption of documents.
- Share documents with users by giving them specific permission (read, write or admin) to documents.
- Unencrypted documents can be exported to PDF.

## [0.3.0] - 2020-07-09
### Added
- Pretty icons make the sign in buttons much more fun to click on.
- The front page is more useful, with lists of documents, teams and users.
- A *Table* block type is added which displays tabular data.
- Documents get the new meta data *Status* which can be used to describe the current state of the document, such as "Draft" or "Final".

### Security
- All block content is sanitized before saving to the database.

## [0.2.0] - 2020-07-03
### Added
- A *Quote* block type is added which displays a quote and an optional source (author) of the quote.
- A *Code* block type is added which display source code with syntax highlighting.
- Documents get the new meta data *Classification* which can be used to describe how sensitive a document is, such as "Open" or "Top Secret".
- Paragraphs can be marked as a *Tip*, *Warning* or *Note* which gives it a special highlight compared to a normal paragraph.

### Fixed
- It was not possible to sign up with an provider using the same email as an existing user.

## [0.1.1] - 2020-06-03
### Fixed
- The container was not published.

## [0.1.0] - 2020-06-03
### Added
- Block editor with headings, paragraphs, lists and images
- Teams for grouping users and collaborating on documents
- Document meta data such as copyright notice
- Deployment using Docker Compose or OpenShift
- Authentication using Google, Microsoft or Keycloak
- Search bar

[Upcoming]: https://gitlab.com/doksi/doksi/-/compare/0.7.0...master
[0.7.0]: https://gitlab.com/doksi/doksi/-/compare/0.6.0...0.7.0
[0.6.0]: https://gitlab.com/doksi/doksi/-/compare/0.5.0...0.6.0
[0.5.0]: https://gitlab.com/doksi/doksi/-/compare/0.4.0...0.5.0
[0.4.0]: https://gitlab.com/doksi/doksi/-/compare/0.3.0...0.4.0
[0.3.0]: https://gitlab.com/doksi/doksi/-/compare/0.2.0...0.3.0
[0.2.0]: https://gitlab.com/doksi/doksi/-/compare/0.1.1...0.2.0
[0.1.1]: https://gitlab.com/doksi/doksi/-/compare/0.1.0...0.1.1
[0.1.0]: https://gitlab.com/doksi/doksi/-/compare/0329912a...0.1.0
