# frozen_string_literal: true

class AddKindToBlocks < ActiveRecord::Migration[6.0]
  def change
    add_column :blocks, :kind, :string
  end
end
