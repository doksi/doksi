# frozen_string_literal: true

class AddMetaToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :username, :string
    add_column :users, :fullname, :string
    add_column :users, :description, :string
    add_column :users, :time_zone, :string
    add_column :users, :country, :string
    add_column :users, :twitter, :string
    add_column :users, :facebook, :string
    add_column :users, :github, :string
    add_column :users, :gitlab, :string
    add_column :users, :linkedin, :string
    add_column :users, :department, :string
    add_column :users, :company, :string
  end
end
