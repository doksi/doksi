# frozen_string_literal: true

class AddClassificationToDocuments < ActiveRecord::Migration[6.0]
  def change
    add_reference :documents, :classification, null: true, foreign_key: true
  end
end
