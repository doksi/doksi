# frozen_string_literal: true

class CreateBlocks < ActiveRecord::Migration[6.0]
  def change
    create_table :blocks do |t|
      t.string :type
      t.string :content
      t.integer :order
      t.string :alignment, default: 'justify'
      t.integer :level, default: 1
      t.belongs_to :document, null: false, foreign_key: true

      t.timestamps
    end
  end
end
