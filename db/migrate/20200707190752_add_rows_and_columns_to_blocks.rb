# frozen_string_literal: true

class AddRowsAndColumnsToBlocks < ActiveRecord::Migration[6.0]
  def change
    add_column :blocks, :rows, :integer
    add_column :blocks, :columns, :integer
  end
end
