class CreateRole < ActiveRecord::Migration[6.0]
  def change

    create_table :roles do |t|
      t.string :name
      t.timestamps
    end

    create_table :privileges do |t|
      t.string :resource
      t.string :verb, default: 'view'
      t.belongs_to :role, null: false, foreign_key: true
      t.timestamps
    end

    create_table :role_assignments do |t|
      t.references :subject, polymorphic: true, null: false
      t.references :object, polymorphic: true, null: false
      t.belongs_to :role, null: false, foreign_key: true
      t.timestamps
    end
  end
end
