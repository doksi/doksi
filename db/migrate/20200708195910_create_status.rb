# frozen_string_literal: true

class CreateStatus < ActiveRecord::Migration[6.0]
  def change
    create_table :statuses do |t|
      t.string :name
      t.references :owner, polymorphic: true, null: true
      t.timestamps
    end
    add_reference :documents, :status, null: true, foreign_key: true
  end
end
