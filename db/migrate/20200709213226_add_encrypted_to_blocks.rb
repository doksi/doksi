class AddEncryptedToBlocks < ActiveRecord::Migration[6.0]
  def change
    add_column :blocks, :encrypted, :boolean
  end
end
