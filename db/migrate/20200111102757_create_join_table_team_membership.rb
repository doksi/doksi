# frozen_string_literal: true

class CreateJoinTableTeamMembership < ActiveRecord::Migration[6.0]
  def change
    create_join_table :teams, :users do |t|
      t.index %i[team_id user_id], unique: true
      t.index %i[user_id team_id]
    end
  end
end
