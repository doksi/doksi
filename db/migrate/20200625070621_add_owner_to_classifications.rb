# frozen_string_literal: true

class AddOwnerToClassifications < ActiveRecord::Migration[6.0]
  def change
    add_reference :classifications, :owner, polymorphic: true, null: true
  end
end
