class CreateProperties < ActiveRecord::Migration[6.0]
  def change
    create_table :properties do |t|
      t.string :name
      t.string :kind
      t.string :default
      t.string :description
      t.string :options, array: true
      t.references :owner, polymorphic: true, null: true

      t.timestamps
    end
    
    create_table :documents_properties do |t|
      t.integer 'document_id', null: false
      t.integer 'property_id', null: false
      t.string :value
      t.index %i[document_id property_id], unique: true
    end
  end
end
