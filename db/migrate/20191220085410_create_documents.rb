# frozen_string_literal: true

class CreateDocuments < ActiveRecord::Migration[6.0]
  def change
    create_table :documents do |t|
      t.string :title
      t.string :copyright
      t.references :owner, polymorphic: true

      t.timestamps
    end
  end
end
