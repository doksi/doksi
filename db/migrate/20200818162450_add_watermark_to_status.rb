class AddWatermarkToStatus < ActiveRecord::Migration[6.0]
  def change
    add_column :statuses, :watermark, :boolean
  end
end
