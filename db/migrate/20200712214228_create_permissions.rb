class CreatePermissions < ActiveRecord::Migration[6.0]
  def change
    create_table :permissions do |t|
      t.references :subject, polymorphic: true, null: false
      t.references :object, polymorphic: true, null: false
      t.integer :privilege, default: 0
      t.integer :mode, default: 0

      t.timestamps
    end
  end
end
