class AddTemplatingToDocuments < ActiveRecord::Migration[6.0]
  def change
    add_column :documents, :dynamic, :boolean, default: false
    add_column :documents, :data_url, :string
    add_column :documents, :data_string, :string
  end
end
