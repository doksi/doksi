# frozen_string_literal: true

class AddVisiblityToDocuments < ActiveRecord::Migration[6.0]
  def change
    add_column :documents, :visibility, :integer
  end
end
