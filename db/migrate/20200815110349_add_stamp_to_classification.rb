class AddStampToClassification < ActiveRecord::Migration[6.0]
  def change
    add_column :classifications, :title, :string
    add_column :classifications, :label, :string
    add_column :classifications, :description, :string
    add_column :classifications, :authority, :string
    add_column :classifications, :stamp, :boolean
    add_column :classifications, :color, :string
    add_column :classifications, :date_format, :string
    add_column :classifications, :double_border, :boolean
  end
end
