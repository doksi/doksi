# frozen_string_literal: true

class AddSourceToBlocks < ActiveRecord::Migration[6.0]
  def change
    add_column :blocks, :source, :string
  end
end
