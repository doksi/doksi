# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_08_18_162450) do

  create_table "blocks", force: :cascade do |t|
    t.string "type"
    t.string "content"
    t.integer "order"
    t.string "alignment", default: "justify"
    t.integer "level", default: 1
    t.integer "document_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "kind"
    t.string "source"
    t.integer "rows"
    t.integer "columns"
    t.boolean "encrypted"
    t.index ["document_id"], name: "index_blocks_on_document_id"
  end

  create_table "classifications", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "owner_type"
    t.integer "owner_id"
    t.string "title"
    t.string "label"
    t.string "description"
    t.string "authority"
    t.boolean "stamp"
    t.string "color"
    t.string "date_format"
    t.boolean "double_border"
    t.index ["owner_type", "owner_id"], name: "index_classifications_on_owner_type_and_owner_id"
  end

  create_table "documents", force: :cascade do |t|
    t.string "title"
    t.string "copyright"
    t.string "owner_type"
    t.integer "owner_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "classification_id"
    t.integer "visibility"
    t.integer "status_id"
    t.boolean "dynamic", default: false
    t.string "data_url"
    t.string "data_string"
    t.index ["classification_id"], name: "index_documents_on_classification_id"
    t.index ["owner_type", "owner_id"], name: "index_documents_on_owner_type_and_owner_id"
    t.index ["status_id"], name: "index_documents_on_status_id"
  end

  create_table "documents_properties", force: :cascade do |t|
    t.integer "document_id", null: false
    t.integer "property_id", null: false
    t.string "value"
    t.index ["document_id", "property_id"], name: "index_documents_properties_on_document_id_and_property_id", unique: true
  end

  create_table "permissions", force: :cascade do |t|
    t.string "subject_type", null: false
    t.integer "subject_id", null: false
    t.string "object_type", null: false
    t.integer "object_id", null: false
    t.integer "privilege", default: 0
    t.integer "mode", default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["object_type", "object_id"], name: "index_permissions_on_object_type_and_object_id"
    t.index ["subject_type", "subject_id"], name: "index_permissions_on_subject_type_and_subject_id"
  end

  create_table "privileges", force: :cascade do |t|
    t.string "resource"
    t.string "verb", default: "view"
    t.integer "role_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["role_id"], name: "index_privileges_on_role_id"
  end

  create_table "properties", force: :cascade do |t|
    t.string "name"
    t.string "kind"
    t.string "default"
    t.string "description"
    t.string "options"
    t.string "owner_type"
    t.integer "owner_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["owner_type", "owner_id"], name: "index_properties_on_owner_type_and_owner_id"
  end

  create_table "role_assignments", force: :cascade do |t|
    t.string "subject_type", null: false
    t.integer "subject_id", null: false
    t.string "object_type", null: false
    t.integer "object_id", null: false
    t.integer "role_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["object_type", "object_id"], name: "index_role_assignments_on_object_type_and_object_id"
    t.index ["role_id"], name: "index_role_assignments_on_role_id"
    t.index ["subject_type", "subject_id"], name: "index_role_assignments_on_subject_type_and_subject_id"
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "statuses", force: :cascade do |t|
    t.string "name"
    t.string "owner_type"
    t.integer "owner_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "watermark"
    t.index ["owner_type", "owner_id"], name: "index_statuses_on_owner_type_and_owner_id"
  end

  create_table "teams", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "teams_users", id: false, force: :cascade do |t|
    t.integer "team_id", null: false
    t.integer "user_id", null: false
    t.index ["team_id", "user_id"], name: "index_teams_users_on_team_id_and_user_id", unique: true
    t.index ["user_id", "team_id"], name: "index_teams_users_on_user_id_and_team_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "provider"
    t.string "uid"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "username"
    t.string "fullname"
    t.string "description"
    t.string "time_zone"
    t.string "country"
    t.string "twitter"
    t.string "facebook"
    t.string "github"
    t.string "gitlab"
    t.string "linkedin"
    t.string "department"
    t.string "company"
    t.boolean "admin"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "blocks", "documents"
  add_foreign_key "documents", "classifications"
  add_foreign_key "documents", "statuses"
  add_foreign_key "privileges", "roles"
  add_foreign_key "role_assignments", "roles"
end
