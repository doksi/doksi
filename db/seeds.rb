# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

unless Classification.any?
  [
    {
      name: 'EU Top Secret',
      stamp: true,
      title: 'Classified',
      description: 'In accordance with 2013/488/EU art 9(2)',
      color: '#ff0000',
      date_format: '%Y-%m-%d'
    },
    {
      name: 'EU Secret',
      stamp: true,
      title: 'Classified',
      description: 'In accordance with 2013/488/EU art 9(2)',
      color: '#ff0000',
      date_format: '%Y-%m-%d'
    },
    {
      name: 'EU Confidential',
      stamp: true,
      title: 'Classified',
      description: 'In accordance with 2013/488/EU art 9(2)',
      color: '#ff0000',
      date_format: '%Y-%m-%d'
    },
    {
      name: 'EU Restricted',
      stamp: true,
      title: 'Classified',
      description: 'In accordance with 2013/488/EU art 9(2)',
      color: '#ff0000',
      date_format: '%Y-%m-%d'
    },
    {
      name: 'NATO Top Secret',
      label: 'Cosmic Top Secret',
      stamp: true,
      title: 'Classified',
      color: '#ff0000',
      date_format: '%Y-%m-%d'
    },
    {
      name: 'NATO Secret',
      stamp: true,
      title: 'Classified',
      color: '#ff0000',
      date_format: '%Y-%m-%d'
    },
    {
      name: 'NATO Confidential',
      stamp: true,
      title: 'Classified',
      color: '#ff0000',
      date_format: '%Y-%m-%d'
    },
    {
      name: 'NATO Restricted',
      stamp: true,
      title: 'Classified',
      color: '#ff0000',
      date_format: '%Y-%m-%d'
    },
    {
      name: 'SE Kvalificerat Hemlig',
      label: 'Kvalificerat Hemlig',
      stamp: true,
      title: 'Hemlig',
      description: 'Enligt 15 kap. 2 § offentlighets- och sekretesslagen (2009:400)',
      color: '#ff0000',
      date_format: '%Y-%m-%d',
      double_border: true
    },
    {
      name: 'SE Hemlig',
      label: 'Hemlig',
      stamp: true,
      title: 'Hemlig',
      description: 'Enligt 15 kap. 2 § offentlighets- och sekretesslagen (2009:400)',
      color: '#ff0000',
      date_format: '%Y-%m-%d',
      double_border: true
    },
    {
      name: 'SE Konfidentiell',
      label: 'Konfidentiell',
      stamp: true,
      title: 'Hemlig',
      description: 'Enligt 15 kap. 2 § offentlighets- och sekretesslagen (2009:400)',
      color: '#ff0000',
      date_format: '%Y-%m-%d',
      double_border: true
    },
    {
      name: 'SE Begränsat Hemlig',
      label: 'Begränsat Hemlig',
      stamp: true,
      title: 'Hemlig',
      description: 'Enligt 15 kap. 2 § offentlighets- och sekretesslagen (2009:400)',
      color: '#ff0000',
      date_format: '%Y-%m-%d',
      double_border: true
    }
  ].each do |classification|
    c = Classification.find_or_create_by(name: classification[:name])
    c.update(classification)
    c.save
  end
end

unless Status.any?
  [
    { name: 'Draft', watermark: true },
    { name: 'In review', watermark: true },
    { name: 'Approved' },
    { name: 'Final' },
    { name: 'Deprecated', watermark: true }
  ].each do |status|
    s = Status.find_or_create_by(name: status[:name])
    s.update(status)
    s.save
  end
end

unless Property.any?
  [
    {
      name: 'Description',
      kind: 'String',
      description: 'Short description of the document.'
    },
    {
      name: 'Kind',
      kind: 'Enum',
      options: ['Report', 'Letter', 'Instruction', 'Guide', 'Thesis', 'White paper', 'Manuscript', 'Other'],
      default: 'Other',
      description: 'What kind of document this is.'
    }
  ].each do |prop|
    p = Property.find_or_create_by(name: prop[:name])
    p.update(prop)
    p.save
  end
end

unless Role.any?
  [
    {
      name: 'Reader',
      privileges: [
        { resource: 'Document', verb: 'show' },
        { resource: 'Team', verb: 'show' },
        { resource: 'Doksi', verb: 'show' }
      ]
    },
    {
      name: 'Team Admin',
      privileges: [
        { resource: 'Team', verb: 'show' },
        { resource: 'Team', verb: 'edit' },
        { resource: 'Team', verb: 'destroy' }
      ]
    },
    {
      name: 'Team Operator',
      privileges: [
        { resource: 'Team', verb: 'show' },
        { resource: 'Team', verb: 'edit' }
      ]
    }
  ].each do |role|
    r = Role.find_or_create_by(name: role[:name])
    r.save
    role[:privileges].each do |priv|
      p = r.privileges.find_or_create_by(priv)
      p.save
    end
  end
end
