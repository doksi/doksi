# Doksi

[Doksi](https://doksi.gitlab.io) is an open source document management system. It stores files as RST inside
a Git repository and provides a web application for editing, managing and building (exporting) the documents.

It can run [in the cloud](https://doksi.herokuapp.com) or on your premises using our official container images.

![Doksi in the browser](app/assets/images/screenshots/window-browser.png)

![Doucment as PDF](app/assets/images/screenshots/window-pdf.png)

## Deployment

### Using Ansible

The best way to deploy Doksi is to use [our official Ansible collection](https://galaxy.ansible.com/ephracis/doksi).

### Using Docker or Podman

Doksi uses a container to run the export jobs. You must thus start the Doksi
container in a way that it can pull and run containers.

In Podman this can be achieved by mounting the correct folders into the Doksi
container:

```shell
podman run -v /sys/fs/cgroup:/sys/fs/cgroup \
           -v /:/rootfs:ro \
           -v /var/run:/var/run \
           -v /sys:/sys:ro \
           -v /var/lib/containers:/var/lib/containers \
           -v /dev/disk:/dev/disk:ro \
           -e RAILS_ENV=development \
           -p 3000:3000 \
           --privileged \
           docker.io/doksi/doksi
```

When using Docker you may instead opt to run the Docker Daemon in a separate
container, and link that container to the Doksi container:

```shell
# start a docker daemon
docker run -it \
           -d \
           --name docker \
           --privileged \
           docker:dind \
           dockerd --host=tcp://0.0.0.0:2375

# start doksi
docker run -it \
           --rm \
           --link docker:docker \
           -e DOCKER_HOST=tcp://docker \
           -e RAILS_ENV=development \
           -p 3000:3000 \
           docker.io/doksi/doksi

```

Note that you must configure Doksi to use the correct container runtime. See
[Configuration](#configuration) for details on how to do this.

#### Development mode

By setting `RAILS_ENV=development`, Doksi will start in *development* mode which
generates a bunch of fake data. You can sign in as one of the users by using the
`_login` parameter, and make yourself admin by using the `_admin` parameter.

For example, to sign in as user 1 and also make that user admin, just go to
http://localhost:5000?_login=1&_admin=true.

#### Production mode

To switch to production mode use `RAILS_ENV=production` instead. This will turn
off the ability to use `_login` and `_admin`.

Production mode requires an external PostgreSQL database. Use the following environment variables to connect to it:

- `DATABASE_HOSTNAME` - Host or IP to PostgreSQL server. Default: `postgres`)
- `DATABASE_POST` - Port number. Default: `5432`
- `DATABASE_USER` - The user to connect as. Default: `postgres`.
- `DATABASE_PASS` - The password for the user. Default: `changeme`.
- `DATABASE_NAME` - The name of the database to create. Default: `doksi`.

For example, you can run the following to start PostgreSQL inside a container next to the Doksi container:

```shell
podman run -d \
           -p 5432:5432 \
           -e POSTGRES_PASSWORD=changeme \
           docker.io/postgres:12
podman run ... \
           -e RAILS_ENV=production \
           -e DATABASE_HOSTNAME=$(hostname)
           docker.io/doksi/doksi
```

#### Configuration

Configuration of Doksi is made by editing the file [config/settings.yml](config/settings.yml). It is full of comments
describing each setting and there is even support for using Embedded Ruby (ERB) inside the file for dynamic values.

Create your own copy of this file and mount it into `/doksi/config/settings.local.yml`. This allows you to override
only part of the settings and leave the other at their default values.

```shell
# assuming your settings are at $PWD/settings.yml
podman run ... \
           -v $PWD/settings.yml:/doksi/config/settings.local.yml \
           docker.io/doksi/doksi
```

### To Kubernetes or OpenShift

To deploy to OpenShift start by cloning or downloading the repository. Then apply the resources:

```shell
# with kubectl
kubectl apply -f kubernetes/psql/*
kubectl apply -f kubernetes/rails/*

# with oc
oc apply -f kubernetes/psql/*
oc apply -f kubernetes/rails/*
```

If you are using OpenShift, open the Administrator console and go to Networking > Routes to find the
URL to the Doksi app.

For Kubernetes you may need to manually create an Ingress to access the application.

### To Heroku

Install [Heroku CLI](https://devcenter.heroku.com/articles/heroku-command-line). Then clone or download the repository and run the following:

```shell
heroku login
heroku git:remote -a doksi
git push heroku master
heroku run --app=doksi --exit-code -- rails db:migrate
```

:warning: **OBS!** When running on Heroku there is no way to export documents since that requires the ability to run containers, which is not possible on Heroku.

## Development

Before you can start developing you need to install Ruby, Python and NodeJS.

Then install all dependencies:

```shell
gem install bundler
npm install yarn
bundle install
yarn install
pip install -r requirements.txt
```

### Run automatic tests

```shell
# test the backend
bin/rails test

# test the frontend
yarn test
```

### Run locally

```shell
# run just the puma server
bin/rails server

# run via foreman for automatic reload when files changes
bundle exec foreman start -f Procfile.dev
```

Doksi is now available on http://localhost:5000.

### Run in containers with Keycloak

There is an Ansible playbook that will set up a production-like environment
using Docker Compose, and configure Keycloak with some predefined user accounts.

```shell
ansible-playbook ansible/deploy.yml
```

After Ansible is finished Doksi will be available on http://localhost:5000.
You may use Keycloak to sign in, using the username `alice` and password
`changeme`.

To administer Keycloak (for example create more users) go to
http://localhost:8080.

To bring everything down:

```shell
docker-compose down
```
