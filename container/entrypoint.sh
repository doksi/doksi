#!/bin/ash
set -e

if [[ $RAILS_ENV == "production" ]]; then
  echo "Waiting for Postgres to start..."
  while ! nc -z ${DATABASE_HOST} ${DATABASE_PORT}; do sleep 0.1; done
  echo "Postgres is up"
fi

# Remove a potentially pre-existing server.pid for Rails.
rm -f /doksi/tmp/pids/server.pid

bundle exec bin/rails db:migrate
bundle exec bin/rails db:seed

if [[ $RAILS_ENV == "development" ]]; then
  bundle exec bin/rails db:reset
  bundle exec bin/rails faker
fi

bundle exec bin/rails assets:precompile

# Then exec the container's main process (what's set as CMD in the Dockerfile).
exec "$@"
