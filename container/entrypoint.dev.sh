#!/bin/ash
set -xe

echo "Waiting for Postgres to start..."
while ! nc -z ${DATABASE_HOST} ${DATABASE_PORT}; do sleep 0.1; done
echo "Postgres is up"

# Remove a potentially pre-existing server.pid for Rails.
rm -f /doksi/tmp/pids/server.pid
mkdir -p tmp/pids

# Reset modules
rm -rf node_modules
yarn install

# Reset database
bundle exec bin/rails db:reset
bundle exec bin/rails faker

# Then exec the container's main process (what's set as CMD in the Dockerfile).
exec "$@"
