# frozen_string_literal: true

module DocumentsHelper
  def classifications_tag(document, options)
    options = {
      include_blank: 'Unclassified',
      'v-model' => 'document.classification_id'
    }.merge(options)

    select_tag 'document[classification_id]',
               options_from_collection_for_select(document.classifications, 'id', 'name'),
               options
  end

  def statuses_tag(document, options)
    options = {
      include_blank: 'No status',
      'v-model' => 'document.status_id'
    }.merge(options)

    select_tag 'document[status_id]',
               options_from_collection_for_select(document.statuses, 'id', 'name'),
               options
  end
end
