# frozen_string_literal: true

module ApplicationHelper
  def navbar_link(title, path, page_matcher = nil)
    opts = { class: 'nav-link' }
    page_matcher ||= path

    match = if page_matcher.is_a? Hash
              if page_matcher[:controller] && page_matcher[:action]
                current_page?(page_matcher)
              elsif page_matcher[:controller]
                params[:controller].to_s == page_matcher[:controller].to_s
              else
                params[:action].to_s == page_matcher[:action].to_s
              end
            else
              current_page?(page_matcher)
            end

    opts = { class: 'nav-link active' } if match

    link_to title, path, opts
  end

  def allowed_to?(method, resource)
    return false unless request.env['warden']

    policy(resource).send("#{method}?")
  end
end
