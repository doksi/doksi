# frozen_string_literal: true

class ApplicationPolicy
  attr_reader :user, :record

  def initialize(user, record)
    @user = user
    @record = record
  end

  def index?
    false
  end

  def show?
    false
  end

  def create?
    false
  end

  def new?
    create?
  end

  def update?
    edit?
  end

  def edit?
    false
  end

  def destroy?
    false
  end

  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
      scope.all
    end
  end

  def permission_for?(action)
    subjects = [user]
    subjects += user.teams if user.is_a? User
    Permission.allows?(subjects, action, record)
  end

  def role_for?(verb)
    RoleAssignment.where(subject: user, object: record).each do |assignment|
      return true if assignment.role.privileges.where(resource: record.class.name, verb: verb).any?
    end
    false
  end
end
