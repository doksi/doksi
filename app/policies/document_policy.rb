# frozen_string_literal: true

class DocumentPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      result = scope.where(visibility: :public)
      result = result.or(scope.where(owner: user))
      result = result.or(scope.where(owner: user.teams)) if user
      result
    end
  end

  def show?
    return true if record.visibility_public?
    return false unless user
    return true if record.owner == user
    return true if record.owner.is_a?(Team) && record.owner.users.include?(user)
    return true if permission_for? :read
    return true if role_for? :show

    false
  end

  def edit?
    return false unless user
    return true if record.owner == user
    return true if record.owner.is_a?(Team) && record.owner.users.include?(user)
    return true if permission_for? :write
    return true if role_for? :edit

    false
  end

  def destroy?
    return false unless user
    return true if record.owner == user
    return true if record.owner.is_a?(Team) && record.owner.users.include?(user)
    return true if permission_for? :admin
    return true if role_for? :destroy

    false
  end
end
