# frozen_string_literal: true

class TeamPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve; end
  end

  def show?
    true
  end

  def edit?
    return false unless user
    return true if user.admin?
    return true if record.users.count == 1 && record.users.include?(user)
    return true if role_for? :edit

    false
  end

  def destroy?
    return false unless user
    return true if user.admin?
    return true if record.users.count == 1 && record.users.include?(user)
    return true if role_for? :destroy

    false
  end
end
