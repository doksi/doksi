# frozen_string_literal: true

class ExportDocumentJob < ApplicationJob
  include ActiveJob::Status

  queue_as :default

  def perform(document, request, passphrase)
    @document = document
    @request = request
    @result = {}
    @passphrase = passphrase
    @path = nil

    Rails.logger.info "Preparing export for #{@document.id}"
    prepare
    Rails.logger.info "Building document #{@document.id}"
    build
    Rails.logger.info "Finalizing document #{@document.id}"
    finalize

    Rails.logger.info "Export finished for document #{@document.id}"
    status.update(step: 3, state: :finished, result: @result)
  rescue StandardError => e
    error = e.message
    Rails.logger.error "Error occurred when exporting #{@document.id}: #{error}"

    unless Rails.env.production?
      trace = e.backtrace.map { |l| "<li>#{l}</li>" }
      error = "<strong>#{e.message}</strong><ul>#{trace.join}</ul>"
    end

    status.update(step: 3, state: :error, error: error)

  # rubocop:disable Style/RescueStandardError
  rescue
    Rails.logger.error "Unknown error occurred when exporting #{@document.id}"
    status.update(step: 3, state: :error, error: 'Unknown error occurred')
  end
  # rubocop:enable Style/RescueStandardError

  def prepare
    status.update(step: 1, state: :in_progress)
    @document.export_prepare(@request[:format], @passphrase)
  end

  def build
    status.update(step: 2, state: :in_progress)
    @document.export_build(@request[:format])
  end

  def finalize
    status.update(step: 3, state: :in_progress)
    path = @document.export_finalize(@request[:format], @request[:archive])

    data = File.open(path).read

    @result[:filename] = File.basename path
    @result[:data] = Base64.encode64 data
    @result[:mime] = format_to_mimetype @request[:format], @request[:archive]

    FileUtils.rm_rf @document.export_folder
  end

  private

  def format_to_mimetype(format, archive)
    case format.to_sym
    when :pdf
      'application/pdf;base64'
    when :epub
      'application/epub+zip'
    when :html
      case archive.to_sym
      when :zip
        'application/zip'
      when :tgz
        'application/gzip'
      when :txz
        'application/x-xz'
      else
        raise "Unknown archive format #{archive}"
      end
    else
      raise "Unknown format #{format}"
    end
  end
end
