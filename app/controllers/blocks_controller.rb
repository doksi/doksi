# frozen_string_literal: true

class BlocksController < ApplicationController
  # GET /blocks/code2html.json
  def code2html
    @language = params[:language]
    @code = Base64.decode64(params[:code])
    @html = Code.to_html(@code, @language)
  end

  # GET /blocks/rst2html.json
  def rst2html
    @rst = Base64.decode64(params[:rst])
    @html = Block.to_html(@rst)
  end
end
