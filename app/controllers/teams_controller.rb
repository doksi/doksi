# frozen_string_literal: true

class TeamsController < ApplicationController
  before_action :set_team, only: %i[show edit update destroy add_member remove_member]
  before_action :authenticate_user!, except: %i[index show]

  # GET /teams
  # GET /teams.json
  def index
    # /users/1/teams
    if params[:user_id]
      @user = User.find(params[:user_id])
      @teams = @user.teams

    # all teams not joined
    elsif params[:scope] == 'joinable'
      authenticate_user!
      @teams = Team.without_member(current_user)

    # all teams
    else
      @teams = Team.all
    end
  end

  # GET /teams/1
  # GET /teams/1.json
  def show; end

  # GET /teams/new
  def new
    @team = Team.new
  end

  # GET /teams/1/edit
  def edit
    authorize @team
  end

  # POST /teams
  # POST /teams.json
  def create
    @team = Team.new(team_params)
    @team.users << current_user
    success = @team.save

    if success
      default_role = Role.find_by(name: 'Team Admin')
      RoleAssignment.create!(
        object_type: Team,
        object_id: @team.id,
        subject_type: User,
        subject_id: current_user.id,
        role: default_role
      )
    end

    respond_to do |format|
      if success
        TeamsChannel.create @team
        format.html { redirect_to @team, notice: 'Team was successfully created.' }
        format.json { render :show, status: :created, location: @team }
      else
        format.html { render :new }
        format.json { render json: @team.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /teams/1
  # PATCH/PUT /teams/1.json
  def update
    authorize @team
    respond_to do |format|
      if @team.update(team_params)
        TeamsChannel.update @team
        TeamChannel.update @team
        format.html { redirect_to @team, notice: 'Team was successfully updated.' }
        format.json { render :show, status: :ok, location: @team }
      else
        format.html { render :edit }
        format.json { render json: @team.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /teams/1
  # DELETE /teams/1.json
  def destroy
    authorize @team
    @team.destroy
    respond_to do |format|
      format.html { redirect_to teams_url, notice: 'Team was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def add_member
    user = User.find(member_params[:user][:id])
    role = Role.find(member_params[:role])
    assignment = RoleAssignment.find_or_create_by(
      object_type: 'Team',
      object_id: @team.id,
      subject_type: 'User',
      subject_id: user.id
    )
    assignment.role = role
    assignment.save
    @team.users << user unless user.in?(@team.users)
    respond_to do |format|
      format.html { redirect_to @team, notice: 'Member was successfully added' }
      format.json { render :show, status: :ok, location: @team }
    end
  end

  def remove_member
    user = User.find(member_params[:user][:id])
    assignment = RoleAssignment.find_by(
      object_type: 'Team',
      object_id: @team.id,
      subject_type: 'User',
      subject_id: user.id
    )
    assignment&.destroy
    @team.users.delete(user)
    respond_to do |format|
      format.html { redirect_to @team, notice: 'Member was successfully removed' }
      format.json { render :show, status: :ok, location: @team }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_team
    @team = Team.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def team_params
    params.require(:team).permit(:name, :description, user_ids: [])
  end

  def member_params
    params.require(:member).permit(:role, user: [:id])
  end
end
