# frozen_string_literal: true

class RolesController < ApplicationController
  before_action :authenticate_user!, :authenticate_admin!
  before_action :set_role, only: %i[show edit update destroy]

  def index
    @roles = Role.all
  end

  def show; end

  def create
    @role = Role.new(role_params)

    respond_to do |format|
      if @role.save
        format.json { render :show, status: :created, location: @role }
      else
        format.json { render json: @role.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @role.update(role_params)
        format.json { render :show, status: :ok, location: @role }
      else
        format.json { render json: @role.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @role.destroy
    respond_to do |format|
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_role
    @role = Role.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def role_params
    params.require(:role).permit(:name)
  end
end
