# frozen_string_literal: true

class PagesController < ApplicationController
  def dashboard
    if current_user
      @my_teams = current_user.teams
      @other_teams = Team.without_member(current_user)
      @my_documents = current_user.documents
      @team_documents = policy_scope(current_user.team_documents)
      doc_ids = (@my_documents.empty? ? [] : @my_documents.ids) + (@team_documents.empty? ? [] : @team_documents.ids)
      @other_documents = policy_scope(Document.where('id NOT IN (?)', doc_ids))
      @users = User.where.not(id: current_user.id)
    else
      @documents = policy_scope(Document.all)
      @teams = policy_scope(Team.all)
    end
  end
end
