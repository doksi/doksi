# frozen_string_literal: true

class RoleAssignmentsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_role
  before_action :set_assignment, only: %i[show update destroy]
  before_action :set_subject, only: %i[create update]
  before_action :set_object, only: %i[create update]

  def index
    @assignment = @role.assignments.all
  end

  def show; end

  def create
    @assignment = @role.assignments.new
    @assignment.subject = @subject
    @assignment.object = @object

    respond_to do |format|
      if @assignment.save
        url = role_assignment_url @role, @assignment, format: :json
        format.json { render :show, status: :created, location: url }
      else
        format.json { render json: @assignment.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @assignment.subject = @subject if @subject
    @assignment.object = @object if @object
    respond_to do |format|
      if @assignment.save
        url = role_assignment_url @role, @assignment, format: :json
        format.json { render :show, status: :ok, location: url }
      else
        format.json { render json: @assignment.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @assignment.destroy
    respond_to do |format|
      format.json { head :no_content }
    end
  end

  private

  def set_assignment
    @assignment = @role.assignments.find(params[:id])
  end

  def set_role
    @role = Role.find(params[:role_id])
  end

  def set_object
    return unless assignment_params[:object_type] && assignment_params[:object_id]

    @object = assignment_params[:object_type].classify.constantize.find assignment_params[:object_id]
  rescue NameError
    render status: :unprocessable_entity, json: {
      object_type: [
        "#{assignment_params[:object_type]} is not valid"
      ]
    }
  rescue ActiveRecord::RecordNotFound
    render status: :unprocessable_entity, json: {
      object_id: [
        "#{assignment_params[:object_id]} is not valid"
      ]
    }
  end

  def set_subject
    return unless assignment_params[:subject_type] && assignment_params[:subject_id]

    @subject = assignment_params[:subject_type].classify.constantize.find assignment_params[:subject_id]
  rescue NameError
    render status: :unprocessable_entity, json: {
      subject_type: [
        "#{assignment_params[:subject_type]} is not valid"
      ]
    }
  rescue ActiveRecord::RecordNotFound
    render status: :unprocessable_entity, json: {
      subject_id: [
        "#{assignment_params[:subject_id]} is not valid"
      ]
    }
  end

  def assignment_params
    params.require(:role_assignment).permit(
      :subject_id,
      :subject_type,
      :object_id,
      :object_type
    )
  end
end
