# frozen_string_literal: true

class ApplicationController < ActionController::Base
  include Pundit

  before_action :make_admin, :dev_login
  before_action :configure_permitted_parameters, if: :devise_controller?

  def new_session_path(_scope)
    new_user_session_path
  end

  def make_admin
    if User.count.zero? && Rails.env.development?
      user = User.create!(
        email: 'admin@mail.com',
        username: 'admin',
        fullname: 'Admin Userson',
        time_zone: 'UTC',
        admin: true
      )
      sign_in user
    end
  end

  def dev_login
    unless Rails.env.production?
      if params['_login']
        begin
          user = User.find(params['_login'])
          sign_in user
        rescue ActiveRecord::RecordNotFound
          flash[:error] = "User with id #{params['_login']} not found"
        end
      end

      if params['_admin'] && current_user
        begin
          current_user.update(admin: params['_admin'])
        end
      end
    end
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:account_update, keys: %i[
                                        country company department fullname username email time_zone
                                        facebook twitter linkedin github gitlab description
                                      ])
  end

  def authenticate_admin!
    unless current_user.admin?
      error_msg = 'Insufficient privileges'
      if (params[:format] || 'html').to_sym == :json
        json = { error: error_msg }
        render json: json, status: :forbidden
      else
        redirect_to root_path, flash: { error: error_msg }
      end
    end
  end
end
