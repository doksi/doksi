# frozen_string_literal: true

class DocumentsController < ApplicationController
  before_action :set_document, only: %i[show edit update destroy export]
  before_action :authenticate_user!, except: %i[index show export]

  # GET /documents
  # GET /documents.json
  def index
    # /teams/1/documents
    if params[:team_id]
      @team = Team.find(params[:team_id])
      @documents = policy_scope(@team.documents).search(params[:search])

    # /users/1/documents
    elsif params[:user_id]
      @user = User.find(params[:user_id])
      @documents = policy_scope(@user.documents).search(params[:search])

    # all documents for my teams
    elsif params[:scope] == 'teams'
      authenticate_user!
      @documents = current_user.team_documents.search(params[:search])

    # all documents not owned by me or my teams
    elsif params[:scope] == 'public'
      authenticate_user!
      @documents = policy_scope(Document).where(
        'id NOT IN (?)',
        (current_user.documents.empty? ? [] : current_user.documents.ids) +
        (current_user.team_documents.empty? ? [] : current_user.team_documents.ids)
      ).search(params[:search])

    # all documents
    else
      @documents = policy_scope(Document).search(params[:search])
    end
  end

  # GET /documents/1
  # GET /documents/1.json
  def show
    authorize @document
    respond_to do |format|
      format.html { render }
      format.json { render }
    end
  end

  # GET /documents/new
  def new
    @document = Document.new
    if params[:owner]
      @document.owner =
        params[:owner][:type].classify.constantize.find(params[:owner][:id])
    end
  end

  # GET /documents/1/edit
  def edit
    authorize @document
  end

  # POST /documents
  # POST /documents.json
  def create
    @document = Document.new
    success = assign_params(@document, document_params)

    respond_to do |format|
      if success
        DocumentsChannel.create @document
        format.html { redirect_to edit_document_path(@document), notice: 'Document was successfully created.' }
        format.json { render :show, status: :created, location: @document }
      else
        format.html { render :new }
        format.json { render json: @document.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /documents/1
  # PATCH/PUT /documents/1.json
  def update
    authorize @document
    success = assign_params(@document, document_params)

    respond_to do |format|
      if success
        DocumentChannel.update @document
        DocumentsChannel.update @document
        format.html { redirect_to edit_document_path(@document), notice: 'Document was successfully updated.' }
        format.json { render :show, status: :ok, location: @document }
      else
        format.html { render :edit }
        format.json { render json: @document.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /documents/1
  # DELETE /documents/1.json
  def destroy
    authorize @document
    @document.destroy
    respond_to do |format|
      DocumentsChannel.destroy @document
      format.html { redirect_to documents_url, notice: 'Document was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def export
    @export_details = {
      document: @document,
      request: {
        format: params[:export_format] || :pdf,
        archive: params[:export_archive] || :zip
      },
      status: {
        step: 1,
        state: :in_progess
      }
    }

    run_export_job @export_details

    respond_to do |format|
      format.html do
        data = Base64.decode64(@export_details[:result][:data])
        filename = @export_details[:result][:filename]
        send_data data, filename: filename, type: @export_details[:request][:format], disposition: 'inline'
      end
      format.json { render json: @export_details }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_document
    @document = Document.find(params[:document_id] || params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def document_params
    params.require(:document).permit(
      :title,
      :copyright,
      :owner_id,
      :owner_type,
      :classification_id,
      :status_id,
      :search,
      :visibility,
      :dynamic,
      :data_url,
      :data_string,
      properties: %i[id value],
      blocks: %i[id type content alignment level source kind rows columns encrypted]
    )
  end

  def run_export_job(export_details)
    # request is probing a specific job
    if params[:job_id]
      export_details[:status][:id] = params[:job_id]
      update_export_status ActiveJob::Status.get(params[:job_id]), export_details

    # json requests are run asynchronous
    elsif params[:format] && params[:format].to_sym == :json
      job = ExportDocumentJob.perform_later(@document, export_details[:request], params[:export_password])
      export_details[:status][:id] = job.job_id

    # html requests are run synchronous
    else
      job_status = ExportDocumentJob.perform_now(@document, export_details[:request], params[:export_password])
      update_export_status job_status, export_details
    end
  end

  def update_export_status(job_status, export_details)
    if job_status.present?
      export_details[:status][:error] = job_status[:error] if job_status[:error]
      export_details[:status][:step] = job_status[:step]
      export_details[:status][:state] = job_status[:state]
      export_details[:result] = job_status[:result] if job_status[:result]
    else
      export_details[:status][:error] = 'Export job finished abruptly'
      export_details[:status][:step] = 3
      export_details[:status][:state] = :error
    end
  end

  def assign_params(document, params)
    document.git_user = current_user.fullname
    document.git_mail = current_user.email
    document.owner = current_user unless document.owner

    params[:status_id] = nil if params[:status_id].to_i <= 0
    params[:classification_id] = nil if params[:classification_id].to_i <= 0

    return false unless document.update(params.except(:blocks, :properties))
    return false unless document.blocks.refresh(params[:blocks])
    return false unless document.assigned_properties.assign(params[:properties])
    return false unless document.commit

    true
  end
end
