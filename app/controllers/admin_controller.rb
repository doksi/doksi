# frozen_string_literal: true

class AdminController < ApplicationController
  before_action :authenticate_user!, :authenticate_admin!

  def dashboard
    @users = User.all
    @documents = Document.all
    @teams = Team.all
    @versions = [
      { name: 'Doksi', number: Doksi::Application.version },
      { name: 'Rails', number: Rails.version },
      { name: 'Ruby', number: RUBY_VERSION }
    ]
  end

  def classifications
    @classifications = Classification.where(owner: nil)
  end

  def statuses
    @statuses = Status.where(owner: nil)
  end

  def properties
    @properties = Property.where(owner: nil)
  end
end
