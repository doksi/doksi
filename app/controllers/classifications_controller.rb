# frozen_string_literal: true

class ClassificationsController < ApplicationController
  before_action :set_classification, only: %i[show edit update destroy]
  before_action :authenticate_user!, except: %i[index show]

  # GET /classifications
  # GET /classifications.json
  def index
    @classifications = Classification.all
  end

  # GET /classifications/1
  # GET /classifications/1.json
  def show; end

  # POST /classifications
  # POST /classifications.json
  def create
    @classification = Classification.new(classification_params)

    respond_to do |format|
      if @classification.save
        TeamChannel.update(@classification.owner) if @classification.owner.is_a?(Team)
        format.html { redirect_to @classification, notice: 'Classification was successfully created.' }
        format.json { render :show, status: :created, location: @classification }
      else
        format.html { render :new }
        format.json { render json: @classification.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /classifications/1
  # PATCH/PUT /classifications/1.json
  def update
    respond_to do |format|
      if @classification.update(classification_params)
        TeamChannel.update(@classification.owner) if @classification.owner.is_a?(Team)
        format.html { redirect_to @classification, notice: 'Classification was successfully updated.' }
        format.json { render :show, status: :ok, location: @classification }
      else
        format.html { render :edit }
        format.json { render json: @classification.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /classifications/1
  # DELETE /classifications/1.json
  def destroy
    TeamChannel.update @classification.owner if @classification.owner.is_a?(Team)
    @classification.destroy
    respond_to do |format|
      format.html { redirect_to classifications_url, notice: 'Classification was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_classification
    @classification = Classification.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def classification_params
    params.require(:classification).permit(
      :name,
      :stamp,
      :label,
      :title,
      :description,
      :authority,
      :date_format,
      :color,
      :double_border,
      :owner_id,
      :owner_type
    )
  end
end
