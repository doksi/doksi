# frozen_string_literal: true

class PrivilegesController < ApplicationController
  before_action :authenticate_user!, :authenticate_admin!
  before_action :set_role
  before_action :set_privilege, only: %i[show edit update destroy]

  def index
    @privileges = @role.privileges.all
  end

  def show; end

  def create
    @privilege = @role.privileges.new(privilege_params)

    respond_to do |format|
      if @privilege.save
        format.json { render :show, status: :created, location: [@role, @privilege] }
      else
        format.json { render json: @privilege.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @privilege.update(privilege_params)
        format.json { render :show, status: :ok, location: [@role, @privilege] }
      else
        format.json { render json: @privilege.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @privilege.destroy
    respond_to do |format|
      format.json { head :no_content }
    end
  end

  private

  def set_privilege
    @privilege = @role.privileges.find(params[:id])
  end

  def set_role
    @role = Role.find(params[:role_id])
  end

  def privilege_params
    params.require(:privilege).permit(:verb, :resource)
  end
end
