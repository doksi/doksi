# frozen_string_literal: true

class PermissionsController < ApplicationController
  before_action :set_object
  before_action :set_permission, only: %i[show edit update destroy]
  before_action :authenticate_user!

  # GET /object/1/permissions
  # GET /object/1/permissions.json
  def index
    @permissions = @object.permissions
  end

  # GET /object/1/permissions/1
  # GET /object/1/permissions/1.json
  def show; end

  # GET /object/1/permissions/new
  def new
    @permission = @object.permissions.build
  end

  # POST /object/1/permissions
  # POST /object/1/permissions.json
  def create
    @permission = @object.permissions.build(permission_params)

    respond_to do |format|
      if @permission.save
        format.html { redirect_to [@permission.object, @permission], notice: 'Permission was successfully created.' }
        format.json { render :show, status: :created, location: [@permission.object, @permission] }
      else
        format.html { render :new }
        format.json { render json: @permission.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /object/1/permissions/1
  # PATCH/PUT /object/1/permissions/1.json
  def update
    respond_to do |format|
      if @permission.update(permission_params)
        format.html { redirect_to [@permission.object, @permission], notice: 'Permission was successfully updated.' }
        format.json { render :show, status: :ok, location: [@permission.object, @permission] }
      else
        format.html { render :edit }
        format.json { render json: @permission.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /object/1/permissions/1
  # DELETE /object/1/permissions/1.json
  def destroy
    @permission.destroy
    respond_to do |format|
      format.html { redirect_to @object, notice: 'Permission was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_object
    @object = if params[:document_id]
                Document.find(params[:document_id])
              else
                raise 'Unknown or missing object ID for permission'
              end
  end

  def set_permission
    @permission = @object.permissions.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def permission_params
    params.require(:permission).permit(
      :subject_id,
      :subject_type,
      :object_id,
      :object_type,
      :privilege,
      :mode
    )
  end
end
