# frozen_string_literal: true

json.extract! team, :id, :name, :description, :available_roles, :created_at, :updated_at

unless defined?(shallow) && shallow
  json.extract! team, :classifications, :statuses, :properties

  json.members team.users do |user|
    json.extract! user, :id, :email, :fullname
    json.role RoleAssignment.where(subject: user, object: team).first || {}
    json.show_url user_url(user)
  end

  if request.env['warden']
    json.documents policy_scope(team.documents) do |document|
      json.partial! 'documents/document', document: document
    end
  end
end

json.url team_url(team, format: :json)

json.show_url team_url(team) if allowed_to? :show, team
json.edit_url edit_team_url(team) if allowed_to? :edit, team
json.destroy_url team_url(team, format: :json) if allowed_to? :destroy, team
