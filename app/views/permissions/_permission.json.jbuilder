# frozen_string_literal: true

json.extract! permission, :id, :subject, :object, :privilege, :mode, :created_at, :updated_at

if permission.object.is_a? Document
  json.url document_permission_url(permission.object, permission, format: :json)
  json.show_url document_permission_url(permission.object, permission)
  json.edit_url edit_document_permission_url(permission.object, permission)
end
