# frozen_string_literal: true

json.partial! 'permissions/permissions', permissions: @permissions
