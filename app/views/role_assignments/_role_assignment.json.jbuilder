# frozen_string_literal: true

json.extract! role_assignment, :id, :subject, :object, :created_at, :updated_at

json.url role_assignment_url(role_assignment.role, role_assignment, format: :json)
