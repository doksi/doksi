# frozen_string_literal: true

json.array! @assignments, partial: 'role_assignments/role_assignment', as: :role_assignment
