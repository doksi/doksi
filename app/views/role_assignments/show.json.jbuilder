# frozen_string_literal: true

json.partial! 'role_assignments/role_assignment', role_assignment: @assignment
