# frozen_string_literal: true

json.merge! JSON.parse(yield)
