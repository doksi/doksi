# frozen_string_literal: true

json.partial! 'devise/registrations/user', user: @user
