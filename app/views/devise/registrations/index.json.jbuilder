# frozen_string_literal: true

if @query
  json.query @query
  json.results do
    json.array! @users, partial: 'devise/registrations/user', as: :user
  end

else
  json.array! @users, partial: 'devise/registrations/user', as: :user
end
