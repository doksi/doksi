# frozen_string_literal: true

json.extract! user, :id, :email, :description, :fullname, :username,
              :facebook, :twitter, :linkedin, :github, :gitlab,
              :time_zone, :country, :department, :company,
              :created_at, :updated_at

unless defined?(shallow) && shallow
  json.extract! user, :classifications, :statuses, :properties

  if request.env['warden']
    json.documents policy_scope(user.documents) do |document|
      json.partial! 'documents/document', document: document
    end
  end

  json.teams user.teams do |team|
    json.partial! 'teams/team', team: team
  end
end

json.admin user.admin?
json.url user_url(user, format: :json)
json.show_url user_url(user)
