# frozen_string_literal: true

json.extract! role, :id, :name, :created_at, :updated_at
json.privileges role.privileges, partial: 'privileges/privilege', as: :privilege
json.url role_url(role, format: :json)
