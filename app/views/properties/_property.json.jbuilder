# frozen_string_literal: true

json.extract! property, :id, :name, :description, :kind, :default, :created_at, :updated_at
json.options JSON.parse(property.options) if property.options

json.url property_url(property, format: :json)
json.show_url property_url(property)
json.edit_url edit_property_url(property)
