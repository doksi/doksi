# frozen_string_literal: true

json.partial! 'properties/properties', properties: @properties
