# frozen_string_literal: true

json.array! @properties, :id, :name, :default, :options, :kind, :description
