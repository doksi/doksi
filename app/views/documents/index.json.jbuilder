# frozen_string_literal: true

json.partial! 'documents/documents', documents: @documents
