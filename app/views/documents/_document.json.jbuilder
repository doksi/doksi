# frozen_string_literal: true

json.extract! document, :id, :title, :copyright, :dynamic, :data_url, :data_string,
              :created_at, :updated_at

json.visibility document.visibility || 'private'

json.owner do
  json.kind document.owner.class.name
  if document.owner.is_a?(User)
    json.partial! 'devise/registrations/user', user: document.owner, shallow: true
  elsif document.owner.is_a?(Team)
    json.partial! 'teams/team', team: document.owner, shallow: true
  else
    json.id document.owner.id
  end
end

json.statistics do
  json.blocks do
    json.total number_with_delimiter(document.blocks.count)
    %w[paragraphs headings lists code images quotes tables].each do |t|
      json.set! t, number_with_delimiter(document.blocks.where(type: t.classify).count)
    end
  end
  characters = 0
  words = 0
  sentences = 0
  document.blocks.each do |block|
    next if block.encrypted?

    if block.is_a? Table
      begin
        JSON.parse(block.content).flatten.each do |cell|
          characters += cell['content'].size
          words += cell['content'].split.size
          sentences += cell['content'].scan(/[^.!?]+[.!?]/).map(&:strip).count
        end
      rescue StandardError
        nil
      end
    elsif !block.is_a? Image
      characters += block.content.size
      words += block.content.split.size
      sentences += block.content.scan(/[^.!?]+[.!?]/).map(&:strip).count
    end
  end
  json.characters number_with_delimiter(characters)
  json.words number_with_delimiter(words)
  json.sentences number_with_delimiter(sentences)
end

json.classification document.classification if document.classification
json.status document.status if document.status

json.blocks document.blocks.order(:order) do |block|
  json.partial! 'blocks/block', block: block
  json.content block.rendered(document.data) if document.dynamic? && !params[:skip_jinja]
end

json.permissions document.permissions do |permission|
  json.partial! 'permissions/permission', permission: permission
end

json.classifications document.classifications do |classification|
  json.partial! 'classifications/classification', classification: classification
end

json.statuses document.statuses do |status|
  json.partial! 'statuses/status', status: status
end

json.properties document.available_properties do |prop|
  json.partial! 'properties/property', property: prop
  assigned = document.assigned_properties.where(property: prop)
  if assigned.count.positive?
    json.value assigned.first.value
  else
    json.value prop.default
  end
end

json.url document_url(document, format: :json)

json.show_url document_url(document) if allowed_to? :show, document
json.edit_url edit_document_url(document) if allowed_to? :edit, document
json.destroy_url document_url(document, format: :json) if allowed_to? :destroy, document
json.export_url document_export_url(document, format: :json) if allowed_to? :show, document
