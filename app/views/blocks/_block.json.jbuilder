# frozen_string_literal: true

json.extract! block, :id, :type, :kind, :order, :content, :alignment, :created_at, :updated_at

case block
when Heading
  json.level block.level
when Quote
  json.source block.source
when Code
  json.html block.to_html
when Table
  json.rows block.rows
  json.columns block.columns
when Raw
  json.html block.to_html
end

json.encrypted block.encrypted?
json.url block_url(block, format: :json)
