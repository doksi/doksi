# frozen_string_literal: true

json.extract! classification, :id, :name, :stamp, :title, :description, :authority, :color, :double_border,
              :date_format, :label, :created_at, :updated_at, :owner

json.url classification_url(classification, format: :json)
json.show_url classification_url(classification)
json.edit_url edit_classification_url(classification)
