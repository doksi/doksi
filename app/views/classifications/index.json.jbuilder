# frozen_string_literal: true

json.partial! 'classifications/classifications', classifications: @classifications
