# frozen_string_literal: true

json.array! @privileges, partial: 'privileges/privilege', as: :privilege
