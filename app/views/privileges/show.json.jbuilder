# frozen_string_literal: true

json.partial! 'privileges/privilege', privilege: @privilege
