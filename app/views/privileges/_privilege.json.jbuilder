# frozen_string_literal: true

json.extract! privilege, :id, :verb, :resource, :created_at, :updated_at

json.url role_privilege_url(privilege.role, privilege, format: :json)
