# frozen_string_literal: true

json.partial! 'statuses/statuses', statuses: @statuses
