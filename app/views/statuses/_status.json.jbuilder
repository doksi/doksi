# frozen_string_literal: true

json.extract! status, :id, :name, :watermark, :created_at, :updated_at

json.url status_url(status, format: :json)
json.show_url status_url(status)
json.edit_url edit_status_url(status)
