# frozen_string_literal: true

class DocumentsChannel < ApplicationCable::Channel
  include ChannelHelper

  def subscribed
    stream_from 'documents_channel'
  end

  def unsubscribed; end

  def self.update(document)
    broadcast document: render(document), action: :update
  end

  def self.create(document)
    broadcast document: render(document), action: :create
  end

  def self.destroy(document)
    broadcast document: render(document), action: :destroy
  end

  def self.render(resource, template = 'documents/show')
    json = ApplicationController.render template: template,
                                        assigns: { document: resource }
    JSON.parse json
  end

  def self.broadcast(params)
    ActionCable.server.broadcast 'documents_channel', params
  end
end
