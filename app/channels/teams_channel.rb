# frozen_string_literal: true

class TeamsChannel < ApplicationCable::Channel
  include ChannelHelper

  def subscribed
    stream_from 'teams_channel'
  end

  def unsubscribed; end

  def self.update(team)
    broadcast team: render(team), action: :update
  end

  def self.create(team)
    broadcast team: render(team), action: :create
  end

  def self.destroy(team)
    broadcast team: render(team), action: :destroy
  end

  def self.render(resource, template = 'teams/show')
    json = ApplicationController.render template: template,
                                        assigns: { team: resource }
    JSON.parse json
  end

  def self.broadcast(params)
    ActionCable.server.broadcast 'teams_channel', params
  end
end
