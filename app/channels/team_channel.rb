# frozen_string_literal: true

class TeamChannel < ApplicationCable::Channel
  def subscribed
    team = Team.find(params[:id])
    stream_for team
  end

  def unsubscribed; end

  def self.update(resource)
    broadcast_to resource, team: render(resource)
  end

  def self.render(resource, template = 'teams/show')
    json = ApplicationController.render template: template,
                                        assigns: { team: resource }
    JSON.parse json
  end
end
