# frozen_string_literal: true

class DocumentChannel < ApplicationCable::Channel
  def subscribed
    document = Document.find(params[:id])
    stream_for document
  end

  def unsubscribed; end

  def self.update(resource)
    broadcast_to resource, document: render(resource)
  end

  def self.render(resource, template = 'documents/show')
    json = ApplicationController.render template: template,
                                        assigns: { document: resource }
    JSON.parse json
  end
end
