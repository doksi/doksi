# frozen_string_literal: true

require 'minitar'
require 'xz'
require 'tmpdir'

module Archive
  class Txz
    # Initialize with the directory to zip and the location of the output archive.
    def initialize(input_dir, output_file)
      @input_dir = input_dir
      @output_file = output_file
    end

    # Pack the input directory.
    def write
      Dir.mktmpdir do |tmp|
        name = File.basename(@output_file, '.*')
        tar_file = File.join(tmp, "#{name}.tar")
        tar_dir = File.join(tmp, name)
        FileUtils.cp_r(@input_dir, tar_dir)
        Dir.chdir tmp do
          Minitar.pack(name, File.open(tar_file, 'wb'))
        end
        XZ.compress_file(tar_file, @output_file)
      end
    end
  end
end
