# frozen_string_literal: true

require 'minitar'
require 'zlib'
require 'tmpdir'

module Archive
  class Tgz
    # Initialize with the directory to zip and the location of the output archive.
    def initialize(input_dir, output_file)
      @input_dir = input_dir
      @output_file = output_file
    end

    # Pack the input directory.
    def write
      Dir.mktmpdir do |tmp|
        name = File.basename(@output_file, '.*')
        tar_dir = File.join(tmp, name)
        tgz_file = Zlib::GzipWriter.new(File.open(@output_file, 'wb'))
        FileUtils.cp_r(@input_dir, tar_dir)
        Dir.chdir tmp do
          Minitar.pack(name, tgz_file)
        end
      end
    end
  end
end
