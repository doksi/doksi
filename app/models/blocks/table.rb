# frozen_string_literal: true

class Table < Block
  validates :rows, presence: true, numericality: { only_integer: true, greater_than: 0, less_than: 100 }
  validates :columns, presence: true, numericality: { only_integer: true, greater_than: 0, less_than: 10 }

  def to_rst
    rst = ['.. list-table:: Table', ':header-rows: 1', '']

    parsed_content.each do |row|
      cells = row.map do |cell|
        "- #{cell['content']}"
      end

      rst << "* #{cells.join("\n      ")}"
    end

    rst.join("\n    ") + "\n"
  end

  def parsed_content
    JSON.parse(content)
  rescue StandardError
    [
      [{ 'content' => 'Encrypted' }, { 'content' => 'Encrypted' }],
      [{ 'content' => 'Encrypted' }, { 'content' => 'Encrypted' }]
    ]
  end

  def self.from_rst(rst_file)
    ret = {}

    element = read_rst(rst_file).root.children.first
    raise "Unknown image element: #{element.name}" unless element.name == 'table'

    ret[:columns] = element.at('tgroup').attributes['cols'].value.to_i
    ret[:rows] = element.search('row').length
    ret[:content] = element.search('row').map do |row|
      row.search('entry').map do |column|
        { content: column.content }
      end
    end.to_json

    ret
  end
end
