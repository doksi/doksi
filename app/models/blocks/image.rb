# frozen_string_literal: true

class Image < Block
  def to_rst
    ".. image:: #{content}"
  end

  def self.from_rst(rst_file)
    ret = {}

    element = read_rst(rst_file).root.children.first
    raise "Unknown image element: #{element.name}" unless element.name == 'image'

    ret[:content] = element.attributes['uri'].value

    ret
  end
end
