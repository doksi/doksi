# frozen_string_literal: true

class Paragraph < Block
  def to_rst
    if kind.present? && kind.downcase.to_s != 'normal'
      ".. #{kind}::\n#{content.indent(4)}\n"
    else
      content
    end
  end

  def self.from_rst(rst_file)
    ret = {}
    first_element = read_rst(rst_file).root.children.first
    allowed_kinds = %w[warning tip note]

    if first_element.name == 'paragraph'
      ret[:content] = first_element.content
      ret[:kind] = 'normal'
    elsif first_element.name.in? allowed_kinds
      paragraph_element = first_element.children.first
      raise "Unknown paragraph element: #{paragraph_element.name}" unless paragraph_element.name == 'paragraph'

      ret[:content] = paragraph_element.content
      ret[:kind] = first_element.name.downcase
    else
      raise "Unknown paragraph element: #{first_element.name}"
    end
    ret
  end
end
