# frozen_string_literal: true

require 'nokogiri'

class List < Block
  def to_rst
    bullet = '*'
    bullet = '#.' if kind == 'ordered'
    list = parsed_content.css('li').map do |element|
      [bullet, element.text].join(' ')
    end.join("\n")

    "\n#{list}\n\n"
  end

  def parsed_content
    if encrypted?
      Nokogiri::HTML.parse('<li>Encrypted</li>' * 3)
    else
      Nokogiri::HTML.parse(content)
    end
  end

  def self.from_rst(rst_file)
    ret = {}
    list_element = read_rst(rst_file).root.children.first

    if list_element.name == 'bullet_list'
      ret[:kind] = :unordered
      ret[:content] = list_element.children.map do |item|
        "<li>#{item.content}</li>"
      end.join('')

    elsif list_element.name == 'enumerated_list'
      ret[:kind] = :ordered
      ret[:content] = list_element.children.map do |item|
        "<li>#{item.content}</li>"
      end.join('')

    else
      raise "Unknown list element: #{list_element.name}"
    end

    ret
  end

  private

  def set_default_values
    self.kind ||= 'unordered'
  end
end
