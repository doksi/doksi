# frozen_string_literal: true

class Raw < Block
  def to_rst
    content
  end

  def self.from_rst(rst_file)
    ret = {}
    ret[:content] = File.read(rst_file) if File.exist?(rst_file)
    ret
  end
end
