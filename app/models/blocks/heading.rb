# frozen_string_literal: true

class Heading < Block
  def to_rst
    underline = Heading.level_to_underline(level) * content.length
    [content, underline, ''].join("\n")
  end

  def self.from_rst(rst_file)
    ret = {}

    element = read_rst(rst_file).root.children.first
    raise "Unknown heading element: #{element.name}" unless element.name == 'title'

    ret[:content] = element.content

    rst = File.read(rst_file).split("\n").map(&:strip)

    # find heading
    index_of_heading = rst.index(ret[:content])
    raise 'Could not find heading in ReST' unless index_of_heading

    # find underline
    underline = rst[index_of_heading + 1]
    raise 'Could not find underline in ReST' unless underline

    # determine level
    ret[:level] = Heading.underline_to_level(underline.first)

    ret
  end

  def self.underline_characters
    %w[= - + ^ ~]
  end

  def self.level_to_underline(level)
    underline_characters[level - 1]
  end

  def self.underline_to_level(character)
    underline_characters.index(character) + 1
  end
end
