# frozen_string_literal: true

class Code < Block
  def to_rst
    ".. code-block:: #{kind}\n\n#{content.indent(4)}"
  end

  def to_html(options = {})
    Code.to_html(content, kind, options)
  end

  def self.to_html(content, language, options = {})
    CodeRay.scan(content, language.to_sym).div(options)
  end

  def self.from_rst(rst_file)
    ret = {}

    element = read_rst(rst_file).root.children.first
    raise "Unknown image element: #{element.name}" unless element.name == 'literal_block'

    ret[:kind] = classes_to_language(element.attributes['classes'].value)
    ret[:content] = element.content

    ret
  end

  def self.classes_to_language(classes)
    case classes.split(' ').sort.join(' ').downcase
    when 'code ruby'
      :ruby
    else
      :text
    end
  end
end
