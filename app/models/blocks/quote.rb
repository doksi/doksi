# frozen_string_literal: true

class Quote < Block
  def to_rst
    rst = content.indent(4)
    rst = "#{rst}\n\n    -- #{source}" if source
    rst
  end

  def self.from_rst(rst_file)
    ret = {}

    element = read_rst(rst_file).root.children.first
    raise "Unknown quote element: #{element.name}" unless element.name == 'block_quote'

    content_element = element.at('paragraph')
    source_element = element.at('attribution')

    ret[:content] = content_element.content if content_element
    ret[:source] = source_element.content if source_element

    ret
  end
end
