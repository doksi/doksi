# frozen_string_literal: true

class Document < ApplicationRecord
  include Rst

  belongs_to :owner, polymorphic: true
  belongs_to :classification, optional: true
  belongs_to :status, optional: true
  has_many :blocks, dependent: :destroy
  has_many :permissions, as: :object, dependent: :destroy
  has_many :assigned_properties, class_name: 'DocumentsProperty', foreign_key: :document_id
  has_many :properties, through: :assigned_properties

  validates :title, length: { in: 2..50 }
  validates :copyright, length: { maximum: 50 }
  validates :owner, presence: true
  validates :data_url, url: true, allow_nil: true, allow_blank: true

  enum visibility: { public: 0, private: 1 }, _prefix: true

  attr_accessor :git_user, :git_mail

  def self.search(search)
    if search
      where('title LIKE ?', "%#{search}%")
    else
      all
    end
  end

  def export_folder
    tmp_path = File.join('tmp', 'export')
    File.join(tmp_path, id.to_s)
  end

  def classifications
    global_classifications = Classification.where(owner: nil)
    if owner
      global_classifications + owner.classifications
    else
      global_classifications
    end
  end

  def statuses
    global_statuses = Status.where(owner: nil)
    if owner
      global_statuses + owner.statuses
    else
      global_statuses
    end
  end

  def available_properties
    global = Property.where(owner: nil)
    if owner
      global + owner.properties
    else
      global
    end
  end

  def export(format = :html, archive = :zip, enc_params = {})
    export_prepare format, enc_params
    export_build format
    export_finalize format, archive
  end

  def export_prepare(format = :html, enc_params = {})
    folder = File.join(export_folder, format.to_s)
    block_folder = File.join(folder, 'source', 'blocks')
    json_data = dynamic? ? data : nil

    FileUtils.rm_rf folder
    FileUtils.mkdir_p folder
    copy_template folder
    blocks.each { |b| b.save_rst(block_folder, enc_params, json_data) }
    save_rst File.join(folder, 'source')
  end

  def export_build(format = :html)
    folder = File.join(export_folder, format.to_s)
    target = case format.to_sym
             when :html
               'html'
             when :pdf
               'latexpdf'
             when :epub
               'epub'
             else
               'html'
             end
    Rails.logger.info "run_sphinx '#{folder}', '#{target}'"
    run_sphinx folder, target
    exported_path(format)
  end

  def export_finalize(format = :html, archive = :zip)
    path = exported_path(format)
    dest = File.join(export_folder, format.to_s, 'build')
    return path unless format.to_sym == :html

    archive_export(path, dest, archive)
  end

  def data
    json = {}
    if dynamic?
      json = if data_string.present?
               data_string
             else
               Net::HTTP.get(URI(data_url))
             end
      begin
        JSON.parse(json)
      rescue StandardError
        nil
      end
    end
    json
  end

  private

  def exported_path(format)
    case format.to_sym
    when :html
      File.join(export_folder, format.to_s, 'build', 'html')
    when :pdf
      File.join(export_folder, format.to_s, 'build', 'latex', "#{title.downcase.gsub(' ', '')}.pdf")
    when :epub
      File.join(export_folder, format.to_s, 'build', 'epub', "#{title.gsub(' ', '')}.epub")
    else
      raise "Unknown format: #{format}"
    end
  end

  def sphinx_container
    'registry.gitlab.com/doksi/sphinx:latest'
  end

  # Run Sphinx inside container
  def run_sphinx(folder, target)
    command = [Settings.export.container_mgr, 'run', '--rm', '-it', '-v']
    command << "$PWD/#{folder}:/docs"
    command << sphinx_container
    command << 'make'
    command << target
    Rails.logger.info "Running export container: #{command.join(' ')}"
    stdout, stderr, status = Open3.capture3(command.join(' '))
    raise stderr.to_s unless status.success?

    stdout
  end

  # Create an archive from a folder
  def archive_export(path, folder, format)
    filename = title.downcase.gsub(' ', '_')

    case format.to_sym
    when :zip
      filename = "#{filename}.zip"
      archive_class = Archive::Zip
    when :tgz
      filename = "#{filename}.tgz"
      archive_class = Archive::Tgz
    when :txz
      filename = "#{filename}.txz"
      archive_class = Archive::Txz
    else
      raise "Unknown archive format: #{format}"
    end

    dest = File.join(folder, filename)
    archive = archive_class.new(path, dest)
    archive.write

    dest
  end
end
