# frozen_string_literal: true

class Status < ApplicationRecord
  belongs_to :owner, polymorphic: true, optional: true
  validates :name, presence: true, length: { in: 2..30 }
end
