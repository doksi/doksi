# frozen_string_literal: true

class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :omniauthable

  has_and_belongs_to_many :teams
  has_many :documents, as: :owner, dependent: :destroy
  has_many :team_documents, through: :teams, class_name: 'Document', source: :documents
  has_many :classifications, as: :owner, dependent: :destroy
  has_many :statuses, as: :owner, dependent: :destroy
  has_many :properties, as: :owner, dependent: :destroy

  validates :time_zone, presence: true, time_zone: true
  validates :username, presence: true, length: { in: 2..50 }, uniqueness: true
  validates :fullname, length: { maximum: 50 }
  validates :description, length: { maximum: 250 }

  def self.from_omniauth(auth)
    where(email: auth.info.email).first_or_create do |user|
      user.time_zone ||= 'UTC'
      user.fullname = auth.info.name
      user.username = user.email[/^[^@]*/]

      # if username is not unique, add random string
      user.username = "#{user.email[/^[^@]*/]}_#{rand(1000..1_000_000)}" while unscoped.find_by(username: user.username)
    end
  end

  def update_with_password(params)
    update(params)
  end
end
