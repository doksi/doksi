# frozen_string_literal: true

class Block < ApplicationRecord
  belongs_to :document
  before_save :set_default_values, :sanitize
  after_save :save_rst, unless: :encrypted?
  after_find :load_rst, unless: :encrypted?
  before_destroy :remove_rst

  def self.refresh(params)
    return true if params.blank?

    # add from params
    result = find_or_build_list(params)

    # remove all not in params
    all.each do |block|
      matching = params.select { |b| b[:id] == block.id }
      block.destroy! if matching.empty?
    end

    # return whether everything succeeded
    result.all?
  end

  def self.find_or_build_list(params)
    params.map.with_index do |b, i|
      obj = find_or_build(b, i)
      r = obj.save
      b[:id] = obj.id if r
      r
    end
  end

  def self.find_or_build(params, index)
    id = params.extract!(:id)[:id]
    obj = find_by(id: id) || new
    obj.assign_attributes params
    obj = obj.becomes(obj.type.constantize) # convert to correct class
    obj.order = index
    obj
  end

  def to_rst
    raise 'You need to implement a custom to_rst for this block type'
  end

  def rst_file(folder = nil)
    folder ||= rst_folder
    File.join(folder, "#{id}.rst")
  end

  def load_rst
    result = self.class.from_rst(rst_file)
    %i[content rows columns kind level].each do |key|
      send("=#{key}", result[key]) if result[key] && respond_to?("=#{key}")
    end
  rescue StandardError
    nil
  end

  def rendered(data = '{}')
    hash = Digest::MD5.hexdigest(content + data)
    Rails.cache.fetch("block/jinja/#{hash}", expires_in: 24.hours) do
      self.class.render(content, data)
    end
  end

  def self.render(text, data = '{}')
    stdout, stderr, status = Open3.capture3(
      'bin/parse-jinja',
      '--base64',
      Base64.encode64(text).strip,
      Base64.encode64(data).strip
    )
    raise stderr.to_s unless status.success?

    stdout.strip
  rescue StandardError
    text
  end

  def to_html
    self.class.to_html(content)
  end

  def self.to_html(content)
    hash = Digest::MD5.hexdigest(content)
    Rails.cache.fetch("block/html/#{hash}", expires_in: 24.hours) do
      stdout, stderr, status = Open3.capture3('bin/parse-rst', 'html', Base64.encode64(content).strip)
      raise stderr.to_s unless status.success?

      stdout
    end
  end

  def self.from_rst
    raise 'You need to implement a custom from_rst for this block type'
  end

  def self.read_rst(rst_file)
    Nokogiri::XML(open_rst(rst_file))
  end

  def self.open_rst(rst_file)
    hash = Digest::MD5.hexdigest(File.read(rst_file))
    Rails.cache.fetch("block/rst/#{rst_file}/#{hash}", expires_in: 24.hours) do
      if File.exist? rst_file
        stdout, stderr, status = Open3.capture3('bin/parse-rst', 'xml', rst_file)
        raise stderr.to_s unless status.success?

        stdout
      else
        ''
      end
    end
  end

  def save_rst(folder = nil, passphrase = nil, data = nil)
    unless passphrase.blank?
      self.content = decrypt(passphrase)
      self.encrypted = false
    end
    folder ||= rst_folder
    text = to_rst
    text = self.class.render(text, data) if data.present?
    FileUtils.mkdir_p folder
    File.open(rst_file(folder), 'w') do |file|
      file.write(text)
    end
  end

  def decrypt(passphrase)
    their_hmac = content[0..63]
    encrypted = content[64..]
    digest = OpenSSL::Digest.new('sha256')
    cipher = OpenSSL::Cipher.new('AES-256-CBC')
    our_hmac = OpenSSL::HMAC.hexdigest(digest, digest.digest(passphrase), encrypted)

    raise 'Incorrect decryption key' if their_hmac != our_hmac

    salt = Base64.decode64(encrypted)[8, 8]
    ciphertext = Base64.decode64(encrypted)[16..]
    key, iv = evp_kdf(passphrase, salt)

    cipher.decrypt
    cipher.key = key
    cipher.iv = iv
    plain = cipher.update(ciphertext) + cipher.final
    plain.encode 'utf-8'
  end

  private

  def set_default_values; end

  def remove_rst
    File.delete(rst_file) if File.exist?(rst_file)
  end

  def rst_folder
    File.join(document.rst_folder, 'blocks')
  end

  def sanitize
    self.content = ApplicationController.helpers.sanitize(
      content,
      tags: %w[div s b i strong em p br hr ul ol li],
      attributes: %w[href name]
    )
  end

  # Generate encryption key and IV from passphrase and salt
  #
  # Specialized implementation of https://www.openssl.org/docs/crypto/EVP_BytesToKey.html
  def evp_kdf(passphrase, salt)
    d1 = OpenSSL::Digest::MD5.new(passphrase + salt).digest
    d2 = OpenSSL::Digest::MD5.new(d1 + passphrase + salt).digest
    d3 = OpenSSL::Digest::MD5.new(d2 + passphrase + salt).digest

    [(d1 + d2), d3]
  end
end
