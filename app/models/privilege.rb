# frozen_string_literal: true

class Privilege < ApplicationRecord
  belongs_to :role
  validates :role, presence: true
  validates :resource, presence: true, inclusion: { in: %w[Document Team Global],
                                                    message: '%{value} is not valid' }
  validates :verb, presence: true, inclusion: { in: %w[show edit destroy],
                                                message: '%{value} is not valid' }
end
