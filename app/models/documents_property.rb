# frozen_string_literal: true

class DocumentsProperty < ActiveRecord::Base
  belongs_to :document
  belongs_to :property

  def self.assign(props)
    return true unless props

    props.map do |prop|
      assigned_prop = find_by(property: Property.find(prop[:id]))
      assigned_prop ||= create(property: Property.find(prop[:id]))
      assigned_prop.value = prop[:value]
      assigned_prop.save
    end.all?
  end
end
