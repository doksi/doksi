# frozen_string_literal: true

class Role < ApplicationRecord
  has_many :privileges, dependent: :destroy
  has_many :assignments, class_name: 'RoleAssignment', dependent: :destroy
  validates :name, presence: true, length: { in: 2..30 }, uniqueness: true
end
