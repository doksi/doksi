# frozen_string_literal: true

class Permission < ApplicationRecord
  belongs_to :subject, polymorphic: true
  belongs_to :object, polymorphic: true
  enum privilege: { read: 0, write: 1, admin: 2 }
  enum mode: { allow: 0, deny: 1 }

  def self.allows?(subject, action, object)
    actions = action_or_higher(action)
    where(subject: subject, object: object, privilege: actions, mode: :allow).any?
  end

  def self.denies?(subject, action, object)
    actions = action_or_lower(action)
    where(subject: subject, object: object, privilege: actions, mode: :deny).any?
  end

  # returns all actions similar or lower
  def self.action_or_lower(action)
    case action
    when :read
      [:read]
    when :write
      %i[read write]
    when :admin
      %i[read write admin]
    end
  end

  # returns all actions similar or higher
  def self.action_or_higher(action)
    case action
    when :read
      %i[read write admin]
    when :write
      %i[write admin]
    when :admin
      [:admin]
    end
  end
end
