# frozen_string_literal: true

module Rst
  extend ActiveSupport::Concern

  included do
    after_save :save_rst
    before_destroy :remove_rst
  end

  def rst_folder
    File.join(Settings.storage.documents, id.to_s)
  end

  def save_rst(folder = nil)
    folder ||= rst_folder
    FileUtils.mkdir_p folder
    create_index_rst(folder)
    create_main_rst(folder)
  end

  def remove_rst
    FileUtils.rm_rf(rst_folder) if File.exist?(rst_folder)
  end

  def create_index_rst(folder)
    path = File.join(folder, 'index.rst')
    content = <<~RST
      .. this is a comment i think

      #{title}
      #{'=' * title.length}

      .. toctree::
        :maxdepth: 2
        :caption: Contents:

        main

      .. only:: html

        :ref:`genindex`

    RST

    File.open(path, 'w') do |file|
      file.write(content)
    end
  end

  def create_main_rst(folder)
    path = File.join(folder, 'main.rst')
    content = blocks.order(:order).map do |block|
      ".. include:: blocks/#{block.id}.rst"
    end.join("\n")

    File.open(path, 'w') do |file|
      file.write(content)
    end
  end

  # Copy template files
  def copy_template(dst_folder)
    src_folder = File.join('app', 'assets', 'templates', 'sphinx')
    src_files = Dir[File.join(src_folder, '**', '*')]
    template_exts = ['.py', '.html', '.sty', '.svg']

    src_files.each do |file|
      use_erb = template_exts.include? File.extname(file)
      copy_file(file, src_folder, dst_folder, use_erb) if File.file?(file)
    end
  end

  # Copy a single file from one folder to another
  def copy_file(file, src_folder, dst_folder, use_erb = false)
    relative = Pathname(file).relative_path_from(Pathname(src_folder))
    dst = File.join(dst_folder, relative)
    dst_folder = File.dirname(dst)

    FileUtils.mkdir_p(dst_folder) unless File.directory?(dst_folder)

    if use_erb
      content = File.open(file).read
      File.open(dst, 'w') do |f|
        f.write ERB.new(content).result(binding)
      end
    else
      FileUtils.cp(file, dst)
    end
  end

  def git_init(_folder)
    git
  end

  def commit
    git = Git.init(rst_folder)
    git.config('user.name', git_user)
    git.config('user.email', git_mail)

    # make an empty commit if status fails (empty repo)
    begin
      git.status
    rescue Git::GitExecuteError
      git.commit('Initialize Doksi document', allow_empty: true)
    end

    git.add('index.rst')
    git.add('main.rst')
    git.add('blocks') if File.exist?(File.join(rst_folder, 'blocks'))

    git.commit('Save document with Doksi') if git.status.added.length.positive?
    true
  end
end
