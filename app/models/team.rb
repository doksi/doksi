# frozen_string_literal: true

class Team < ApplicationRecord
  has_and_belongs_to_many :users
  has_many :documents, as: :owner, dependent: :destroy
  has_many :classifications, as: :owner, dependent: :destroy
  has_many :statuses, as: :owner, dependent: :destroy
  has_many :properties, as: :owner, dependent: :destroy

  validates :users, length: { minimum: 1 }
  validates :name, length: { in: 2..30 }
  validates :description, length: { maximum: 100 }

  scope :without_member, ->(user) { where('id NOT IN (?)', user.teams.empty? ? '' : user.teams.ids) }

  def available_roles
    Role.select do |role|
      role.privileges.where(resource: self.class.name).any?
    end
  end
end
