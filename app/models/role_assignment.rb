# frozen_string_literal: true

class RoleAssignment < ApplicationRecord
  belongs_to :subject, polymorphic: true
  belongs_to :object, polymorphic: true
  belongs_to :role

  validates :subject, presence: true
  validates :object, presence: true
  validates :role, presence: true
end
