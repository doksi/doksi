import Vue from 'vue'

export const store = Vue.observable({
  copyright: 'My copyright line'
})

export const mutations = {
  setCopyright (copyright) {
    store.copyright = copyright
  }
}
