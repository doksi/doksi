import Vue from 'vue/dist/vue.esm.js'
import TurbolinksAdapter from 'vue-turbolinks'
import Vue2Filters from 'vue2-filters'
import TextareaAutosize from 'vue-textarea-autosize'
import VueLocalStorage from 'vue-localstorage'

const requireComponent = require.context(
  // load all components from this folder
  '../components',

  // whether or not to look in subfolders
  true,

  // load all files matching this pattern
  /\.(js|vue)$/i
)

requireComponent.keys().forEach(filename => {
  // Get component config
  const componentConfig = requireComponent(filename)
  const componentName = filename.split('/').join('').match(/\w+/)[0]

  Vue.component(
    componentName,
    // Look for the component options on `.default`, which will
    // exist if the component was exported with `export default`,
    // otherwise fall back to module's root.
    componentConfig.default || componentConfig
  )
})

Vue.use(TurbolinksAdapter)
Vue.use(Vue2Filters)
Vue.use(TextareaAutosize)
Vue.use(VueLocalStorage)

Vue.config.productionTip = false

document.addEventListener('turbolinks:load', () => {
  if (document.getElementById('doksi') != null) {
    // eslint-disable-next-line no-unused-vars
    const app = new Vue({
      el: '#doksi',

      mixins: [Vue2Filters.mixin],

      // global store
      data () {
        return {
          document: {},
          documents: [],
          user: {},
          users: [],
          team: {},
          teams: []
        }
      }
    })

    global.app = app
  }
})
