import Vue from 'vue'

export const store = Vue.observable({
  count: 0
})

export const mutations = {
  setCount (count) {
    console.log('set count to ' + count)
    store.count = 42
  }
}
