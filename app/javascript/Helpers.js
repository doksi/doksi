// Run a function on all children, and stop when it returns false.
function nodeWalk (node, func) {
  var r = func(node)

  for (node = node.firstChild; r !== false && node; node = node.nextSibling) {
    r = nodeWalk(node, func)
  }

  return r
}

export { nodeWalk }
