# frozen_string_literal: true

module Doksi
  class Application
    VERSION = '0.7.0'

    def version
      VERSION
    end
  end
end
