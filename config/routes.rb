# frozen_string_literal: true

Rails.application.routes.draw do
  get 'admin/dashboard'
  get 'admin/classifications'
  get 'admin/statuses'
  get 'admin/properties'
  get 'admin/roles'

  resources :classifications
  resources :statuses
  resources :properties

  resources :roles do
    resources :privileges
    resources :assignments, controller: 'role_assignments'
  end

  resources :documents do
    get :export
    resources :permissions
  end

  resources :blocks do
    collection do
      get :code2html
      get :rst2html
    end
  end

  resources :teams do
    resources :documents, only: [:index]
    member do
      delete :members, action: :remove_member
      post :members, action: :add_member
    end
  end

  root to: 'pages#dashboard'

  # auth
  devise_for :users, controllers: {
    omniauth_callbacks: 'users/omniauth_callbacks',
    registrations: 'users/registrations'
  }

  devise_scope :user do
    get 'sign_in', to: 'devise/sessions#new', as: :new_user_session
    delete 'sign_out', to: 'devise/sessions#destroy', as: :destroy_user_session
    get 'users', to: 'users/registrations#index', as: :users
    get 'users/:id', to: 'users/registrations#show', as: :user
    get 'settings', to: 'users/registrations#edit', as: :settings
    put 'settings', to: 'users/registrations#update'
    delete 'users', to: 'users/registrations#destroy', as: :destroy_user
  end

  resources :users, only: [] do
    resources :documents, only: [:index]
    resources :teams, only: [:index]
  end
end
