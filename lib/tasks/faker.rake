# frozen_string_literal: true

namespace :faker do
  desc 'Generate fake data'
  task generate: :environment do
    unless Rails.env.development?
      puts 'This can only be run in the development environment. Aborting!'
      return
    end
    puts ''

    puts 'Generate users'
    (0..15).each do |_n|
      puts "  #{generate_user.email}"
    end
    puts ''

    puts 'Generate teams'
    %w[
      Core
      Marketing
      Sales
    ].each do |team|
      generate_team(team)
      puts "  #{team}"
    end
    puts ''

    puts 'Generate documents'
    (0..10).each do |_n|
      owner = if rand(2) == 1
                User.offset(rand(User.count)).first
              else
                Team.offset(rand(Team.count)).first
        end
      puts "  #{generate_document(owner).title}"
    end
    puts ''
  end
end

task faker: ['db:reset', 'faker:generate']

def generate_user
  user = User.create(
    email: Faker::Internet.unique.email,
    provider: 'google_oauth2',
    uid: Faker::Internet.uuid,
    username: Faker::Internet.unique.username,
    fullname: Faker::Name.name,
    country: Faker::Address.country_code,
    time_zone: Faker::Address.time_zone.split('/').last,
    description: Faker::Hipster.paragraph(
      sentence_count: 2, supplemental: true, random_sentences_to_add: 4
    )
  )

  user.company = Faker::Company.name if rand(10) > 3
  user.department = Faker::Commerce.department(max: 1) if rand(10) > 7
  user.facebook = user.username if rand(10) > 3
  user.twitter = user.username if rand(10) > 4
  user.linkedin = user.username if rand(10) > 6
  user.github = user.username if rand(10) > 8
  user.gitlab = user.username if rand(10) > 9

  user.save

  user
end

def generate_document(owner)
  title = Faker::Lorem.words(number: rand(2..5))
  doc = Document.create(
    title: title.map(&:titleize).join(' '),
    copyright: Faker::Company.name,
    owner: owner,
    visibility: 'public'
  )

  (2..rand(3..8)).each do |_i|
    doc.blocks << case rand(4)
                  when 0
                    generate_list
                  else
                    generate_paragraph
                  end
  end

  level = 1
  (0..rand(2..4)).each do |_n|
    # generate heading
    level += rand(-1..1)
    level = 1 if level < 1
    level = 5 if level > 5
    doc.blocks << generate_heading(level)

    # generate content
    (2..rand(5..10)).each do |_i|
      doc.blocks << generate_block
    end
  end

  # fix order
  doc.blocks.each_with_index do |block, index|
    block.update_attribute(:order, index)
  end

  doc
end

# Generate a random type of block
def generate_block
  case rand(10)
  when 0
    generate_list
  when 1
    generate_quote
  when 2
    generate_image
  else
    generate_paragraph
  end
end

def generate_paragraph
  content = Faker::Lorem.sentences(number: rand(20..50))

  Paragraph.create(
    content: content.join(' '),
    order: 0,
    alignment: 'justify'
  )
end

def generate_image
  sizes = %w[
    200x200
    300x150
    500x200
    400x400
    800x600
    800x400
    800x800
  ]
  categories = %w[
    abstract
    animals
    business
    city
    food
    nightlife
    fashion
    people
    nature
    technics
  ]
  image_url = Faker::LoremPixel.image(size: sizes.sample, category: categories.sample, number: rand(1..10))
  Image.create(
    content: image_url,
    alignment: 'center'
  )
end

def generate_quote
  quote = Quote.create(
    content: '',
    order: 0,
    alignment: 'justify'
  )

  x = rand(12)

  case x
  when 0
    quote.content = Faker::Quote.famous_last_words
    quote.source = 'Famous Last Words'
  when 1
    quote.content = Faker::Quote.robin
    quote.source = 'Robin'
  when 2
    quote.content = Faker::Quote.yoda
    quote.source = 'Yoda'
  when 3
    quote.content = Faker::Quotes::Shakespeare.hamlet_quote
  when 4
    quote.content = Faker::Quotes::Shakespeare.romeo_and_juliet_quote
  when 6
    quote.content = Faker::TvShows::Buffy.quote
    quote.source = Faker::TvShows::Buffy.character
  when 7
    quote.content = Faker::TvShows::FamilyGuy.quote
    quote.source = Faker::TvShows::FamilyGuy.character
  when 8
    quote.content = Faker::TvShows::GameOfThrones.quote
    quote.source = Faker::TvShows::GameOfThrones.character
  when 9
    quote.content = Faker::TvShows::MichaelScott.quote
    quote.source = 'Michael Scott'
  when 10
    quote.content = Faker::TvShows::RickAndMorty.quote
    quote.source = Faker::TvShows::RickAndMorty.character
  else
    quote.content = Faker::Lorem.sentences(number: rand(1..10)).join(' ')
  end
  quote
end

def generate_heading(level)
  content = Faker::Lorem.words(number: rand(2..5), supplemental: true)

  Heading.create(
    content: content.map(&:titleize).join(' '),
    order: 0,
    level: level,
    alignment: 'left'
  )
end

def generate_list
  kinds = %w[unordered ordered]

  content = Array.new(rand(2..8)).map do
    words = Faker::Lorem.words(number: rand(2..5))
    "<li>#{words.join(' ').capitalize}</li>"
  end

  List.create(
    content: content.join(''),
    order: 0,
    kind: kinds[rand(kinds.length)],
    alignment: 'left'
  )
end

def generate_team(name)
  team = Team.create(
    name: name,
    description: Faker::Lorem.words(number: rand(2..10)).join(' '),
    user_ids: Array.new(rand(1..User.count)) { rand(1..User.count) }.uniq
  )
end
